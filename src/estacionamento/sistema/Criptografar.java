package estacionamento.sistema;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.swing.JOptionPane;

public class Criptografar {
	public static String criptografar(String xValor, String xAlgoritimo,
			int xBase) {
		MessageDigest md = null;
		BigInteger assinatura = null;
		try {
			md = MessageDigest.getInstance(xAlgoritimo);
			assinatura = new BigInteger(1, md.digest(xValor.getBytes()));
		} catch (NoSuchAlgorithmException e) {
			JOptionPane
					.showMessageDialog(
							null,
							"Informe um algoritmo de criptografia válido...\n MD2, MD5, SHA, SHA-256, SHA-383, SHA-512",
							"Criptografar", JOptionPane.ERROR_MESSAGE);
			return null;
		}
		return assinatura.toString(xBase);

	}
}