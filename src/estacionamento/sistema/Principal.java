package estacionamento.sistema;
import java.awt.Dimension;

import estacionamento.config.Parametros;
import estacionamento.gui.Login;

public class Principal {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Login login = new Login();
		Dimension d = new Dimension();
		d.setSize(450, 300);
		login.setMinimumSize(d);
		login.setLocationRelativeTo(null);
		login.setVisible(true);
	}

}
