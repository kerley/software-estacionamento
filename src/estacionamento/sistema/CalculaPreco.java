package estacionamento.sistema;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.joda.time.Minutes;

import estacionamento.entidade.Preco;
import estacionamento.persistencia.GenericDao;
import framework.util.MessageFactory;

public class CalculaPreco {

	private double precoHora = 0;
	private double precoDia = 0;
	private double precoMes = 0;
	private double valorTotal = 0;

	GenericDao<Object> precoDao = new GenericDao<Object>();

	public double calculaValorHora(String ent, String sai) {
		SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyyhh:mm:ss");
		// Criando entrada e saída em formato Date
		Date entrada;
		Date saida;
		try {
			entrada = df.parse(ent);
			saida = df.parse(sai);

			// Criando entrada e saída em formato Timestamp
			Timestamp a = new Timestamp(entrada.getTime());
			Timestamp b = new Timestamp(saida.getTime());

			// recebendo dados a partir de um Timestamp
			Minutes min1 = Minutes.minutesBetween(new DateTime(a),
					new DateTime(b));
			// recebendo dados a partir de um Date
			Minutes min2 = Minutes.minutesBetween(new DateTime(entrada),
					new DateTime(saida));

			// Retorna um int contendo o intervalo em minutos
			System.out.println("Timestamp convertido: " + min1.getMinutes()); // Consegue
																				// trabalhar
																				// com
																				// Timestamp
			System.out.println("Date convertido: " + min2.getMinutes()); // Ou
																			// Date

			// Convertendo os minutos totais em um float que represente a qtde
			// de horas
			double totalHoras = (double) min2.getMinutes() / 60;

			List<Preco> listaPreco = precoDao.listar("Preco");

			// pega o valor da hora do veiculo
			precoHora = listaPreco.get(0).getValor();

			// Calculando o valor sobre a qtde de tempo

			double valorTotal = totalHoras * precoHora;

			return valorTotal;

		} catch (ParseException e) {
			e.printStackTrace();
		}
		MessageFactory.showError("Erro ao calcular o valor de horas !!!",
				"Calcula Halor Hora");
		return 0;

	}

	public double calculaValorDiaria(long dias) {

		List<Preco> listaPreco = precoDao.listar("Preco");
		// pega o valor da diaria do veiculo
		precoDia = listaPreco.get(1).getValor();
		// pega o valor mensal do veiculo
		precoMes = listaPreco.get(2).getValor();

		if (dias == 0) {
			valorTotal = 1 * precoDia;

		} else if (dias < 31) {

			// Calculando o valor sobre a qtde de Dias
			valorTotal = dias * precoDia;

		} else {

			long meses = dias / 30;
			dias = dias % 30;
			// Calculando o valor sobre a qtde de meses
			valorTotal = meses * precoMes;
			// soma com a qtd de dias restantes.
			valorTotal = valorTotal + dias * precoDia;
			// Mostrando na tela

		}
		return valorTotal;

	}

	public double calculaValorMes() {

		// List<Preco> listaPreco = precoDao.listar("Preco");
		//
		// // pega o valor mensal do veiculo
		// precoMes = listaPreco.get(2).getValor();
		//
		// long meses = dias / 30;
		// if (meses == 0) {
		// valorTotal = precoMes * 1;
		// } else {
		// // Calculando o valor sobre a qtde de meses
		// valorTotal = meses * precoMes;
		// }
		valorTotal = 0;
		return valorTotal;

	}

	public double calculaValorConvenio() {
		valorTotal = 0;
		return valorTotal;

	}

}
