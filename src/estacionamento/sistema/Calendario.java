package estacionamento.sistema;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

public class Calendario {
	private String Calendario = "";

	public String getCalendario() {
		return Calendario;
	}

	public void setCalendario(String calendario) {
		Calendario = calendario;
	}

	/**
	 * Metado para exibir um calendario do mes e ano informando pelo usuario
	 * 
	 * @param mes
	 * @param ano
	 * 
	 */
	public Integer ano() {
		Calendar d = new GregorianCalendar();
		;
		d.getInstance();
		return d.get(Calendar.YEAR);
	}

	public String mes() {
		Calendar d = new GregorianCalendar();
		;
		d.getInstance();
		if (d.get(Calendar.MONTH) == 0) {
			return "Janeiro";
		}
		if (d.get(Calendar.MONTH) == 1) {
			return "Fevereiro";

		}
		if (d.get(Calendar.MONTH) == 2) {
			return "Março";
		}
		if (d.get(Calendar.MONTH) == 3) {
			return "Abril";

		}
		if (d.get(Calendar.MONTH) == 4) {
			return "Maio";
		}
		if (d.get(Calendar.MONTH) == 5) {
			return "Junho";
		}
		if (d.get(Calendar.MONTH) == 6) {
			return "Julho";

		}
		if (d.get(Calendar.MONTH) == 7) {
			return "Agosto";
		}
		if (d.get(Calendar.MONTH) == 8) {
			return "Setembro";

		}
		if (d.get(Calendar.MONTH) == 9) {
			return "Outubro";
		}
		if (d.get(Calendar.MONTH) == 10) {
			return "Novembro";

		}
		if (d.get(Calendar.MONTH) == 11) {
			return "Dezembro";
		}

		return "";

	}

	public void exibirMes(int mes, int ano) {

		Calendar data = new GregorianCalendar(ano, mes, 1);
		for (int i = 1; i < data.get(Calendar.DAY_OF_WEEK); i++) {
			Calendario = Calendario + "\t";
		}

		while (data.get(Calendar.MONTH) == mes) {

			Calendario = Calendario + data.get(Calendar.DAY_OF_MONTH) + "\t";
			data.add(Calendar.DAY_OF_MONTH, 1);

			if (data.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY) {
				Calendario = Calendario + "\n";
			}
		}
	}

	/**
	 * Metado calcula a idade do usuario de acordo com os paramtros informado
	 * pelo mesmo.
	 * 
	 * @param nasc
	 * @return
	 */
	public int calcularIdade(Calendar nasc) {
		int idade;
		Calendar atual = new GregorianCalendar();
		Calendar nascimento = new GregorianCalendar();
		nascimento.setTimeInMillis(nasc.getTimeInMillis());
		idade = atual.get(Calendar.YEAR) - nasc.get(Calendar.YEAR);
		int anoatual = atual.get(Calendar.YEAR);
		nascimento.set(Calendar.YEAR, anoatual);

		if (nascimento.after(atual)) {
			idade--;
			return idade;
		}

		return idade;

	}

	/**
	 * Metado para verificar o dia da semana
	 * 
	 * @param data
	 * @return
	 */
	public String diaDaSemana(Calendar data) {
		return data.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG,
				new Locale("pt", "BR"));
	}

	public long calcularDias(Calendar data) {
		Calendar atual = Calendar.getInstance();
		long difere = atual.getTimeInMillis() - data.getTimeInMillis();
		return difere / 1000 / 60 / 60 / 24;
	}

	public static void main(String[] args) {
		Calendario c = new Calendario();
		c.exibirMes(10, 2012); // metado de exibir um calendario do mes e ano
	}
}
