package estacionamento.sistema;

public class Preco {
	private int formaCobranca;
	private double tolerancia;
	private double veiculoHora;
	private double veiculoDiaria;
	private double veiculoMensal;
	private double veiculoPernoite;
	private double motoHora;
	private double motoDiaria;
	private double motoMensal;
	private double motoPernoite;
	private double veiculoGrandeHora;
	private double veiculoGrandeDiaria;
	private double veiculoGrandeMensal;
	private double veiculoGrandePernoite;
	private double veiculo15;
	private double veiculo30;
	private double veiculo45;
	private double moto15;
	private double moto30;
	private double moto45;
	private double veiculoGrande15;
	private double veiculoGrande30;
	private double veiculoGrande45;

	

	public int getFormaCobranca() {
		return formaCobranca;
	}

	public void setFormaCobranca(int formaCobranca) {
		this.formaCobranca = formaCobranca;
	}

	public double getTolerancia() {
		return tolerancia;
	}

	public void setTolerancia(double tolerancia) {
		this.tolerancia = tolerancia;
	}

	public double getVeiculoHora() {
		return veiculoHora;
	}

	public void setVeiculoHora(double veiculoHora) {
		this.veiculoHora = veiculoHora;
	}

	public double getVeiculoDiaria() {
		return veiculoDiaria;
	}

	public void setVeiculoDiaria(double veiculoDiaria) {
		this.veiculoDiaria = veiculoDiaria;
	}

	public double getVeiculoMensal() {
		return veiculoMensal;
	}

	public void setVeiculoMensal(double veiculoMensal) {
		this.veiculoMensal = veiculoMensal;
	}

	public double getVeiculoPernoite() {
		return veiculoPernoite;
	}

	public void setVeiculoPernoite(double veiculoPernoite) {
		this.veiculoPernoite = veiculoPernoite;
	}

	public double getMotoHora() {
		return motoHora;
	}

	public void setMotoHora(double motoHora) {
		this.motoHora = motoHora;
	}

	public double getMotoDiaria() {
		return motoDiaria;
	}

	public void setMotoDiaria(double motoDiaria) {
		this.motoDiaria = motoDiaria;
	}

	public double getMotoMensal() {
		return motoMensal;
	}

	public void setMotoMensal(double motoMensal) {
		this.motoMensal = motoMensal;
	}

	public double getMotoPernoite() {
		return motoPernoite;
	}

	public void setMotoPernoite(double motoPernoite) {
		this.motoPernoite = motoPernoite;
	}

	public double getVeiculoGrandeHora() {
		return veiculoGrandeHora;
	}

	public void setVeiculoGrandeHora(double veiculoGrandeHora) {
		this.veiculoGrandeHora = veiculoGrandeHora;
	}

	public double getVeiculoGrandeDiaria() {
		return veiculoGrandeDiaria;
	}

	public void setVeiculoGrandeDiaria(double veiculoGrandeDiaria) {
		this.veiculoGrandeDiaria = veiculoGrandeDiaria;
	}

	public double getVeiculoGrandeMensal() {
		return veiculoGrandeMensal;
	}

	public void setVeiculoGrandeMensal(double veiculoGrandeMensal) {
		this.veiculoGrandeMensal = veiculoGrandeMensal;
	}

	public double getVeiculoGrandePernoite() {
		return veiculoGrandePernoite;
	}

	public void setVeiculoGrandePernoite(double veiculoGrandePernoite) {
		this.veiculoGrandePernoite = veiculoGrandePernoite;
	}

	public double getVeiculo15() {
		return veiculo15;
	}

	public void setVeiculo15(double veiculo15) {
		this.veiculo15 = veiculo15;
	}

	public double getVeiculo30() {
		return veiculo30;
	}

	public void setVeiculo30(double veiculo30) {
		this.veiculo30 = veiculo30;
	}

	public double getVeiculo45() {
		return veiculo45;
	}

	public void setVeiculo45(double veiculo45) {
		this.veiculo45 = veiculo45;
	}

	public double getMoto15() {
		return moto15;
	}

	public void setMoto15(double moto15) {
		this.moto15 = moto15;
	}

	public double getMoto30() {
		return moto30;
	}

	public void setMoto30(double moto30) {
		this.moto30 = moto30;
	}

	public double getMoto45() {
		return moto45;
	}

	public void setMoto45(double moto45) {
		this.moto45 = moto45;
	}

	public double getVeiculoGrande15() {
		return veiculoGrande15;
	}

	public void setVeiculoGrande15(double veiculoGrande15) {
		this.veiculoGrande15 = veiculoGrande15;
	}

	public double getVeiculoGrande30() {
		return veiculoGrande30;
	}

	public void setVeiculoGrande30(double veiculoGrande30) {
		this.veiculoGrande30 = veiculoGrande30;
	}

	public double getVeiculoGrande45() {
		return veiculoGrande45;
	}

	public void setVeiculoGrande45(double veiculoGrande45) {
		this.veiculoGrande45 = veiculoGrande45;
	}

}
