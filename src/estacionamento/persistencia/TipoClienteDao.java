package estacionamento.persistencia;

import java.util.List;

import org.hibernate.Session;

import estacionamento.config.HibernateUtil;
import estacionamento.entidade.TipoCliente;

public class TipoClienteDao {
	public List<TipoCliente> listar() {
		Session session = HibernateUtil.getSession();
		session.beginTransaction();
		List<TipoCliente> lista = session.createQuery(
				"from TipoCliente").list();
		session.getTransaction().commit();
		session.close();
		return lista;
	}
	

}
