package estacionamento.persistencia;

import java.util.List;

import org.hibernate.Session;

import estacionamento.config.HibernateUtil;
import estacionamento.entidade.Endereco;

public class CidadeDao {
	public List<Endereco> listar(String uf) {
		Session session = HibernateUtil.getSession();
		session.beginTransaction();
		List<Endereco> cidade = session.createQuery("from Endereco where uf=?")
				.setString(0, uf).list();
		session.getTransaction().commit();
		session.close();
		return cidade;
	}
}
