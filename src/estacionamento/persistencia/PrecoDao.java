package estacionamento.persistencia;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.hibernate.Session;

public class PrecoDao {
	protected Session session;
	protected Class<?> classe;

	public void up(Object obj) {
		try {
			session.beginTransaction();
			session.update(obj);
			session.getTransaction().commit();
			session.clear();
			session.close();
		} catch (SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public boolean salvar(Object obj) {
		boolean salvar = true;
		try {
			session.beginTransaction();
			session.saveOrUpdate(obj);
			session.getTransaction().commit();
		} catch (org.hibernate.exception.ConstraintViolationException e) {
			salvar = false;
			JOptionPane.showMessageDialog(null, obj.toString()
					+ " já cadastrado!");
		} catch (Exception e) {
			salvar = false;
			e.printStackTrace();

		} finally {
			session.close();
			return salvar;
		}

	}

	public void excluir(Object obj) {
		session.beginTransaction();
		session.delete(obj);
		session.getTransaction().commit();
	}

	public List executarQuery(String sql) {
		List<String> cliente = new ArrayList<String>();
		cliente = session.createQuery(sql).list();
		return cliente;
	}

	public List listar(String classe) {
		return executarQuery("from " + classe);
	}

	public List listarAtivos(String classe) {
		return executarQuery("from " + classe);
	}

}
