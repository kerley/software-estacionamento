package estacionamento.persistencia;

import java.util.List;

import javax.swing.JOptionPane;

import org.hibernate.Session;

import estacionamento.config.HibernateUtil;
import estacionamento.entidade.TipoVeiculo;

public class TipoVeiculoDao {
	protected Session session;
	protected Class<?> classe;

	public TipoVeiculoDao() {
		session = HibernateUtil.getSession();
	}

	public void salvar(Object obj) {
		try {
			session.beginTransaction();
			session.saveOrUpdate(obj);
			session.getTransaction().commit();
		} catch (org.hibernate.exception.ConstraintViolationException e) {
			JOptionPane.showMessageDialog(null,
					"ERRO ...Excessao ainda nao tratada");
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			session.close();
		}

	}

	public List<TipoVeiculo> listar() {
		Session session = HibernateUtil.getSession();
		session.beginTransaction();
		List<TipoVeiculo> tipoVeiculo = session.createQuery("from TipoVeiculo")
				.list();
		session.getTransaction().commit();
		session.close();
		return tipoVeiculo;
	}

}
