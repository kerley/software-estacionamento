package estacionamento.persistencia;

import java.util.List;

import javax.swing.JOptionPane;

import org.hibernate.Session;

import estacionamento.config.HibernateUtil;
import estacionamento.entidade.Funcionarios;

public class GenericDao<ENTIDADE extends Object> {
	protected Session session;
	protected Class<?> classe;

	public GenericDao() {
		session = HibernateUtil.getSession();
	}

	public boolean salvar(Object obj){
		boolean salvar = true;
		try {
			session.beginTransaction();
			session.saveOrUpdate(obj);
			session.getTransaction().commit();
			session.close();

		} catch (org.hibernate.exception.ConstraintViolationException e) {
			salvar = false;
			JOptionPane.showMessageDialog(null, e.getSQLException().toString()
					.split(":"));
		} catch (Exception e) {
			salvar = false;
			e.printStackTrace();

		}
		return salvar;

	}

	public void excluir(Object obj) {
		try {
			session.beginTransaction();
			session.delete(obj);
			session.getTransaction().commit();
			session.close();
		} catch (org.hibernate.exception.ConstraintViolationException e) {
			JOptionPane.showMessageDialog(null,
					"Não é permitido excluir usuário !\nDevido o seguinte erro"+e.getSQLException().toString()
					.split(":"));
		}catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	public List listarEntrada(String classe) {
		List<String> select = session.createQuery(
				"from " + classe
						+ " where horaSaida is null and dataSaida is null ")
				.list();
		session.close();
		return select;
	}

	public List listarSaida(String classe, String placa) {
		List<String> select = session
				.createQuery(
						"from "
								+ classe
								+ " where horaSaida is null and dataSaida is null and placa = ?")
				.setString(0, placa).list();
		return select;
	}

	public List listar(String classe) {
		List<String> select = session.createQuery("from " + classe).list();
		session.close();
		return select;
	}

	public List listarEndereco(String classe, String cep) {
		List<String> cidade = session.createQuery("from Endereco where cep=?")
				.setString(0, cep).list();
		session.close();
		return cidade;
	}

	public List listarVeiculo(String classe, String placa) {
		List<String> lista = session.createQuery("from Veiculo where placa=?")
				.setString(0, placa).list();
		session.close();
		return lista;
	}

	public List listarVeiculoCliente(long idCliente) {
		List<String> lista = session
				.createQuery("from Veiculo where cliente_id=?")
				.setLong(0, idCliente).list();
		session.close();
		return lista;
	}

	public List listarCliente(String cpf) {
		List<String> cliente = session.createQuery("from Cliente where cpf=?")
				.setString(0, cpf).list();
		return cliente;
	}

	public long ultimoID(String classe) {
		List listID = null;
		listID = session.createQuery("select max(id) from " + classe).list();
		session.close();
		return (Long) listID.get(0);

	}

	public List<Funcionarios> loginDao(String senha, String usuario) {
		List<Funcionarios> user = session
				.createQuery("from Funcionarios where login=? and senha=?")
				.setString(0, usuario).setString(1, senha).list();
		session.close();

		return user;
	}

	public Session getSession() {
		return session;
	}

	public Class<?> getClasse() {
		return classe;
	}

	public void setClasse(Class<?> classe) {
		this.classe = classe;
	}

}
/*
 * protected void do_fieldPlaca_focusLost(FocusEvent arg0) { if
 * (!fieldPlaca.getText().equalsIgnoreCase("___-____")) { try {
 * GenericDao<Veiculo> listarVeiculo = new GenericDao<Veiculo>(); List<Veiculo>
 * veiculo = new ArrayList<Veiculo>(); veiculo.add((Veiculo)
 * listarVeiculo.listarVeiculo("Veiculo", fieldPlaca.getText()).get(0));
 * fieldCor.setText(veiculo.get(0).getCor());
 * fieldFabricante.setText(veiculo.get(0).getFrabricante());
 * fieldModelo.setText(veiculo.get(0).getModelo());
 * 
 * fieldModelo.setEditable(false); fieldFabricante.setEditable(false);
 * fieldCor.setEditable(false);
 * 
 * btnGravar.requestFocus(); } catch (java.lang.IndexOutOfBoundsException e) {
 * // JOptionPane.showMessageDialog(null, // "Veiculo não cadastrado")
 * fieldModelo.requestFocus(); // if (JOptionPane.showConfirmDialog(null, //
 * "Veiculo nao cadastrado!\nDeseja cadastrá-lo?", // "Excluir",
 * JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) { // Veiculo veiculo =
 * new Veiculo(); // GenericDao<Object> veiculoDao = new GenericDao<Object>();
 * // veiculo.setFrabricante(fieldFabricante.getText()); //
 * veiculo.setCor(fieldCor.getText()); //
 * veiculo.setModelo(fieldModelo.getText()); //
 * veiculo.setPlaca(fieldPlaca.getText()); // if (veiculoDao.salvar(veiculo) ==
 * true) { // JOptionPane.showMessageDialog(null, "Veiculo " // +
 * veiculo.getPlaca() // + " cadastrado com sucesso."); // }
 * 
 * }
 */