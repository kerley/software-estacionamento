package estacionamento.enums;

public enum Uf {
	AC("Acre"), AL("Alagoas"), AP("Macapá"), AM("Amazonas"), BA("Bahia"), CE(
			"Ceará"), DF("Distrito Federal"), GO("Goiás"), MA("Maranhão"), MT(
			"Mato Grosso"), MS("Mato Grosso do Sul"), MG("Minas Gerais"), PA(
			"Pará"), PB("Paraiba"), PE("Pernambuco"), PI("Piauí"), RJ(
			"Rio de Janeiro"), RN("Rio Grande do Sul"), RO("Rondônia"), RR(
			"Roraima"), SC("Santa Catarina"), SP("São Paulo"), SE("Sergipe"), TO(
			"Tocantins");

	private String uf_estenco;

	private Uf(String uf_estenco) {
		this.uf_estenco = uf_estenco;
	}

	public String getUf_estenco() {
		return uf_estenco;
	}
	
}
