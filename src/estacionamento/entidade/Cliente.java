package estacionamento.entidade;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import estacionamento.sistema.Calendario;

//@Entity---> define a classe java de acordo com o banco dados por default com o mesmo nome da classe/tabela 

@Entity
public class Cliente {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	@Column(length = 100, nullable = false)
	private String nome;

	// Atributo para controlar cliente ativo/inativo
	// Ativo = 1
	// Inativo = 0
	@Column(length = 1)
	private boolean ativo;
	@Column(length = 3, nullable = false)
	@Temporal(TemporalType.DATE)
	private Calendar datanasc;
	@Transient
	private Integer idade;
	private String sexo;
	@Column(length = 65, nullable = false)
	private String endereco;
	@Column(length = 6, nullable = false)
	private String endNumero;
	@Column(length = 45, nullable = false)
	private String bairro;
	@Column(length = 45, nullable = false)
	private String cidade;
	@Column(length = 3, nullable = false)
	private String uf;
	@Column(length = 20, nullable = false)
	private String cep;
	@Column(length = 20)
	private String tipoEndereco;
	@Column(length = 55)
	private String email;
	@Column(length = 15)
	private String telefone;
	@Column(length = 15)
	private String celular;
	@Column(length = 15, nullable = false)
	private String rg;
	// colocando a coluna cpf para ser do tipo unique
	@Column(length = 15, nullable = false, unique = true)
	private String cpf;
	@ManyToOne
	@JoinColumn(nullable = true)
	private TipoCliente tipoCliente;

	// @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	// @JoinTable(name = "Veiculo_Cliente", joinColumns = @JoinColumn(name =
	// "cliente_id"), inverseJoinColumns = @JoinColumn(name = "veiculo_id"))
	// private Collection<Veiculo> veiculos;

	public Integer getIdade() {
		Calendario calcIdade = new Calendario();
		idade = calcIdade.calcularIdade(getDatanasc());
		return idade;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public Calendar getDatanasc() {
		return datanasc;
	}

	public void setDatanasc(Calendar datanasc) {
		this.datanasc = datanasc;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getEndNumero() {
		return endNumero;
	}

	public void setEndNumero(String endNumero) {
		this.endNumero = endNumero;
	}
	
	public String getTipoEndereco() {
		return tipoEndereco;
	}

	public void setTipoEndereco(String tipoEndereco) {
		this.tipoEndereco = tipoEndereco;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public TipoCliente getTipoCliente() {
		return tipoCliente;
	}

	public void setTipoCliente(TipoCliente tipoCliente) {
		this.tipoCliente = tipoCliente;
	}

	// public Collection<Veiculo> getVeiculos() {
	// return veiculos;
	// }
	//
	// public void setVeiculos(Collection<Veiculo> veiculos) {
	// this.veiculos = veiculos;
	// }

	@Override
	public String toString() {
		return this.nome;
	}

}
