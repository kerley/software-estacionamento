package estacionamento.entidade;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class FormaCobranca {
	@Id
	@GeneratedValue
	private int id;
	@Column(length = 45)
	private String descricao;

	private boolean ativo;

	// @ManyToMany(cascade = CascadeType.ALL, targetEntity = Preco.class)
	// @JoinTable(name = "Cobranca_Veiculo_Preco", joinColumns =
	// @JoinColumn(name = "formaCobranca_id"), inverseJoinColumns =
	// @JoinColumn(name = "preco_id"))
	// private Set<Preco> precos;
	// @ManyToMany(cascade = CascadeType.ALL, targetEntity = Veiculo.class)
	// @JoinTable(name = "Cobranca_Veiculo_Preco", joinColumns =
	// @JoinColumn(name = "preco_id"), inverseJoinColumns = @JoinColumn(name =
	// "veiculo_id"))
	// private Set<Veiculo> veiculos;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public boolean isAtivo() {
		return ativo;
	}

	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	@Override
	public String toString() {
		return this.descricao;
	}

}
