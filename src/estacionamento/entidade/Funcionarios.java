package estacionamento.entidade;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import estacionamento.sistema.Calendario;

@Entity
public class Funcionarios {
	@Id
	@GeneratedValue
	private long id;
	@Column(length = 100)
	private String nome;
	@Column(length = 14, unique = true)
	private String cpf;
	@Temporal(TemporalType.DATE)
	private Calendar datanasc;
	@Transient
	private Integer idade;
	@Column(length = 15, nullable = false)
	private String rg;
	@Column(length = 13)
	private String telefone;
	@Column(length = 9)
	private String cep;
	@Column(length = 45)
	private String rua;
	@Column(length = 45)
	private String bairro;
	@Column(length = 45)
	private String cidade;
	@Column(length = 45)
	private String uf;
	@Column(length = 45)
	private String numero;
	@Column(length = 45)
	private String complemento;
	private String login;
	private String senha;
	@Column(length = 1)
	private String tipo;

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Calendar getDatanasc() {
		return datanasc;
	}

	public void setDatanasc(Calendar datanasc) {
		this.datanasc = datanasc;
	}

	public Integer getIdade() {
		Calendario calcIdade = new Calendario();
		idade = calcIdade.calcularIdade(getDatanasc());
		return idade;
	}

	public void setIdade(Integer idade) {
		this.idade = idade;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCep() {
		return cep;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

}
