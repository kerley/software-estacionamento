package estacionamento.entidade;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Estacionamento {
	@Id
	@GeneratedValue
	private Long id;
	@Column(length = 12)
	private String placa;
	@Column(length = 40)
	private String modelo;
	@Column(length = 40)
	private String fabricante;
	@Column(length = 20)
	private String cor;
	@Column(length = 20)
	private String dataEntrada;
	@Column(length = 20)
	private String horaEntrada;
	@Column(length = 20)
	private String dataSaida;
	@Column(length = 20)
	private String horaSaida;
	@ManyToOne
	@JoinColumn(nullable = true)
	private Cliente cliente;
	@Column(length = 8)
	private Double desconto;
	@Column(length = 8)
	private Double juros;
	@Column(length = 8)
	private Double valorTotal;
	@Column(length = 8)
	private Double valorPago;
	@Column(length = 8)
	private Double troco;
	@Column(length = 20)
	private String formaDeCobranca;
	@Column(length = 20)
	private String permanencia;

	// private Calendar horaSaida;

	// // private Calendar dataSaida;

	// private boolean servico;
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa.toUpperCase();
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public String getDataEntrada() {
		return dataEntrada;
	}

	public void setDataEntrada(String dataEntrada) {
		this.dataEntrada = dataEntrada;
	}

	public String getHoraEntrada() {
		return horaEntrada;
	}

	public void setHoraEntrada(String horaEntrada) {
		this.horaEntrada = horaEntrada;
	}

	public String getDataSaida() {
		return dataSaida;
	}

	public void setDataSaida(String dataSaida) {
		this.dataSaida = dataSaida;
	}

	public String getHoraSaida() {
		return horaSaida;
	}

	public void setHoraSaida(String horaSaida) {
		this.horaSaida = horaSaida;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Double getDesconto() {
		return desconto;
	}

	public void setDesconto(Double desconto) {
		this.desconto = desconto;
	}

	public Double getJuros() {
		return juros;
	}

	public void setJuros(Double juros) {
		this.juros = juros;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}

	public Double getValorPago() {
		return valorPago;
	}

	public void setValorPago(Double valorPago) {
		this.valorPago = valorPago;
	}

	public Double getTroco() {
		return troco;
	}

	public void setTroco(Double troco) {
		this.troco = troco;
	}

	public String getFormaDeCobranca() {
		return formaDeCobranca;
	}

	public void setFormaDeCobranca(String formaDeCobranca) {
		this.formaDeCobranca = formaDeCobranca;
	}

	public String getPermanencia() {
		return permanencia;
	}

	public void setPermanencia(String permanencia) {
		this.permanencia = permanencia;
	}

	// public boolean isServico() {
	// return servico;
	// }
	//
	// public void setServico(boolean servico) {
	// this.servico = servico;
	// }
	//

}
