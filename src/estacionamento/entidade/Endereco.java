package estacionamento.entidade;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Endereco {
	@Id
	@GeneratedValue
	private Integer id;
	@Column(length=50)
	private String cidade;
	@Column(length=70)
	private String logradouro;
	@Column(length=72)
	private String bairro;
	@Column(length=9,nullable=true,unique=true)
	private String cep;
	@Column(length=20)
	private String tp_logradouro;
	@Column(length=2,nullable=true)
	private String uf;
	@Column(length=60, nullable=true)
	private String uf_nome;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getTp_logradouro() {
		return tp_logradouro;
	}
	public void setTp_logradouro(String tp_logradouro) {
		this.tp_logradouro = tp_logradouro;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getUf_nome() {
		return uf_nome;
	}
	public void setUf_nome(String uf_nome) {
		this.uf_nome = uf_nome;
	}
	

}
