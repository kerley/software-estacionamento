package estacionamento.entidade;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Veiculo {
	@Id
	@GeneratedValue
	private long id;
	@Column(length = 45, nullable = false, unique = true)
	private String placa;
	@Column(length = 45, nullable = false)
	private String modelo;
	@Column(length = 45, nullable = false)
	private String Frabricante;
	@Column(length = 45)
	private String cor;
	private int anoFabricacao;
	@ManyToOne
	@JoinColumn(nullable = true)
	private Cliente cliente;
	@ManyToOne
	@JoinColumn(nullable = true)
	private TipoVeiculo tipoveiculo;
	

	public TipoVeiculo getTipoveiculo() {
		return tipoveiculo;
	}

	public void setTipoveiculo(TipoVeiculo tipoveiculo) {
		this.tipoveiculo = tipoveiculo;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getFrabricante() {
		return Frabricante;
	}

	public void setFrabricante(String frabricante) {
		Frabricante = frabricante;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public int getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(int anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	// @Override
	// public String toString() {
	// return "Placa "+ this.placa;
	// }

}
