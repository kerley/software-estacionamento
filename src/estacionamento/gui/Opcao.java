package estacionamento.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JInternalFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.MatteBorder;
import javax.swing.border.TitledBorder;

import estacionamento.entidade.FormaCobranca;
import estacionamento.entidade.TipoCliente;
import estacionamento.persistencia.GenericDao;
import estacionamento.persistencia.TipoClienteDao;

public class Opcao extends JInternalFrame implements ActionListener {
	public static boolean aberto = false;
	private final JCheckBox chckbxHorista = new JCheckBox("HORISTA");
	private final JCheckBox chckbxMensalista = new JCheckBox("MENSALISTA");
	private final JCheckBox chckbxDiarista = new JCheckBox("DIARISTA");
	private final JCheckBox chckbxVeculoNormal = new JCheckBox("VEÍCULO NORMAL");
	private final JCheckBox chckbxMoto = new JCheckBox("MOTO");
	private final JCheckBox chckbxVeculoDeGrande = new JCheckBox(
			"VEÍCULO DE GRANDE PORTE");
	private final JPanel panel_4;
	ButtonGroup group = new ButtonGroup();
	private final JRadioButton radioSemFracao;
	private final JRadioButton radioComFracao;
	private final JRadioButton radioFracao15min;
	private final JButton btnOk;
	private final JCheckBox chckbxConvenioCLIDESP;

	public boolean isAberto() {
		return aberto;
	}

	public void setAberto(boolean aberto) {
		this.aberto = aberto;
	}

	public Opcao() {
		setBounds(100, 100, 586, 396);

		JPanel panel_1 = new JPanel();
		panel_1.setBorder(new TitledBorder(null, "Tipos de Veículo",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));

		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1
				.setHorizontalGroup(gl_panel_1
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panel_1
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												gl_panel_1
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_panel_1
																		.createSequentialGroup()
																		.addComponent(
																				chckbxVeculoDeGrande,
																				GroupLayout.DEFAULT_SIZE,
																				175,
																				Short.MAX_VALUE)
																		.addGap(241))
														.addGroup(
																gl_panel_1
																		.createSequentialGroup()
																		.addComponent(
																				chckbxMoto,
																				GroupLayout.PREFERRED_SIZE,
																				175,
																				GroupLayout.PREFERRED_SIZE)
																		.addContainerGap(
																				231,
																				Short.MAX_VALUE))
														.addGroup(
																gl_panel_1
																		.createSequentialGroup()
																		.addComponent(
																				chckbxVeculoNormal,
																				GroupLayout.PREFERRED_SIZE,
																				203,
																				GroupLayout.PREFERRED_SIZE)
																		.addContainerGap(
																				203,
																				Short.MAX_VALUE)))));
		gl_panel_1.setVerticalGroup(gl_panel_1.createParallelGroup(
				Alignment.TRAILING).addGroup(
				gl_panel_1.createSequentialGroup().addGap(18)
						.addComponent(chckbxVeculoNormal)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(chckbxMoto)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(chckbxVeculoDeGrande).addGap(19)));
		panel_1.setLayout(gl_panel_1);

		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new MatteBorder(1, 1, 1, 1, new Color(0, 0, 0)));

		JPanel panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(null, "Tipos de Cliente",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		chckbxHorista.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				do_chckbxHorista_actionPerformed(e);
			}
		});
		chckbxHorista.setBounds(12, 22, 175, 23);

		chckbxConvenioCLIDESP = new JCheckBox("CONVÊNIO CLIDESP");
		chckbxConvenioCLIDESP.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				do_chckbxConvenio_actionPerformed(arg0);
			}
		});
		chckbxConvenioCLIDESP.setBounds(12, 133, 175, 23);

		panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(null, "Forma de Cobran\u00E7a",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_4.setFont(new Font("Tahoma", Font.PLAIN, 11));

		radioSemFracao = new JRadioButton("SEM FRAÇÃO DE MINUTO");
		radioSemFracao.setActionCommand("1");
		radioComFracao = new JRadioButton("COM FRAÇÃO DE MINUTO");
		radioComFracao.setActionCommand("2");
		radioFracao15min = new JRadioButton("FRAÇÕES DE 15 MINUTOS");
		radioFracao15min.setActionCommand("3");
		group.add(radioSemFracao);
		group.add(radioComFracao);
		group.add(radioFracao15min);

		GroupLayout gl_panel_4 = new GroupLayout(panel_4);
		gl_panel_4
				.setHorizontalGroup(gl_panel_4
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panel_4
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												gl_panel_4
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																radioFracao15min)
														.addGroup(
																gl_panel_4
																		.createSequentialGroup()
																		.addComponent(
																				radioSemFracao)
																		.addPreferredGap(
																				ComponentPlacement.RELATED,
																				4,
																				GroupLayout.PREFERRED_SIZE))
														.addComponent(
																radioComFracao))
										.addContainerGap(109, Short.MAX_VALUE)));
		gl_panel_4.setVerticalGroup(gl_panel_4.createParallelGroup(
				Alignment.TRAILING).addGroup(
				Alignment.LEADING,
				gl_panel_4.createSequentialGroup().addGap(15)
						.addComponent(radioSemFracao)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(radioComFracao)
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addComponent(radioFracao15min)
						.addContainerGap(27, Short.MAX_VALUE)));
		panel_4.setLayout(gl_panel_4);
		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout
				.setHorizontalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.LEADING,
																false)
														.addComponent(
																panel_2,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE)
														.addGroup(
																groupLayout
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				panel_4,
																				GroupLayout.PREFERRED_SIZE,
																				280,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addGroup(
																				groupLayout
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addComponent(
																								panel_3,
																								GroupLayout.PREFERRED_SIZE,
																								280,
																								GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								panel_1,
																								GroupLayout.PREFERRED_SIZE,
																								280,
																								GroupLayout.PREFERRED_SIZE))))
										.addGap(311)));
		groupLayout
				.setVerticalGroup(groupLayout
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								groupLayout
										.createSequentialGroup()
										.addGroup(
												groupLayout
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																panel_1,
																GroupLayout.PREFERRED_SIZE,
																132,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																panel_4,
																GroupLayout.PREFERRED_SIZE,
																132,
																GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addComponent(panel_3,
												GroupLayout.DEFAULT_SIZE, 165,
												Short.MAX_VALUE)
										.addGap(18)
										.addComponent(panel_2,
												GroupLayout.PREFERRED_SIZE,
												GroupLayout.DEFAULT_SIZE,
												GroupLayout.PREFERRED_SIZE)
										.addContainerGap()));
		panel_3.setLayout(null);
		panel_3.add(chckbxConvenioCLIDESP);
		panel_3.add(chckbxHorista);
		chckbxDiarista.setBounds(12, 59, 175, 23);
		panel_3.add(chckbxDiarista);
		chckbxMensalista.setBounds(12, 96, 175, 23);
		panel_3.add(chckbxMensalista);
		chckbxMensalista.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				do_chckbxMensalista_actionPerformed(arg0);
			}
		});

		chckbxDiarista.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				do_chckbxDiarista_actionPerformed(arg0);
			}
		});

		btnOk = new JButton("OK");
		btnOk.addActionListener(this);
		panel_2.add(btnOk);

		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				do_btnCancelar_actionPerformed(arg0);
			}
		});
		panel_2.add(btnCancelar);
		getContentPane().setLayout(groupLayout);
		verificaStatus();
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == btnOk) {
			do_btnOk_actionPerformed(e);
		}

	}

	public void verificaStatus() {
		TipoClienteDao tcDao = new TipoClienteDao();
		GenericDao<Object> fcDao = new GenericDao<Object>();
		List<TipoCliente> tCliente = tcDao.listar();
		List<FormaCobranca> fCobranca = fcDao.listar("FormaCobranca");

		try {
			if (tCliente.get(0).isAtivo()) {
				chckbxHorista.setSelected(true);
			}
			if (tCliente.get(1).isAtivo()) {
				chckbxDiarista.setSelected(true);
			}
			if (tCliente.get(2).isAtivo()) {
				chckbxMensalista.setSelected(true);
			}
			if (fCobranca.get(0).isAtivo()) {
				radioSemFracao.setSelected(true);
			} else if (fCobranca.get(1).isAtivo()) {
				radioComFracao.setSelected(true);
			} else {
				radioFracao15min.setSelected(true);
			}
			
			if (tCliente.get(1).isAtivo()) {
				chckbxDiarista.setSelected(true);
			}
			if (tCliente.get(2).isAtivo()) {
				chckbxMensalista.setSelected(true);
			}
			if (tCliente.get(3).isAtivo()) {
				chckbxConvenioCLIDESP.setSelected(true);
			}
			if (fCobranca.get(0).isAtivo()) {
				radioSemFracao.setSelected(true);
			} else if (fCobranca.get(1).isAtivo()) {
				radioComFracao.setSelected(true);
			} else {
				radioFracao15min.setSelected(true);
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		
	}

	protected void do_btnOk_actionPerformed(ActionEvent e) {
		GenericDao<Object> fcDao = new GenericDao<Object>();
		FormaCobranca fcs = new FormaCobranca();
		FormaCobranca fcc = new FormaCobranca();
		FormaCobranca fc15 = new FormaCobranca();
		fcs.setId(1);
		fcs.setDescricao("Sem Fracao de Minuto");
		fcc.setId(2);
		fcc.setDescricao("Com Fracao de Minuto");
		fc15.setId(3);
		fc15.setDescricao("Fracoes de 15 Minutos");
		if (radioSemFracao.isSelected()) { // verifica se a opção esta marcada
			fcs.setAtivo(true);
		} else {
			fcs.setAtivo(false);
		}

		if (radioComFracao.isSelected()) { // verifica se a opção esta marcada
			fcc.setAtivo(true);
		} else {
			fcc.setAtivo(false);
		}

		if (radioFracao15min.isSelected()) { // verifica se a opção esta marcada
			fc15.setAtivo(true);
		} else {
			fc15.setAtivo(false);
		}
		// fcDao.up(fcs);
		// fcDao.up(fcc);
		// fcDao.up(fc15);
		aberto = false;
		dispose();
	}

	protected void do_btnCancelar_actionPerformed(ActionEvent arg0) {
		aberto = false;
		dispose();
	}

	protected void do_chckbxHorista_actionPerformed(ActionEvent e) {

		TipoCliente tCliente = new TipoCliente();
		tCliente.setId(1);
		tCliente.setDescricao("Horista");
		if (chckbxHorista.isSelected()) {
			tCliente.setAtivo(true);

		} else {
			tCliente.setAtivo(false);
		}

		GenericDao<Object> tClienteDao = new GenericDao<Object>();
		tClienteDao.salvar(tCliente);

	}

	protected void do_chckbxDiarista_actionPerformed(ActionEvent arg0) {

		TipoCliente tCliente = new TipoCliente();
		tCliente.setId(2);
		tCliente.setDescricao("Diarista");
		if (chckbxDiarista.isSelected()) {
			tCliente.setAtivo(true);

		} else {
			tCliente.setAtivo(false);
		}
		GenericDao<Object> tClienteDao = new GenericDao<Object>();
		tClienteDao.salvar(tCliente);

	}

	protected void do_chckbxMensalista_actionPerformed(ActionEvent arg0) {

		TipoCliente tCliente = new TipoCliente();
		tCliente.setId(3);
		tCliente.setDescricao("Mensalista");
		if (chckbxMensalista.isSelected()) {
			tCliente.setAtivo(true);

		} else {
			tCliente.setAtivo(false);
		}

		GenericDao<Object> tClienteDao = new GenericDao<Object>();
		tClienteDao.salvar(tCliente);

	}

	protected void do_chckbxConvenio_actionPerformed(ActionEvent arg0) {
		TipoCliente tCliente = new TipoCliente();
		tCliente.setId(4);
		tCliente.setDescricao("CLIDESP");
		if (chckbxConvenioCLIDESP.isSelected()) {
			tCliente.setAtivo(true);

		} else {
			tCliente.setAtivo(false);
		}

		GenericDao<Object> tClienteDao = new GenericDao<Object>();
		tClienteDao.salvar(tCliente);

	}
}
