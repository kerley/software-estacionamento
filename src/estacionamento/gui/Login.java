package estacionamento.gui;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import estacionamento.componente.TButton;
import estacionamento.componente.TTextField;
import estacionamento.entidade.Funcionarios;
import estacionamento.persistencia.GenericDao;
import estacionamento.sistema.Criptografar;

public class Login extends JDialog implements FocusListener {

	private final JPanel panel_2;
	private final JLabel lblUsurio;
	private final JLabel lblSenha;
	private final TTextField fieldUsuario;
	private final JPasswordField pwdSenha;
	private final JLabel lblIcon;
	private final JPanel panel_3;
	private final JLabel label;
	private final JLabel lblSenhaerrada;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public Login() {

		setUndecorated(true);
		getContentPane().setBackground(SystemColor.activeCaption);

		panel_2 = new JPanel();
		panel_2.setBackground(Color.WHITE);
		panel_2.setBorder(new LineBorder(new Color(99, 130, 191), 10, true));
		GroupLayout groupLayout_1 = new GroupLayout(getContentPane());
		groupLayout_1.setHorizontalGroup(groupLayout_1.createParallelGroup(
				Alignment.LEADING).addGroup(
				groupLayout_1
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 415,
								Short.MAX_VALUE).addContainerGap()));
		groupLayout_1.setVerticalGroup(groupLayout_1.createParallelGroup(
				Alignment.LEADING).addGroup(
				groupLayout_1
						.createSequentialGroup()
						.addContainerGap()
						.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 276,
								Short.MAX_VALUE).addContainerGap()));

		lblUsurio = new JLabel("Usuário");
		lblUsurio.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 18));
		lblUsurio.setHorizontalAlignment(SwingConstants.LEFT);

		lblSenha = new JLabel("Senha");
		lblSenha.setHorizontalAlignment(SwingConstants.LEFT);
		lblSenha.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 18));

		final TButton btnLogin = new TButton("Login");

		fieldUsuario = new TTextField();
		fieldUsuario.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				pwdSenha.requestFocus();

			}
		});
		fieldUsuario.setColumns(10);

		pwdSenha = new JPasswordField();
		pwdSenha.addFocusListener(this);
		pwdSenha.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				btnLogin.requestFocus();

			}
		});
		pwdSenha.setEchoChar('*');

		lblIcon = new JLabel("Estacionameto");
		lblIcon.setHorizontalAlignment(SwingConstants.CENTER);
		lblIcon.setForeground(Color.BLUE);
		lblIcon.setFont(new Font("Dialog", Font.ITALIC, 18));
		lblIcon.setIcon(new ImageIcon(Login.class
				.getResource("/img/HDLogo.gif")));

		panel_3 = new JPanel();
		panel_3.setBackground(Color.WHITE);

		label = new JLabel("");
		label.setIcon(new ImageIcon(Login.class.getResource("/img/login.png")));

		lblSenhaerrada = new JLabel("Senha/Usuário inválido.");
		lblSenhaerrada.setFont(new Font("Dialog", Font.BOLD, 12));
		lblSenhaerrada.setForeground(Color.RED);
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2
				.setHorizontalGroup(gl_panel_2
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panel_2
										.createSequentialGroup()
										.addGap(22)
										.addComponent(lblIcon,
												GroupLayout.PREFERRED_SIZE,
												336, GroupLayout.PREFERRED_SIZE))
						.addGroup(
								gl_panel_2
										.createSequentialGroup()
										.addGroup(
												gl_panel_2
														.createParallelGroup(
																Alignment.TRAILING,
																false)
														.addGroup(
																Alignment.LEADING,
																gl_panel_2
																		.createSequentialGroup()
																		.addGap(47)
																		.addComponent(
																				panel_3,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE))
														.addGroup(
																Alignment.LEADING,
																gl_panel_2
																		.createSequentialGroup()
																		.addContainerGap()
																		.addComponent(
																				label,
																				GroupLayout.PREFERRED_SIZE,
																				51,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(18)
																		.addGroup(
																				gl_panel_2
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addComponent(
																								lblUsurio,
																								GroupLayout.PREFERRED_SIZE,
																								81,
																								GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								lblSenha,
																								GroupLayout.PREFERRED_SIZE,
																								81,
																								GroupLayout.PREFERRED_SIZE))
																		.addGroup(
																				gl_panel_2
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addGroup(
																								gl_panel_2
																										.createSequentialGroup()
																										.addGap(6)
																										.addComponent(
																												lblSenhaerrada,
																												GroupLayout.PREFERRED_SIZE,
																												181,
																												GroupLayout.PREFERRED_SIZE))
																						.addGroup(
																								gl_panel_2
																										.createSequentialGroup()
																										.addPreferredGap(
																												ComponentPlacement.RELATED)
																										.addGroup(
																												gl_panel_2
																														.createParallelGroup(
																																Alignment.LEADING,
																																false)
																														.addComponent(
																																pwdSenha)
																														.addComponent(
																																fieldUsuario,
																																GroupLayout.DEFAULT_SIZE,
																																209,
																																Short.MAX_VALUE))))))
										.addContainerGap(41, Short.MAX_VALUE)));
		gl_panel_2
				.setVerticalGroup(gl_panel_2
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panel_2
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												gl_panel_2
														.createParallelGroup(
																Alignment.LEADING,
																false)
														.addGroup(
																gl_panel_2
																		.createSequentialGroup()
																		.addGap(3)
																		.addGroup(
																				gl_panel_2
																						.createParallelGroup(
																								Alignment.BASELINE)
																						.addComponent(
																								lblUsurio)
																						.addComponent(
																								fieldUsuario,
																								GroupLayout.PREFERRED_SIZE,
																								24,
																								GroupLayout.PREFERRED_SIZE))
																		.addGap(18)
																		.addGroup(
																				gl_panel_2
																						.createParallelGroup(
																								Alignment.BASELINE,
																								false)
																						.addComponent(
																								lblSenha)
																						.addComponent(
																								pwdSenha,
																								GroupLayout.PREFERRED_SIZE,
																								22,
																								GroupLayout.PREFERRED_SIZE))
																		.addGap(21)
																		.addComponent(
																				lblSenhaerrada))
														.addComponent(
																label,
																GroupLayout.PREFERRED_SIZE,
																48,
																GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addComponent(lblIcon)
										.addGap(26)
										.addComponent(panel_3,
												GroupLayout.PREFERRED_SIZE, 41,
												GroupLayout.PREFERRED_SIZE)
										.addGap(31)));
		lblSenhaerrada.setVisible(false);
		panel_3.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		btnLogin.addActionListener(new ActionListener() {
			@Override
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {

				String usuario = fieldUsuario.getText();
				GenericDao login = new GenericDao();
				// Chamando metado para verificar o usuario e senha no banco de
				// dados, caso o metado loginDao returne true, o sistema abre
				// a tela principal, caso o contrario o sistema passa o lebal
				// erroSenha para visivel e não passa para proxima tela.
				Criptografar cript = new Criptografar();
				String senha = cript.criptografar(pwdSenha.getText(), "md5", 16);

				List<Funcionarios> listaUser = login.loginDao(senha, usuario);

				if (listaUser.size() != 0) {
					Funcionarios user = new Funcionarios();
					user.setNome(listaUser.get(0).getLogin());
					user.setTipo(listaUser.get(0).getTipo());
					FramePrincipal principal = new FramePrincipal();
					principal.inforUser(user);

					principal.setVisible(true);
					dispose();
				} else {
					lblSenhaerrada.setVisible(true);
				}
			}
		});
		btnLogin.setFont(new Font("Dialog", Font.BOLD, 14));
		panel_3.add(btnLogin);

		TButton btnCancelar = new TButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		btnCancelar.setFont(new Font("Dialog", Font.BOLD, 14));
		panel_3.add(btnCancelar);

		panel_2.setLayout(gl_panel_2);
		getContentPane().setLayout(groupLayout_1);

	}

	public void focusGained(FocusEvent e) {
		if (e.getSource() == pwdSenha) {
			do_pwdSenha_focusGained(e);
		}
	}

	public void focusLost(FocusEvent e) {
	}

	protected void do_pwdSenha_focusGained(FocusEvent e) {

		lblSenhaerrada.setVisible(false);
	}
}
