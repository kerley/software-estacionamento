package estacionamento.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.MaskFormatter;

import estacionamento.componente.TButton;
import estacionamento.componente.TTextField;
import estacionamento.componente.UpperCaseDocument;
import estacionamento.entidade.Cliente;
import estacionamento.entidade.Estacionamento;
import estacionamento.entidade.TipoVeiculo;
import estacionamento.entidade.Veiculo;
import estacionamento.persistencia.GenericDao;

public class FrameEntrada extends JInternalFrame implements ActionListener {

	private final JPanel contentPanel = new JPanel();
	private final JLabel lblPlaca;
	private final JFormattedTextField fieldPlaca;
	private final JLabel lblVeculo;
	private final TTextField fieldModelo;
	private final JLabel lblCor;
	private final TTextField fieldCor;
	private final MaskFormatter maskPlaca = new MaskFormatter();
	private final JPanel panel_1;
	private final TButton btnGravar;
	private final TButton btnCancelar;
	private static boolean atualizar = false;
	private FramePrincipal principal;
	public boolean aberto = false;
	private final TTextField fieldFabricante;
	private List<Veiculo> listaVeiculos;
	private String cpfCNPJ;
	private final JComboBox cbxVeiculos;
	private final JComboBox comboBox;
	private final JPanel panelCliente;
	private final JLabel label;
	private final TTextField fieldNomeCliente;
	private final JLabel label_1;
	private final TTextField fieldCpf;
	private final JLabel label_2;
	private final JLabel label_3;
	private final TTextField fieldTelefone;
	private final TTextField fieldCelular;
	private final JLabel label_4;
	private final TTextField fieldEndereco;
	private final JLabel label_5;
	private final JLabel label_6;
	private final TTextField fieldCidade;
	private final JLabel label_7;
	private final TTextField fieldTipoCliente;
	private final JLabel label_8;
	private final TTextField fieldNumero;
	private final TTextField fieldBairro;
	private Cliente c = null;
	List<Estacionamento> listaEntrada = new ArrayList<Estacionamento>();
	private final JLabel lblCPF;
	/**
	 * @wbp.nonvisual location=-19,437
	 */
	private final MaskFormatter maskCPF = new MaskFormatter();
	/**
	 * @wbp.nonvisual location=-29,487
	 */
	private final MaskFormatter maskCNPJ = new MaskFormatter();
	/**
	 * @wbp.nonvisual location=241,467
	 */
	private MaskFormatter maskgenerica;
	private final JFormattedTextField frmtdtxtfldCnpj;
	private final JFormattedTextField frmtdtxtfldCpf;

	public FramePrincipal getPrincipal() {
		return principal;
	}

	public void setPrincipal(FramePrincipal principal) {
		this.principal = principal;
	}

	public boolean isAberto() {
		return aberto;
	}

	public void setAberto(boolean aberto) {
		this.aberto = aberto;
	}

	public FrameEntrada() {
		try {
			maskCNPJ.setMask("###.###.##/####");
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		try {
			maskCPF.setMask("###.###.###-##");
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// setModal(false);
		try {

			maskPlaca.setMask("***-####");
			// maskPlaca.getMask().toUpperCase();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		maskPlaca.setPlaceholderCharacter('_');

		setTitle("Entrada");
		setBounds(100, 100, 928, 565);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Informe os dados",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));

		panel_1 = new JPanel();

		panelCliente = new JPanel();
		panelCliente.setBorder(new TitledBorder(null, "Dados do Cliente",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelCliente.setVisible(false);
		label = new JLabel("Nome:");

		fieldNomeCliente = new TTextField();
		fieldNomeCliente.setEditable(false);
		fieldNomeCliente.setForeground(Color.RED);
		fieldNomeCliente.setColumns(10);
		fieldNomeCliente.setBackground(Color.WHITE);

		label_1 = new JLabel("CPF:");

		fieldCpf = new TTextField();
		fieldCpf.setEditable(false);
		fieldCpf.setForeground(Color.RED);
		fieldCpf.setColumns(10);
		fieldCpf.setBackground(Color.WHITE);

		label_2 = new JLabel("Telefone:");

		label_3 = new JLabel("Celular:");

		fieldTelefone = new TTextField();
		fieldTelefone.setEditable(false);
		fieldTelefone.setForeground(Color.RED);
		fieldTelefone.setColumns(10);
		fieldTelefone.setBackground(Color.WHITE);

		fieldCelular = new TTextField();
		fieldCelular.setForeground(Color.RED);
		fieldCelular.setEditable(false);
		fieldCelular.setColumns(10);
		fieldCelular.setBackground(Color.WHITE);

		label_4 = new JLabel("Endereço:");

		fieldEndereco = new TTextField();
		fieldEndereco.setForeground(Color.RED);
		fieldEndereco.setEditable(false);
		fieldEndereco.setColumns(10);
		fieldEndereco.setBackground(Color.WHITE);

		label_5 = new JLabel("Numero:");

		label_6 = new JLabel("Bairro:");

		fieldCidade = new TTextField();
		fieldCidade.setForeground(Color.RED);
		fieldCidade.setEditable(false);
		fieldCidade.setColumns(10);
		fieldCidade.setBackground(Color.WHITE);

		label_7 = new JLabel("Tipo de Cliente");

		fieldTipoCliente = new TTextField();
		fieldTipoCliente.setForeground(Color.RED);
		fieldTipoCliente.setEditable(false);
		fieldTipoCliente.setColumns(10);
		fieldTipoCliente.setBackground(Color.WHITE);

		label_8 = new JLabel("Cidade:");

		fieldNumero = new TTextField();
		fieldNumero.setForeground(Color.RED);
		fieldNumero.setEditable(false);
		fieldNumero.setColumns(10);
		fieldNumero.setBackground(Color.WHITE);

		fieldBairro = new TTextField();
		fieldBairro.setForeground(Color.RED);
		fieldBairro.setEditable(false);
		fieldBairro.setColumns(10);
		fieldBairro.setBackground(Color.WHITE);

		btnGravar = new TButton("Gravar");
		btnGravar.setIcon(new ImageIcon(FrameEntrada.class
				.getResource("/img/salvar.gif")));
		btnGravar.addActionListener(this);

		btnCancelar = new TButton("Cancelar");
		btnCancelar.setIcon(new ImageIcon(FrameEntrada.class
				.getResource("/img/cancel.png")));
		btnCancelar.addActionListener(this);
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1.setHorizontalGroup(gl_panel_1.createParallelGroup(
				Alignment.TRAILING).addGroup(
				gl_panel_1.createSequentialGroup()
						.addContainerGap(270, Short.MAX_VALUE)
						.addComponent(btnGravar)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(btnCancelar).addContainerGap()));
		gl_panel_1.setVerticalGroup(gl_panel_1.createParallelGroup(
				Alignment.LEADING)
				.addGroup(
						gl_panel_1
								.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										gl_panel_1
												.createParallelGroup(
														Alignment.BASELINE)
												.addComponent(btnGravar)
												.addComponent(btnCancelar))
								.addContainerGap(GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)));
		panel_1.setLayout(gl_panel_1);

		lblPlaca = new JLabel("PLACA:");
		lblPlaca.setForeground(Color.BLUE);
		lblPlaca.setFont(new Font("Dialog", Font.BOLD, 18));

		fieldPlaca = new JFormattedTextField(maskPlaca);
		fieldPlaca.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				do_fieldPlaca_focusLost(arg0);
			}
		});
		fieldPlaca.setFont(new Font("Dialog", Font.BOLD, 18));
		// fieldPlaca.setColumns(10);

		lblVeculo = new JLabel("MODELO:");
		lblVeculo.setFont(new Font("Dialog", Font.PLAIN, 18));

		fieldModelo = new TTextField();
		fieldModelo.setFont(new Font("Dialog", Font.PLAIN, 18));
		fieldModelo.setColumns(10);

		lblCor = new JLabel("COR:");
		lblCor.setFont(new Font("Dialog", Font.PLAIN, 18));

		fieldCor = new TTextField();
		fieldCor.setFont(new Font("Dialog", Font.PLAIN, 18));
		fieldCor.setColumns(10);
		fieldCor.setDocument(new UpperCaseDocument());
		JLabel lblFabricante = new JLabel("FABRICANTE:");
		lblFabricante.setFont(new Font("Dialog", Font.PLAIN, 18));

		fieldFabricante = new TTextField();
		fieldFabricante.setFont(new Font("Dialog", Font.PLAIN, 18));
		fieldFabricante.setColumns(10);

		lblCPF = new JLabel("CPF:");
		lblCPF.setFont(new Font("Dialog", Font.PLAIN, 18));

		frmtdtxtfldCpf = new JFormattedTextField(maskCPF);
		frmtdtxtfldCpf.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent e) {
				cpfCNPJ = frmtdtxtfldCpf.getText();
				PesquisaVeiculos();
			}
		});

		JLabel lblTipo = new JLabel("Tipo");
		lblTipo.setFont(new Font("Dialog", Font.PLAIN, 18));

		comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (comboBox.getSelectedIndex() == 0) {
					frmtdtxtfldCpf.setBounds(144, 95, 134, 20);
					lblCPF.setText("CPF");
					frmtdtxtfldCpf.setText(null);
					frmtdtxtfldCpf.setVisible(true);
					frmtdtxtfldCnpj.setBounds(462, 252, 161, 19);
					frmtdtxtfldCnpj.setVisible(false);
				} else {
					frmtdtxtfldCnpj.setBounds(144, 95, 134, 20);
					lblCPF.setText("CNPJ");
					frmtdtxtfldCnpj.setText(null);
					frmtdtxtfldCnpj.setVisible(true);
					frmtdtxtfldCpf.setBounds(462, 252, 161, 19);
					frmtdtxtfldCpf.setVisible(false);
				}
			}
		});
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "Fisíca",
				"Jurídica" }));

		JLabel lblVeculo_1 = new JLabel("Veículo");
		lblVeculo_1.setFont(new Font("Dialog", Font.PLAIN, 18));

		cbxVeiculos = new JComboBox();
		cbxVeiculos.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fieldPlaca.setEditable(false);
				fieldPlaca.setText(listaVeiculos.get(
						cbxVeiculos.getSelectedIndex()).getPlaca());
				fieldModelo.setText(listaVeiculos.get(
						cbxVeiculos.getSelectedIndex()).getModelo());
				fieldFabricante.setText(listaVeiculos.get(
						cbxVeiculos.getSelectedIndex()).getFrabricante());
				fieldCor.setText(listaVeiculos.get(
						cbxVeiculos.getSelectedIndex()).getCor());

				fieldModelo.setEditable(false);
				fieldFabricante.setEditable(false);
				fieldCor.setEditable(false);
			}
		});

		frmtdtxtfldCnpj = new JFormattedTextField(maskCNPJ);
		frmtdtxtfldCnpj.setVisible(false);
		contentPanel.setLayout(new BorderLayout(0, 0));
		contentPanel.add(panel_1, BorderLayout.SOUTH);
		contentPanel.add(panel);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(7)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblTipo, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
							.addGap(62)
							.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblCPF, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(frmtdtxtfldCpf, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE)
							.addGap(33)
							.addComponent(lblVeculo_1, GroupLayout.PREFERRED_SIZE, 83, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(cbxVeiculos, GroupLayout.PREFERRED_SIZE, 197, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblPlaca, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(fieldPlaca, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblVeculo, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(fieldModelo, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblCor, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(fieldCor, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblFabricante, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(fieldFabricante, GroupLayout.PREFERRED_SIZE, 134, GroupLayout.PREFERRED_SIZE)
							.addGap(34)
							.addComponent(frmtdtxtfldCnpj, GroupLayout.PREFERRED_SIZE, 161, GroupLayout.PREFERRED_SIZE))))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(30)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblTipo, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
						.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(21)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(4)
							.addComponent(lblCPF, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(3)
							.addComponent(frmtdtxtfldCpf, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(5)
							.addComponent(lblVeculo_1, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))
						.addComponent(cbxVeiculos, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(21)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblPlaca, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
						.addComponent(fieldPlaca, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
					.addGap(12)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(6)
							.addComponent(lblVeculo, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE))
						.addComponent(fieldModelo, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(6)
							.addComponent(lblCor, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE))
						.addComponent(fieldCor, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(6)
							.addComponent(lblFabricante, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE))
						.addComponent(fieldFabricante, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(3)
							.addComponent(frmtdtxtfldCnpj, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
		);
		panel.setLayout(gl_panel);
		contentPanel.add(panelCliente, BorderLayout.EAST);
		GroupLayout gl_panelCliente = new GroupLayout(panelCliente);
		gl_panelCliente.setHorizontalGroup(
			gl_panelCliente.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelCliente.createSequentialGroup()
					.addGap(12)
					.addComponent(label, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panelCliente.createSequentialGroup()
					.addGap(12)
					.addComponent(fieldNomeCliente, GroupLayout.PREFERRED_SIZE, 254, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panelCliente.createSequentialGroup()
					.addGap(12)
					.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panelCliente.createSequentialGroup()
					.addGap(12)
					.addComponent(fieldCpf, GroupLayout.PREFERRED_SIZE, 254, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panelCliente.createSequentialGroup()
					.addGap(12)
					.addComponent(label_2, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
					.addGap(59)
					.addComponent(label_3, GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panelCliente.createSequentialGroup()
					.addGap(12)
					.addComponent(fieldTelefone, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(fieldCelular, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panelCliente.createSequentialGroup()
					.addGap(12)
					.addComponent(label_4, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panelCliente.createSequentialGroup()
					.addGap(12)
					.addComponent(fieldEndereco, GroupLayout.PREFERRED_SIZE, 254, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panelCliente.createSequentialGroup()
					.addGap(12)
					.addComponent(label_5, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(label_6, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panelCliente.createSequentialGroup()
					.addGap(12)
					.addComponent(fieldNumero, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
					.addGap(4)
					.addComponent(fieldBairro, GroupLayout.PREFERRED_SIZE, 164, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panelCliente.createSequentialGroup()
					.addGap(12)
					.addComponent(label_8, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panelCliente.createSequentialGroup()
					.addGap(12)
					.addComponent(fieldCidade, GroupLayout.PREFERRED_SIZE, 254, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panelCliente.createSequentialGroup()
					.addGap(11)
					.addComponent(label_7, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
				.addGroup(gl_panelCliente.createSequentialGroup()
					.addGap(11)
					.addComponent(fieldTipoCliente, GroupLayout.PREFERRED_SIZE, 254, GroupLayout.PREFERRED_SIZE))
		);
		gl_panelCliente.setVerticalGroup(
			gl_panelCliente.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panelCliente.createSequentialGroup()
					.addGap(21)
					.addComponent(label, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(fieldNomeCliente, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
					.addGap(10)
					.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(fieldCpf, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
					.addGap(15)
					.addGroup(gl_panelCliente.createParallelGroup(Alignment.LEADING)
						.addComponent(label_2, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_3, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE))
					.addGap(11)
					.addGroup(gl_panelCliente.createParallelGroup(Alignment.LEADING)
						.addComponent(fieldTelefone, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
						.addComponent(fieldCelular, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
					.addGap(21)
					.addComponent(label_4, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(fieldEndereco, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
					.addGap(12)
					.addGroup(gl_panelCliente.createParallelGroup(Alignment.LEADING)
						.addComponent(label_5, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
						.addComponent(label_6, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE))
					.addGap(11)
					.addGroup(gl_panelCliente.createParallelGroup(Alignment.LEADING)
						.addComponent(fieldNumero, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
						.addComponent(fieldBairro, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
					.addGap(15)
					.addComponent(label_8, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(fieldCidade, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
					.addGap(13)
					.addComponent(label_7, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
					.addGap(11)
					.addComponent(fieldTipoCliente, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
		);
		panelCliente.setLayout(gl_panelCliente);
		limpar();
		// aberto = true;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == btnCancelar) {
			do_btnCancelar_actionPerformed(e);
		}
		if (e.getSource() == btnGravar) {
			do_btnGravar_actionPerformed(e);
		}

	}

	public void limpar() {
		fieldFabricante.setText(null);
		fieldCor.setText(null);
		fieldModelo.setText(null);
		fieldPlaca.setText(null);
		fieldFabricante.setEditable(true);
		fieldCor.setEditable(true);
		fieldModelo.setEditable(true);
		panelCliente.setVisible(false);
	}

	protected void do_btnGravar_actionPerformed(ActionEvent e) {
		if (fieldPlaca.getText().equalsIgnoreCase("___-____")) {
			JOptionPane.showMessageDialog(null, "Informe a Placa do Veiculo");
			fieldPlaca.requestFocus();
		} else {

			// pega a data e hora da entrada
			Date dataHora = new Date();

			// formata a data e a hora
			SimpleDateFormat sdfData = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat sdfHora = new SimpleDateFormat("HH:mm:ss");

			Estacionamento entrada = new Estacionamento();
			entrada.setPlaca(fieldPlaca.getText());
			entrada.setModelo(fieldModelo.getText());
			entrada.setCor(fieldCor.getText());
			entrada.setFabricante(fieldFabricante.getText());
			entrada.setDataEntrada(sdfData.format(dataHora));
			entrada.setHoraEntrada(sdfHora.format(dataHora));
			// pegando o cliente caso seja cadastrado e associado ao veiculo
			if (listarCliente()) {
				entrada.setCliente(c);
			}

			// entrada.setServico(false);
			listaEntrada.add(entrada);
			GenericDao<Object> dao = new GenericDao<Object>();
			dao.salvar(entrada);
			if (listaVeiculos.size() == 0) {
				if (JOptionPane.showConfirmDialog(null,
						"Veiculo nao cadastrado!\nDeseja cadastrá-lo?",
						"Cadastrar Veiculo", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
					Veiculo veiculo = new Veiculo();
					GenericDao<Object> veiculoDao = new GenericDao<Object>();
					veiculo.setFrabricante(fieldFabricante.getText());
					veiculo.setCor(fieldCor.getText());
					veiculo.setModelo(fieldModelo.getText());
					veiculo.setPlaca(fieldPlaca.getText());
					do {
						try {
							veiculo.setAnoFabricacao(Integer.parseInt(JOptionPane
									.showInputDialog("Informe o ano de fabricação do veiculo:")));
						} catch (java.lang.NumberFormatException ex) {
							veiculo.setAnoFabricacao(0);
						}
					} while (veiculo.getAnoFabricacao() == 0);
					if (veiculoDao.salvar(veiculo) == true) {
						JOptionPane.showMessageDialog(null, "Veiculo "
								+ veiculo.getPlaca()
								+ " cadastrado com sucesso.");
					}
				}
			}
			limpar();
			principal.atualizaTabelaEntrada();

			aberto = false;
			dispose();

		}

	}

	protected void do_btnCancelar_actionPerformed(ActionEvent e) {
		limpar();
		aberto = false;
		dispose();
	}

	private void PesquisaVeiculos() {
		//teste
		cbxVeiculos.removeAllItems();
		List<Cliente> idCliente;
		listaVeiculos = new ArrayList<Veiculo>();
		GenericDao<Object> select = new GenericDao<Object>();
		idCliente = select.listarCliente(cpfCNPJ);
		if(!idCliente.isEmpty()){
			listaVeiculos = select.listarVeiculoCliente(idCliente.get(0).getId());
			for (Veiculo veiculo : listaVeiculos) {
				cbxVeiculos.addItem(veiculo.getPlaca());
			}
			listarCliente();
		}else{
			JOptionPane.showMessageDialog(null, "Usuário não cadastrado");
			limpar();
		}

	}

	private void desabilitarFields() {
		fieldModelo.setEditable(false);
		fieldFabricante.setEditable(false);
		fieldCor.setEditable(false);
	}

	protected void do_fieldPlaca_focusLost(FocusEvent arg0) {
		if (!fieldPlaca.getText().equalsIgnoreCase("___-____")) {
			try {
				GenericDao<Veiculo> veiculoDao = new GenericDao<Veiculo>();
				listaVeiculos = new ArrayList<Veiculo>();
				listaVeiculos.add((Veiculo) veiculoDao.listarVeiculo("Veiculo",
						fieldPlaca.getText()).get(0));
				fieldCor.setText(listaVeiculos.get(0).getCor());
				fieldFabricante.setText(listaVeiculos.get(0).getFrabricante());
				fieldModelo.setText(listaVeiculos.get(0).getModelo());
				desabilitarFields();
				listarCliente();
				btnGravar.requestFocus();
			} catch (java.lang.IndexOutOfBoundsException e) {
				fieldModelo.requestFocus();
			}

		}
	}

	public boolean listarCliente() {
		boolean cliente = false;
		GenericDao<Object> veiculoDao = new GenericDao<Object>();
		List<Veiculo> listaVeiculo = veiculoDao.listar("Veiculo");
		for (int i = 0; i < listaVeiculo.size(); i++) {
			if ((listaVeiculo.get(i).getPlaca().equalsIgnoreCase(fieldPlaca
					.getText())) && (listaVeiculo.get(i).getCliente() != null)) {
				cliente = true;
				fieldNomeCliente.setText(listaVeiculo.get(i).getCliente()
						.getNome());
				fieldCpf.setText(listaVeiculo.get(i).getCliente().getCpf());
				fieldCelular.setText(listaVeiculo.get(i).getCliente()
						.getCelular());
				fieldTelefone.setText(listaVeiculo.get(i).getCliente()
						.getTelefone());
				fieldEndereco.setText(listaVeiculo.get(i).getCliente()
						.getEndereco());
				fieldNumero.setText(listaVeiculo.get(i).getCliente()
						.getEndNumero());
				fieldBairro.setText(listaVeiculo.get(i).getCliente()
						.getBairro());
				fieldCidade.setText(listaVeiculo.get(i).getCliente()
						.getCidade());
				fieldTipoCliente.setText(listaVeiculo.get(i).getCliente()
						.getTipoCliente().getDescricao());
				c = new Cliente();
				c = listaVeiculo.get(i).getCliente();
			}
		}
		panelCliente.setVisible(cliente);
		return cliente;
	}

}