package estacionamento.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.EmptyBorder;
import javax.swing.text.MaskFormatter;

public class DialogPlaca extends JDialog implements ActionListener {

	private final JPanel contentPanel = new JPanel();
	/**
	 * @wbp.nonvisual location=31,327
	 */
	private final MaskFormatter maskPlaca = new MaskFormatter();
	private final JLabel lblPlaca;
	private final JFormattedTextField frmtdtxtfldPlaca;
	private String placa = null;
	private JButton okButton;
	private JButton cancelButton;

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public DialogPlaca() {
		setModal(true);
		setTitle("Saída Veiculo");
		try {
			maskPlaca.setMask("***-####");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setBounds(100, 100, 450, 122);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		lblPlaca = new JLabel("Informe a placa do veiculo");
		lblPlaca.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 14));

		frmtdtxtfldPlaca = new JFormattedTextField(maskPlaca);
		GroupLayout gl_contentPanel = new GroupLayout(contentPanel);
		gl_contentPanel
				.setHorizontalGroup(gl_contentPanel
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_contentPanel
										.createSequentialGroup()
										.addGroup(
												gl_contentPanel
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(lblPlaca)
														.addComponent(
																frmtdtxtfldPlaca,
																GroupLayout.PREFERRED_SIZE,
																317,
																GroupLayout.PREFERRED_SIZE))
										.addContainerGap(123, Short.MAX_VALUE)));
		gl_contentPanel.setVerticalGroup(gl_contentPanel.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_contentPanel
						.createSequentialGroup()
						.addComponent(lblPlaca)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(frmtdtxtfldPlaca,
								GroupLayout.PREFERRED_SIZE,
								GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addContainerGap(213, Short.MAX_VALUE)));
		contentPanel.setLayout(gl_contentPanel);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				okButton = new JButton("OK");
				okButton.addActionListener(this);
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(this);
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}

	public void atualizarPlaca(String placa) {
		frmtdtxtfldPlaca.setText(placa);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == cancelButton) {
			do_cancelButton_actionPerformed(arg0);
		}
		if (arg0.getSource() == okButton) {
			do_okButton_actionPerformed(arg0);
		}
	}

	protected void do_okButton_actionPerformed(ActionEvent arg0) {
		placa = frmtdtxtfldPlaca.getText();
		dispose();
	}

	protected void do_cancelButton_actionPerformed(ActionEvent arg0) {
		dispose();
	}
}
