package estacionamento.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.text.MaskFormatter;

import estacionamento.config.Parametros;
import estacionamento.entidade.Endereco;
import estacionamento.entidade.Funcionarios;
import estacionamento.enums.Uf;
import estacionamento.persistencia.GenericDao;
import estacionamento.sistema.Calendario;
import estacionamento.sistema.Criptografar;
import estacionamento.sistema.ValidacaoCpf;
import estacionamento.sistema.WebServiceCep;
import framework.component.field.TTextField;
import framework.component.table.TTableCompleta;
import framework.util.MessageFactory;

public class InternalFuncionario extends JInternalFrame implements
		ActionListener, FocusListener {
	private final TTextField txtNomefunc;
	private final TTextField txtLogin;
	public static boolean aberto = false;
	private final MaskFormatter maskCpf = new MaskFormatter();
	private final JPasswordField pwdSenha;
	private final JPasswordField pwdSenha_1;
	private final MaskFormatter maskTelefone = new MaskFormatter();
	private final JFormattedTextField txtTelefone;
	private final JComboBox Tipo;
	private final JButton btnGravar;
	private final JFormattedTextField txtCpf;
	private final JLabel lblLad;
	private final JPanel panel;
	private final JPanel panel_1;
	private final JPanel panel_2;
	private final JPanel panel_3;
	private final JLabel lblUf;
	private final JLabel lblCidade;
	private final JLabel lblCep;
	private final JFormattedTextField txtfldCep;
	private final MaskFormatter maskCep = new MaskFormatter();
	private final JLabel lblRua;
	private final TTextField txtFieldEndereco;
	private final TTextField textFieldComplemento;
	private final JLabel lblComplemento;
	private final TTextField textFieldNumero;
	private final JLabel lblNumero;
	private final JLabel lblBairro;
	private final TTextField textFieldBairro;
	private final JButton btnExcluir;
	private final JButton btnAlterar;
	private final JPanel panel_4;
	private final JPanel panel_5;
	private long id;
	private final JLabel lblCodigo;
	private final JTextField textFieldCod;
	private boolean alterar = false;
	private final JLabel lblDataNascimento;
	private final JFormattedTextField frmtdtxtfldDatanasc;
	private boolean alterarSenha = false;
	/**
	 * @wbp.nonvisual location=31,-13
	 */
	private final MaskFormatter maskDataNasc = new MaskFormatter();
	private final JTextField textFieldUF;
	private final JTextField textFieldCidade;
	private final JTextField txtIdade;
	private final JLabel lblRg;
	private final TTextField txtRg;
	private final TTableCompleta tableCompleta;
	private JTextField txtTipoendereco;
	private JLabel lblNewLabel;
	private JLabel lblSenha;
	private JLabel lblConfirmasenha;

	public InternalFuncionario() {
		maskDataNasc.setPlaceholderCharacter('_');
		maskDataNasc.setPlaceholder("");
		try {
			maskDataNasc.setMask("##/##/####");
		} catch (ParseException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}

		try {
			GenericDao d = new GenericDao();
			id = d.ultimoID("Funcionarios") + 1;
		} catch (java.lang.NullPointerException e) {
			id = 1;
		}

		try {
			maskCep.setPlaceholderCharacter('_');
			maskCep.setMask("#####-###");
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		setVerifyInputWhenFocusTarget(false);
		setTitle("Cadastro de Funcionários");
		try {
			maskTelefone.setPlaceholderCharacter('_');
			maskTelefone.setMask("(##)####-####");
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			maskCpf.setPlaceholderCharacter('_');
			maskCpf.setMask("###.###.###-##");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		setBounds(100, 100, 886, 673);

		panel = new JPanel();
		panel.setBorder(new BevelBorder(BevelBorder.RAISED, null, null, null,
				null));

		panel_1 = new JPanel();
		panel_1.setVerifyInputWhenFocusTarget(false);
		panel_1.setBorder(new TitledBorder(new LineBorder(new Color(184, 207,
				229)), "Login", TitledBorder.LEADING, TitledBorder.TOP, null,
				new Color(51, 51, 51)));

		panel_3 = new JPanel();
		panel_3.setBorder(new TitledBorder(null, "Endere\u00E7o",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));

		panel_2 = new JPanel();
		panel_2.setAlignmentX(Component.RIGHT_ALIGNMENT);
		panel_2.setBorder(new TitledBorder(new LineBorder(new Color(184, 207,
				229)), "Pessoa", TitledBorder.LEADING, TitledBorder.TOP, null,
				null));

		panel_4 = new JPanel();
		panel_4.setLayout(new GridLayout(0, 1, 0, 0));
		panel_4.add(panel_2);
		panel_4.add(panel_3);
		panel_4.add(panel_1);

		panel_4.setPreferredSize(new Dimension(1, 300));

		txtTelefone = new JFormattedTextField(maskTelefone);

		txtCpf = new JFormattedTextField(maskCpf);
		txtCpf.setText("");

		JLabel lblTelefone = new JLabel("Telefone.:");

		JLabel lblCpf = new JLabel("CPF.:");

		txtNomefunc = new TTextField();
		txtNomefunc.setColumns(10);

		JLabel lblNome = new JLabel("Nome.:");

		JLabel lblIdade = new JLabel("Idade");

		lblCodigo = new JLabel("Código.:");

		textFieldCod = new JTextField();
		textFieldCod.setEnabled(true);
		textFieldCod.setEditable(false);
		textFieldCod.setText(String.valueOf(id));
		textFieldCod.setColumns(10);

		lblDataNascimento = new JLabel("Data Nascimento.:");

		frmtdtxtfldDatanasc = new JFormattedTextField(maskDataNasc);
		frmtdtxtfldDatanasc.addFocusListener(this);

		txtIdade = new TTextField();
		txtIdade.setEditable(false);
		txtIdade.setBounds(492, 74, 97, 19);
		txtIdade.setColumns(10);

		lblRg = new JLabel("RG.:");

		txtRg = new TTextField();
		txtRg.setColumns(10);
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2
				.setHorizontalGroup(gl_panel_2
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panel_2
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												gl_panel_2
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_panel_2
																		.createParallelGroup(
																				Alignment.TRAILING,
																				false)
																		.addComponent(
																				lblCpf,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE)
																		.addComponent(
																				lblCodigo,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE))
														.addComponent(
																lblTelefone,
																GroupLayout.PREFERRED_SIZE,
																80,
																GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												gl_panel_2
														.createParallelGroup(
																Alignment.TRAILING,
																false)
														.addComponent(
																txtTelefone)
														.addGroup(
																gl_panel_2
																		.createSequentialGroup()
																		.addComponent(
																				textFieldCod,
																				GroupLayout.PREFERRED_SIZE,
																				75,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				lblNome,
																				GroupLayout.PREFERRED_SIZE,
																				60,
																				GroupLayout.PREFERRED_SIZE))
														.addComponent(txtCpf))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												gl_panel_2
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																txtNomefunc,
																GroupLayout.DEFAULT_SIZE,
																591,
																Short.MAX_VALUE)
														.addGroup(
																gl_panel_2
																		.createSequentialGroup()
																		.addComponent(
																				lblDataNascimento)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				frmtdtxtfldDatanasc,
																				GroupLayout.PREFERRED_SIZE,
																				132,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				lblIdade,
																				GroupLayout.PREFERRED_SIZE,
																				42,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(4)
																		.addComponent(
																				txtIdade,
																				GroupLayout.PREFERRED_SIZE,
																				91,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				lblRg)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				txtRg)))
										.addContainerGap()));
		gl_panel_2
				.setVerticalGroup(gl_panel_2
						.createParallelGroup(Alignment.TRAILING)
						.addGroup(
								gl_panel_2
										.createSequentialGroup()
										.addGroup(
												gl_panel_2
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(lblCodigo)
														.addComponent(
																textFieldCod,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																txtNomefunc,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(lblNome))
										.addPreferredGap(
												ComponentPlacement.UNRELATED)
										.addGroup(
												gl_panel_2
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_panel_2
																		.createParallelGroup(
																				Alignment.BASELINE)
																		.addComponent(
																				txtCpf,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(
																				lblIdade)
																		.addComponent(
																				frmtdtxtfldDatanasc,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(
																				lblDataNascimento)
																		.addComponent(
																				txtIdade,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(
																				lblRg)
																		.addComponent(
																				txtRg,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE))
														.addComponent(lblCpf))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												gl_panel_2
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblTelefone,
																GroupLayout.PREFERRED_SIZE,
																15,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																txtTelefone,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE))
										.addGap(21)));
		panel_2.setLayout(gl_panel_2);

		lblUf = new JLabel("UF.:");

		lblCidade = new JLabel("Cidade.:");

		lblCep = new JLabel("Cep.:");

		txtfldCep = new JFormattedTextField(maskCep);
		txtfldCep.addFocusListener(this);

		lblRua = new JLabel("Endereço.:");

		txtFieldEndereco = new TTextField();
		txtFieldEndereco.setEditable(false);
		txtFieldEndereco.setColumns(10);

		textFieldComplemento = new TTextField();
		textFieldComplemento.setColumns(10);

		lblComplemento = new JLabel("Complemento.:");

		textFieldNumero = new TTextField();
		textFieldNumero.setColumns(10);

		lblNumero = new JLabel("Número.:");

		lblBairro = new JLabel("Bairro.:");

		textFieldBairro = new TTextField();
		textFieldBairro.setEditable(false);
		textFieldBairro.setColumns(10);

		textFieldUF = new TTextField();
		textFieldUF.setEditable(false);
		textFieldUF.setColumns(10);

		textFieldCidade = new TTextField();
		textFieldCidade.setEditable(false);
		textFieldCidade.setColumns(10);

		txtTipoendereco = new JTextField();
		txtTipoendereco.setEditable(false);
		txtTipoendereco.setColumns(10);

		lblNewLabel = new JLabel("Tipo.:");
		GroupLayout gl_panel_3 = new GroupLayout(panel_3);
		gl_panel_3
				.setHorizontalGroup(gl_panel_3
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panel_3
										.createSequentialGroup()
										.addGap(23)
										.addGroup(
												gl_panel_3
														.createParallelGroup(
																Alignment.LEADING,
																false)
														.addComponent(
																lblBairro,
																GroupLayout.PREFERRED_SIZE,
																65,
																GroupLayout.PREFERRED_SIZE)
														.addGroup(
																gl_panel_3
																		.createSequentialGroup()
																		.addComponent(
																				lblCep,
																				GroupLayout.PREFERRED_SIZE,
																				54,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED,
																				11,
																				Short.MAX_VALUE)))
										.addGap(0)
										.addGroup(
												gl_panel_3
														.createParallelGroup(
																Alignment.LEADING,
																false)
														.addGroup(
																gl_panel_3
																		.createSequentialGroup()
																		.addComponent(
																				txtfldCep,
																				GroupLayout.PREFERRED_SIZE,
																				96,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				lblRua,
																				GroupLayout.PREFERRED_SIZE,
																				80,
																				GroupLayout.PREFERRED_SIZE))
														.addComponent(
																textFieldBairro,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.DEFAULT_SIZE,
																Short.MAX_VALUE))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												gl_panel_3
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_panel_3
																		.createSequentialGroup()
																		.addComponent(
																				lblCidade,
																				GroupLayout.PREFERRED_SIZE,
																				66,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				textFieldCidade,
																				GroupLayout.DEFAULT_SIZE,
																				117,
																				Short.MAX_VALUE))
														.addComponent(
																txtFieldEndereco,
																GroupLayout.DEFAULT_SIZE,
																234,
																Short.MAX_VALUE))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												gl_panel_3
														.createParallelGroup(
																Alignment.LEADING,
																false)
														.addGroup(
																gl_panel_3
																		.createSequentialGroup()
																		.addComponent(
																				lblUf,
																				GroupLayout.PREFERRED_SIZE,
																				43,
																				GroupLayout.PREFERRED_SIZE)
																		.addPreferredGap(
																				ComponentPlacement.RELATED)
																		.addComponent(
																				textFieldUF,
																				GroupLayout.PREFERRED_SIZE,
																				42,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(28)
																		.addComponent(
																				lblComplemento))
														.addGroup(
																Alignment.TRAILING,
																gl_panel_3
																		.createSequentialGroup()
																		.addComponent(
																				lblNewLabel)
																		.addPreferredGap(
																				ComponentPlacement.RELATED,
																				GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE)
																		.addComponent(
																				txtTipoendereco,
																				GroupLayout.PREFERRED_SIZE,
																				82,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(33)
																		.addComponent(
																				lblNumero)))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												gl_panel_3
														.createParallelGroup(
																Alignment.LEADING,
																false)
														.addComponent(
																textFieldComplemento,
																0, 0,
																Short.MAX_VALUE)
														.addComponent(
																textFieldNumero,
																GroupLayout.DEFAULT_SIZE,
																76,
																Short.MAX_VALUE))
										.addContainerGap()));
		gl_panel_3
				.setVerticalGroup(gl_panel_3
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panel_3
										.createSequentialGroup()
										.addGroup(
												gl_panel_3
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(lblCep)
														.addComponent(
																textFieldNumero,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																txtfldCep,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(lblNumero)
														.addComponent(lblRua)
														.addComponent(
																txtFieldEndereco,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																txtTipoendereco,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																lblNewLabel))
										.addGap(18)
										.addGroup(
												gl_panel_3
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																textFieldComplemento,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addGroup(
																gl_panel_3
																		.createParallelGroup(
																				Alignment.BASELINE)
																		.addComponent(
																				lblBairro,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				Short.MAX_VALUE)
																		.addComponent(
																				textFieldBairro,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE)
																		.addComponent(
																				lblCidade)
																		.addComponent(
																				textFieldCidade,
																				GroupLayout.PREFERRED_SIZE,
																				GroupLayout.DEFAULT_SIZE,
																				GroupLayout.PREFERRED_SIZE))
														.addComponent(lblUf)
														.addComponent(
																textFieldUF,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																lblComplemento))
										.addGap(50)));
		panel_3.setLayout(gl_panel_3);

		JLabel lblLogin = new JLabel("Usuário.:");

		txtLogin = new TTextField();
		txtLogin.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (txtLogin.getText().isEmpty()) {
					JOptionPane.showMessageDialog(null, "Campo obrigatório");
					txtLogin.requestFocus();
				}
			}
		});
		txtLogin.setColumns(10);

		lblSenha = new JLabel("Senha.:");

		pwdSenha = new JPasswordField();
		pwdSenha.addFocusListener(this);

		pwdSenha_1 = new JPasswordField();
		pwdSenha_1.addFocusListener(this);

		lblConfirmasenha = new JLabel("Confirma-Senha");

		lblLad = new JLabel("Senha não confere");
		lblLad.setVisible(false);
		lblLad.setFont(new Font("Ubuntu", Font.BOLD | Font.ITALIC, 15));
		lblLad.setForeground(Color.RED);

		JLabel lblTipo = new JLabel("Tipo.:");

		Tipo = new JComboBox();
		Tipo.setModel(new DefaultComboBoxModel(new String[] { "Operador",
				"Supervisor" }));

		btnAlterarSenha = new JButton("Alterar Senha");
		btnAlterarSenha.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				alterarSenha = true;
				lblSenha.setVisible(true);
				pwdSenha.setVisible(true);
				lblConfirmasenha.setVisible(true);
				pwdSenha_1.setVisible(true);
			}
		});
		btnAlterarSenha.setVisible(false);
		GroupLayout gl_panel_1 = new GroupLayout(panel_1);
		gl_panel_1
				.setHorizontalGroup(gl_panel_1
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panel_1
										.createSequentialGroup()
										.addContainerGap()
										.addGroup(
												gl_panel_1
														.createParallelGroup(
																Alignment.LEADING)
														.addComponent(
																lblSenha,
																GroupLayout.PREFERRED_SIZE,
																127,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																lblConfirmasenha)
														.addComponent(
																lblLogin,
																GroupLayout.PREFERRED_SIZE,
																72,
																GroupLayout.PREFERRED_SIZE))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												gl_panel_1
														.createParallelGroup(
																Alignment.LEADING,
																false)
														.addGroup(
																gl_panel_1
																		.createSequentialGroup()
																		.addGroup(
																				gl_panel_1
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addComponent(
																								txtLogin,
																								GroupLayout.PREFERRED_SIZE,
																								207,
																								GroupLayout.PREFERRED_SIZE)
																						.addComponent(
																								pwdSenha_1,
																								GroupLayout.PREFERRED_SIZE,
																								207,
																								GroupLayout.PREFERRED_SIZE))
																		.addPreferredGap(
																				ComponentPlacement.UNRELATED)
																		.addGroup(
																				gl_panel_1
																						.createParallelGroup(
																								Alignment.LEADING)
																						.addGroup(
																								gl_panel_1
																										.createSequentialGroup()
																										.addComponent(
																												lblTipo)
																										.addPreferredGap(
																												ComponentPlacement.RELATED)
																										.addComponent(
																												Tipo,
																												GroupLayout.PREFERRED_SIZE,
																												146,
																												GroupLayout.PREFERRED_SIZE)
																										.addGap(18)
																										.addComponent(
																												btnAlterarSenha))
																						.addComponent(
																								lblLad)))
														.addComponent(
																pwdSenha,
																GroupLayout.PREFERRED_SIZE,
																207,
																GroupLayout.PREFERRED_SIZE))
										.addGap(157)));
		gl_panel_1
				.setVerticalGroup(gl_panel_1
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panel_1
										.createSequentialGroup()
										.addGroup(
												gl_panel_1
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(lblLogin)
														.addComponent(
																txtLogin,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(lblTipo)
														.addComponent(
																Tipo,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(
																btnAlterarSenha))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												gl_panel_1
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																pwdSenha,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(lblSenha))
										.addPreferredGap(
												ComponentPlacement.RELATED)
										.addGroup(
												gl_panel_1
														.createParallelGroup(
																Alignment.BASELINE)
														.addComponent(
																lblConfirmasenha)
														.addComponent(
																pwdSenha_1,
																GroupLayout.PREFERRED_SIZE,
																GroupLayout.DEFAULT_SIZE,
																GroupLayout.PREFERRED_SIZE)
														.addComponent(lblLad))
										.addGap(69)));
		panel_1.setLayout(gl_panel_1);

		btnGravar = new JButton("Salvar");
		btnGravar.addActionListener(this);
		btnGravar.setIcon(new ImageIcon(InternalFuncionario.class
				.getResource("/img/save.png")));

		JButton btnCancelar = new JButton("Sair");
		btnCancelar.setIcon(new ImageIcon(InternalFuncionario.class
				.getResource("/img/Sair.png")));
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		panel.add(btnGravar);

		btnExcluir = new JButton("Excluir");
		btnExcluir.addActionListener(this);
		btnExcluir.setIcon(new ImageIcon(InternalFuncionario.class
				.getResource("/img/excluir.gif")));
		panel.add(btnExcluir);

		btnAlterar = new JButton("Alterar");
		btnAlterar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				do_btnAlterar_actionPerformed(arg0);
			}
		});
		btnAlterar.setIcon(new ImageIcon(InternalFuncionario.class
				.getResource("/img/alterar1.png")));
		panel.add(btnAlterar);
		panel.add(btnCancelar);
		btnCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {

				aberto = false;
				dispose();
			}
		});
		getContentPane().setLayout(new BorderLayout(0, 0));
		getContentPane().add(panel, BorderLayout.SOUTH);
		getContentPane().add(panel_4, BorderLayout.NORTH);

		panel_5 = new JPanel();
		getContentPane().add(panel_5, BorderLayout.CENTER);
		panel_5.setLayout(new BorderLayout(0, 0));

		tableCompleta = new TTableCompleta(Funcionarios.class);
		tableCompleta.removeColumn("senha");
		panel_5.add(tableCompleta, BorderLayout.CENTER);
		atualizaTabelaFuncionarios();
	}

	public void limpar() {
		txtIdade.setText(null);
		txtNomefunc.setText(null);
		txtTelefone.setText(null);
		txtCpf.setText(null);
		textFieldBairro.setText(null);
		textFieldCidade.setText(null);
		textFieldComplemento.setText(null);
		textFieldNumero.setText(null);
		textFieldUF.setText(null);
		txtFieldEndereco.setText(null);
		txtRg.setText(null);
		txtTipoendereco.setText(null);
		txtfldCep.setText(null);
		txtLogin.setText(null);
		pwdSenha.setText(null);
		pwdSenha_1.setText(null);
		frmtdtxtfldDatanasc.setText(null);
		lblLad.setVisible(false);

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == btnExcluir) {
			do_btnExcluir_actionPerformed(arg0);
		}
		if (arg0.getSource() == btnGravar) {
			do_btnGravar_actionPerformed(arg0);
		}
	}

	Funcionarios funcionario;
	private JButton btnAlterarSenha;

	protected void do_btnGravar_actionPerformed(ActionEvent arg0) {

		String tipo;
		if (Tipo.getSelectedIndex() == 0) {
			tipo = "O";
		} else {
			tipo = "S";
		}

		Criptografar cript = new Criptografar();
		funcionario = new Funcionarios();

		String msg = " cadastrado com sucesso.";
		// Para saber quando estou fazendo um update ou insert
		if (alterar) {
			funcionario.setId(Integer.parseInt(textFieldCod.getText()));
			msg = " alterado com sucesso.";
			id = id - 1;
			alterar = false;
		}
		funcionario.setNome(txtNomefunc.getText());

		try {
			int dia = Integer.parseInt(frmtdtxtfldDatanasc.getText().substring(
					0, 2));
			int mes = Integer.parseInt(frmtdtxtfldDatanasc.getText().substring(
					3, 5));
			int ano = Integer.parseInt(frmtdtxtfldDatanasc.getText().substring(
					6, 10));

			funcionario.setDatanasc(new GregorianCalendar(ano, mes - 1, dia));
		} catch (java.lang.NumberFormatException ex) {

		}
		funcionario.setCpf(txtCpf.getText());
		funcionario.setRg(txtRg.getText());
		funcionario.setTelefone(txtTelefone.getText());
		funcionario.setBairro(textFieldBairro.getText());
		funcionario.setCep(txtfldCep.getText());
		funcionario.setRua(txtFieldEndereco.getText());
		funcionario.setCidade(textFieldCidade.getText());
		funcionario.setUf(textFieldUF.getText());
		funcionario.setNumero(textFieldNumero.getText());
		funcionario.setComplemento(textFieldComplemento.getText());
		funcionario.setIdade(Integer.parseInt(txtIdade.getText()));
		funcionario.setLogin(txtLogin.getText());

		if (alterar) {
			if (alterarSenha) {

				funcionario.setSenha(cript.criptografar(pwdSenha.getText(),
						"md5", 16));
			} else {
				funcionario.setSenha(((Funcionarios) tableCompleta
						.getSelecionado()).getSenha());
			}
		}else{
			funcionario.setSenha(cript.criptografar(pwdSenha.getText(),
					"md5", 16));			
		}
		funcionario.setTipo(tipo);

		if (validarCampos() == false) {
			GenericDao<Object> generic = new GenericDao<Object>();
			if (generic.salvar(funcionario) == true) {
				atualizaTabelaFuncionarios();
				id = id + 1;
				JOptionPane.showMessageDialog(null, "Funcionario "
						+ funcionario.getNome() + msg);
				limpar();
				textFieldCod.setText(String.valueOf(id));
				alterarSenha = false;

				lblSenha.setVisible(true);
				pwdSenha.setVisible(true);
				lblConfirmasenha.setVisible(true);
				pwdSenha_1.setVisible(true);
				btnAlterarSenha.setVisible(false);
			}

		}
	}

	public boolean validarCampos() {
		boolean validado = false;
		String CPF = txtCpf.getText().substring(0, 3)
				+ txtCpf.getText().substring(4, 7)
				+ txtCpf.getText().substring(8, 11)
				+ txtCpf.getText().substring(12, 14);

		if (txtNomefunc.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Campo Nome obrigatório");
			txtNomefunc.requestFocus();
			validado = true;
		} else if (ValidacaoCpf.isCPF(CPF) != true) {
			JOptionPane.showMessageDialog(null, "CPF Invalido", "INFORMATIVO",
					JOptionPane.INFORMATION_MESSAGE);
			txtCpf.setText(null);
			txtCpf.requestFocus();
			validado = true;
		} else if (txtLogin.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Campo Login obrigatório");
			txtLogin.requestFocus();
			validado = true;
		} else if (alterarSenha) {
			if (pwdSenha.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Campo senha obrigatório");
				validado = true;
			} else if (!pwdSenha.getText().equalsIgnoreCase(
					pwdSenha_1.getText())) {
				lblLad.setVisible(true);
				validado = true;
			}

		}

		return validado;

	}

	public void atualizaTabelaFuncionarios() {
		GenericDao<Object> cliDao = new GenericDao<Object>();
		tableCompleta.setDadosIniciais(cliDao.listar("Funcionarios"));
	}

	@Override
	public void focusGained(FocusEvent e) {
		if (e.getSource() == pwdSenha_1) {
			do_pwdSenha_1_focusGained(e);
		}
		if (e.getSource() == pwdSenha) {
			do_pwdSenha_focusGained(e);
		}
	}

	@Override
	public void focusLost(FocusEvent e) {
		if (e.getSource() == txtfldCep) {
			do_frmtdtxtfldCep_focusLost(e);
		}
		if (e.getSource() == frmtdtxtfldDatanasc) {
			do_frmtdtxtfldDatanasc_focusLost(e);
		}
	}

	protected void do_pwdSenha_focusGained(FocusEvent e) {
		lblLad.setVisible(false);
	}

	protected void do_pwdSenha_1_focusGained(FocusEvent e) {
		lblLad.setVisible(false);
	}

	protected void do_btnExcluir_actionPerformed(ActionEvent arg0) {
		try {
			GenericDao<Object> cdao = new GenericDao<Object>();
			if (JOptionPane.showConfirmDialog(
					null,
					"Deseja realmente excluir o funcionario "
							+ ((Funcionarios) tableCompleta.getSelecionado())
									.getNome(), "Excluir",
					JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				cdao.excluir(tableCompleta.getSelecionado());
				atualizaTabelaFuncionarios();
			}

		} catch (java.lang.NullPointerException e) {
			JOptionPane.showMessageDialog(null,
					"Selecione o Funcionario que deseja excluir!");

		}

	}

	protected void do_frmtdtxtfldDatanasc_focusLost(FocusEvent e) {
		try {
			int dia = Integer.parseInt(frmtdtxtfldDatanasc.getText().substring(
					0, 2));
			int mes = Integer.parseInt(frmtdtxtfldDatanasc.getText().substring(
					3, 5));
			int ano = Integer.parseInt(frmtdtxtfldDatanasc.getText().substring(
					6, 10));
			Calendar nasc = new GregorianCalendar(ano, mes - 1, dia);
			txtIdade.setText(String.valueOf(new Calendario()
					.calcularIdade(nasc)));
		} catch (java.lang.NumberFormatException ex) {
			MessageFactory
					.showError("Data inválida.\nInforme a data de nascimento! ");
			frmtdtxtfldDatanasc.requestFocus();
		}
	}

	protected void do_frmtdtxtfldCep_focusLost(FocusEvent e) {

		GenericDao<Endereco> listarEndereco = new GenericDao<Endereco>();
		List<Endereco> endereco = new ArrayList<Endereco>();
		try {
			endereco.add((Endereco) listarEndereco.listarEndereco("Endereco",
					txtfldCep.getText()).get(0));
			System.out.println(endereco.size());

			textFieldCidade.setText(endereco.get(0).getCidade());
			textFieldBairro.setText(endereco.get(0).getBairro());
			txtFieldEndereco.setText(endereco.get(0).getLogradouro());
			textFieldUF.setText(endereco.get(0).getUf());
			textFieldNumero.requestFocus();

		} catch (java.lang.IndexOutOfBoundsException e2) {
			Parametros p = new Parametros();
			if (Boolean.parseBoolean(p.lerArqConf("cep_internet"))) {
				buscaCepInternet();
				textFieldNumero.requestFocus();
			} else {
				JOptionPane.showMessageDialog(null,
						"CEP inexistente ou não cadastrado no banco de dados");
				textFieldCidade.setText(null);
				textFieldBairro.setText(null);
				txtFieldEndereco.setText(null);
				textFieldUF.setText(null);
				txtTipoendereco.setText(null);
			}
		}

	}

	protected void do_btnAlterar_actionPerformed(ActionEvent arg0) {
		if (tableCompleta.getSelecionado() != null) {
			funcionario = new Funcionarios();
			funcionario = (Funcionarios) tableCompleta.getSelecionado();
			Calendar data = funcionario.getDatanasc();
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

			txtNomefunc.setText(funcionario.getNome());
			textFieldCod.setText(String.valueOf(funcionario.getId()));
			txtCpf.setText(funcionario.getCpf());

			frmtdtxtfldDatanasc.setText(sdf.format(data.getTime()));
			txtIdade.setText(String.valueOf(funcionario.getIdade()));
			if (funcionario.getTipo().equalsIgnoreCase("S")) {
				Tipo.setSelectedIndex(1);
			} else {
				Tipo.setSelectedIndex(0);
			}
			txtLogin.setText(funcionario.getLogin());
			txtRg.setText(funcionario.getRg());
			txtTelefone.setText(funcionario.getTelefone());
			textFieldBairro.setText(funcionario.getBairro());
			txtfldCep.setText(funcionario.getCep());
			textFieldCidade.setText(funcionario.getCidade());
			textFieldComplemento.setText(funcionario.getComplemento());
			txtFieldEndereco.setText(funcionario.getRua());
			textFieldUF.setText(funcionario.getUf());
			textFieldNumero.setText(funcionario.getNumero());
			btnAlterarSenha.setVisible(true);
			pwdSenha.setVisible(false);
			pwdSenha_1.setVisible(false);
			lblSenha.setVisible(false);
			lblConfirmasenha.setVisible(false);
			alterar = true;
		}
	}

	public void buscaCepInternet() {

		// Faz a busca para o cep 58043-280
		WebServiceCep endereco = WebServiceCep.searchCep(txtfldCep.getText());
		// A ferramenta de busca ignora qualquer caracter que n?o seja n?mero.

		// caso a busca ocorra bem, imprime os resultados.
		if (endereco.wasSuccessful()) {
			textFieldCidade.setText(endereco.getCidade());
			textFieldBairro.setText(endereco.getBairro());
			textFieldUF.setText(endereco.getUf());
			txtTipoendereco.setText(endereco.getLogradouroType());
			txtFieldEndereco.setText(endereco.getLogradouro());

			// Gravar endereço localizado no banco de dados
			Endereco end = new Endereco();
			GenericDao<Endereco> endDao = new GenericDao<Endereco>();

			String uf_nome = null;
			String ufInternet = endereco.getUf();
			for (Uf uf : Uf.values()) {

				if (ufInternet.equalsIgnoreCase(String.valueOf(uf))) {
					uf_nome = uf.getUf_estenco();
					break;
				}

			}
			end.setCep(txtfldCep.getText());
			end.setCidade(endereco.getCidade());
			end.setBairro(endereco.getBairro());

			end.setUf_nome(uf_nome);
			end.setUf(endereco.getUf());
			end.setTp_logradouro(endereco.getLogradouroType());
			end.setLogradouro(endereco.getLogradouro());

			endDao.salvar(end);

			// caso haja problemas imprime as exceções.
		} else {
			JOptionPane.showMessageDialog(null, "Descrição do erro: "
					+ endereco.getResultText());
		}
	}
}