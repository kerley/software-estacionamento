/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package estacionamento.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.border.LineBorder;

import estacionamento.componente.TButton;
import estacionamento.componente.TComboBox;
import estacionamento.entidade.FormaCobranca;
import estacionamento.entidade.Preco;
import estacionamento.persistencia.GenericDao;
import estacionamento.persistencia.PrecoDao;
import framework.component.field.TTextField;
import framework.componente.TTable;

public class InternalPreco extends JInternalFrame {
	private JButton buttonCancelar;
	private JButton buttonSair;
	private TComboBox comboBoxFormaCobranca;
	private JLabel jLabel1;
	private JLabel jLabel2;
	private JLabel jLabel3;
	private JLabel jLabel4;
	private JLabel jLabel5;
	private JLabel jLabel6;
	private JPanel jPanel1;
	private JPanel panel_Veiculo;
	private JPanel jPanel4;
	private JPanel jPanel6;
	private TTextField fieldTolerancia;
	private TTextField fieldId;
	private TTextField fieldDescricao;
	private TTextField fieldValor;
	private boolean aberto = false;
	private final TTable<Preco> tablePreco = new TTable<Preco>(Preco.class);
	PrecoDao precoDao = new PrecoDao();
	private JPanel panel;
	private JPanel panel_1;
	private JButton btnAlterarVeiculo;
	private JButton btnSalvarVeiculo;
	private TButton button;

	public boolean isAberto() {
		return aberto;
	}

	public void setAberto(boolean aberto) {
		this.aberto = aberto;
	}

	public InternalPreco() {
		initComponents();

	}

	@SuppressWarnings("unchecked")
	private void initComponents() {

		jPanel1 = new javax.swing.JPanel();
		jLabel1 = new javax.swing.JLabel();
		comboBoxFormaCobranca = new TComboBox();
		jLabel2 = new javax.swing.JLabel();
		fieldTolerancia = new TTextField();
		fieldTolerancia.setEditable(false);
		jLabel3 = new javax.swing.JLabel();
		panel_Veiculo = new javax.swing.JPanel();
		jLabel4 = new javax.swing.JLabel();
		jLabel4.setFont(new Font("Tahoma", Font.PLAIN, 14));
		jLabel5 = new javax.swing.JLabel();
		jLabel5.setFont(new Font("Tahoma", Font.PLAIN, 14));
		fieldId = new TTextField();
		fieldId.setEditable(false);
		fieldId.setFont(new Font("Tahoma", Font.PLAIN, 14));
		fieldDescricao = new TTextField();
		fieldDescricao.setEditable(false);
		fieldDescricao.setFont(new Font("Tahoma", Font.PLAIN, 18));
		jLabel6 = new javax.swing.JLabel();
		jLabel6.setFont(new Font("Tahoma", Font.PLAIN, 14));
		fieldValor = new TTextField();
		fieldValor.setTipoDado(1);
		fieldValor.setFont(new Font("Tahoma", Font.PLAIN, 18));
		jPanel6 = new javax.swing.JPanel();

		buttonCancelar = new TButton();
		buttonCancelar.setFont(new Font("Tahoma", Font.PLAIN, 12));
		buttonCancelar.setBounds(373, 5, 110, 31);
		buttonCancelar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				do_buttonCancelar_actionPerformed(arg0);
			}
		});
		buttonCancelar.setIcon(new ImageIcon(InternalPreco.class
				.getResource("/img/cancel.png")));

		buttonSair = new TButton();
		buttonSair.setFont(new Font("Tahoma", Font.PLAIN, 12));
		buttonSair.setBounds(495, 5, 110, 31);
		buttonSair.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				do_buttonSair_actionPerformed(e);
			}
		});
		buttonSair.setIcon(new ImageIcon(InternalPreco.class
				.getResource("/img/Sair.png")));

		setTitle("Alteração de Preços");

		jLabel1.setText("FORMA DE COBRANÇA:");

		jLabel2.setText("TEMPO DE TOLER\u00C2NCIA:");

		jLabel3.setText("min.");

		panel_Veiculo.setBorder(javax.swing.BorderFactory.createTitledBorder(
				new javax.swing.border.SoftBevelBorder(
						javax.swing.border.BevelBorder.RAISED), "VEICULO:"));

		jLabel4.setText("ID:");

		jLabel5.setText("DESCRIÇÃO:");

		jLabel6.setText("VALOR:");

		panel_1 = new JPanel();
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));

		btnSalvarVeiculo = new JButton("Salvar");
		btnSalvarVeiculo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnSalvarVeiculo.setBounds(289, 10, 104, 30);
		btnSalvarVeiculo.setIcon(new ImageIcon(InternalPreco.class
				.getResource("/img/save.png")));
		btnSalvarVeiculo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				do_btnSalvarVeiculo_actionPerformed(e);
			}
		});
		panel_1.setLayout(null);
		panel_1.add(btnSalvarVeiculo);

		javax.swing.GroupLayout gl_jPanel1 = new javax.swing.GroupLayout(
				jPanel1);
		gl_jPanel1.setHorizontalGroup(
			gl_jPanel1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_jPanel1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_jPanel1.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_Veiculo, GroupLayout.DEFAULT_SIZE, 814, Short.MAX_VALUE)
						.addGroup(gl_jPanel1.createSequentialGroup()
							.addComponent(jLabel1)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(comboBoxFormaCobranca, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
							.addGap(37)
							.addComponent(jLabel2)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(fieldTolerancia, GroupLayout.PREFERRED_SIZE, 79, GroupLayout.PREFERRED_SIZE)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(jLabel3)))
					.addContainerGap())
		);
		gl_jPanel1.setVerticalGroup(
			gl_jPanel1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_jPanel1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_jPanel1.createParallelGroup(Alignment.BASELINE)
						.addComponent(jLabel1)
						.addComponent(comboBoxFormaCobranca, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(jLabel2)
						.addComponent(fieldTolerancia, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(jLabel3))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(panel_Veiculo, GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE)
					.addContainerGap())
		);

		button = new TButton();
		button.setFont(new Font("Tahoma", Font.PLAIN, 12));
		button.setBounds(400, 10, 104, 30);
		button.setIcon(new ImageIcon(InternalPreco.class
				.getResource("/img/cancel.png")));
		button.setText("Cancelar");
		panel_1.add(button);
		GroupLayout gl_panel_Veiculo = new GroupLayout(panel_Veiculo);
		gl_panel_Veiculo.setHorizontalGroup(
			gl_panel_Veiculo.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_Veiculo.createSequentialGroup()
					.addGap(3)
					.addGroup(gl_panel_Veiculo.createParallelGroup(Alignment.LEADING)
						.addComponent(panel_1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addGroup(gl_panel_Veiculo.createSequentialGroup()
							.addComponent(jLabel4, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
							.addGap(86)
							.addComponent(jLabel5, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
							.addGap(398)
							.addComponent(jLabel6, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_Veiculo.createSequentialGroup()
							.addComponent(fieldId, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE)
							.addGap(18)
							.addComponent(fieldDescricao, GroupLayout.PREFERRED_SIZE, 472, GroupLayout.PREFERRED_SIZE)
							.addGap(10)
							.addComponent(fieldValor, GroupLayout.PREFERRED_SIZE, 184, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap())
		);
		gl_panel_Veiculo.setVerticalGroup(
			gl_panel_Veiculo.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_Veiculo.createSequentialGroup()
					.addGap(15)
					.addGroup(gl_panel_Veiculo.createParallelGroup(Alignment.LEADING)
						.addComponent(jLabel4, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
						.addComponent(jLabel5, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
						.addComponent(jLabel6, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE))
					.addGap(6)
					.addGroup(gl_panel_Veiculo.createParallelGroup(Alignment.LEADING)
						.addComponent(fieldId, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
						.addComponent(fieldDescricao, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
						.addComponent(fieldValor, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE))
					.addGap(19)
					.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(20, Short.MAX_VALUE))
		);
		panel_Veiculo.setLayout(gl_panel_Veiculo);
		jPanel1.setLayout(gl_jPanel1);

		buttonCancelar.setText("Cancelar");

		buttonSair.setText("Sair");

		panel = new JPanel();
		panel.setLayout(new BorderLayout(0, 0));
		tablePreco.removeColumn("id");
		panel.add(tablePreco);

		javax.swing.GroupLayout layout = new javax.swing.GroupLayout(
				getContentPane());
		layout.setHorizontalGroup(
			layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(layout.createSequentialGroup()
					.addGroup(layout.createParallelGroup(Alignment.TRAILING)
						.addGroup(layout.createSequentialGroup()
							.addGap(10)
							.addComponent(panel, GroupLayout.DEFAULT_SIZE, 824, Short.MAX_VALUE))
						.addComponent(jPanel1, GroupLayout.DEFAULT_SIZE, 834, Short.MAX_VALUE))
					.addGap(18))
				.addGroup(Alignment.LEADING, layout.createSequentialGroup()
					.addGap(10)
					.addComponent(jPanel6, GroupLayout.DEFAULT_SIZE, 832, Short.MAX_VALUE)
					.addContainerGap())
		);
		layout.setVerticalGroup(
			layout.createParallelGroup(Alignment.TRAILING)
				.addGroup(layout.createSequentialGroup()
					.addComponent(jPanel1, GroupLayout.PREFERRED_SIZE, 224, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(panel, GroupLayout.DEFAULT_SIZE, 346, Short.MAX_VALUE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(jPanel6, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
					.addContainerGap())
		);
		getContentPane().setLayout(layout);

		btnAlterarVeiculo = new JButton("Alterar");
		btnAlterarVeiculo.setFont(new Font("Tahoma", Font.PLAIN, 12));
		btnAlterarVeiculo.setBounds(253, 5, 110, 31);
		btnAlterarVeiculo.setIcon(new ImageIcon(InternalPreco.class
				.getResource("/img/alterar.gif")));
		btnAlterarVeiculo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				do_btnAlterarVeiculo_actionPerformed(e);
			}
		});
		jPanel6.setLayout(null);
		jPanel6.add(btnAlterarVeiculo);
		jPanel6.add(buttonCancelar);
		jPanel6.add(buttonSair);

		pack();
		listarFormaCobranca();
		atualizaTabelaPreco();
	}

	public void atualizaTabelaPreco() {
		GenericDao<Object> precoDao = new GenericDao<Object>();
		tablePreco.refreshData(precoDao.listar("Preco"));

	}

	public List<FormaCobranca> listarFormaCobranca() {
		comboBoxFormaCobranca.removeAllItems();
		GenericDao<Object> formaCobrancaDao = new GenericDao<Object>();

		List<FormaCobranca> listaFC = formaCobrancaDao.listar("FormaCobranca");
		for (int i = 0; i < listaFC.size(); i++) {
			if (listaFC.get(i).isAtivo() == true)
				comboBoxFormaCobranca.addItem(listaFC.get(i));

		}
		opcaoTela();
		return listaFC;
	}

	// ação do combobox
	public void opcaoTela() {
		if (comboBoxFormaCobranca.getSelectedItem().toString()
				.equals("Fracoes de 15 Minutos")) {

		}

	}

	protected void do_buttonCancelar_actionPerformed(ActionEvent arg0) {
		aberto = false;
		dispose();

	}

	protected void do_buttonSair_actionPerformed(ActionEvent e) {
		aberto = false;
		dispose();
	}

	protected void do_btnAlterarVeiculo_actionPerformed(ActionEvent e) {
		if (tablePreco.getSelecionado() != null) {

			fieldId.setText(String.valueOf(tablePreco.getSelecionado().getId()));
			fieldDescricao.setText(tablePreco.getSelecionado().getDescricao());
			fieldValor.setText(String.valueOf(tablePreco.getSelecionado()
					.getValor()));
			fieldValor.requestFocus();
		} else {
			JOptionPane.showMessageDialog(null,
					"Selecione o preço que deseja alterar!");
		}

	}

	protected void do_btnSalvarVeiculo_actionPerformed(ActionEvent e) {

		try {
			GenericDao<Object> precoDao = new GenericDao<Object>();

			Preco preco = new Preco();
			preco.setId(Long.parseLong(fieldId.getText()));
			preco.setDescricao(fieldDescricao.getText());
			preco.setValor(Double.parseDouble(fieldValor.getText()));

			precoDao.salvar(preco);
			atualizaTabelaPreco();
			fieldDescricao.setText(null);
			fieldId.setText(null);
			fieldValor.setText(null);
			JOptionPane.showMessageDialog(null, "Preço alterado com sucesso!");
		} catch (java.lang.NumberFormatException exc) {
			JOptionPane.showMessageDialog(null, "Informe um valor válido!");

		}
	}
}
