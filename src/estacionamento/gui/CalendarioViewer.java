package estacionamento.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import com.toedter.calendar.JCalendar;

public class CalendarioViewer extends JDialog {

	private final JPanel contentPanel = new JPanel();
	public CalendarioViewer() {
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		setResizable(false);
		setModal(true);
		setBounds(100, 100, 297, 183);
		getContentPane().setLayout(new BorderLayout(0, 0));
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel);
		{
			JCalendar calendar = new JCalendar();
			getContentPane().add(calendar);
		}
	}

}
