package estacionamento.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

import estacionamento.entidade.Estacionamento;
import estacionamento.entidade.TipoCliente;
import estacionamento.entidade.Veiculo;
import estacionamento.persistencia.GenericDao;
import estacionamento.sistema.CalculaPreco;
import framework.component.field.TTextField;
import framework.util.MessageFactory;

public class InternalFechamentoSaida extends JInternalFrame {

	private final JPanel contentPanel = new JPanel();
	private final JLabel lblPlaca;
	private final TTextField fieldPlaca;
	private final JLabel lblModelo;
	private final TTextField fieldModelo;
	private final JLabel lblCor;
	private final TTextField fieldCor;
	private final JLabel lblEntrada;
	private final JLabel lblSada;
	private final JLabel lblPenmancia;
	private final JTextField fieldEntrada;
	private final JTextField fieldSaida;
	private final TTextField fieldPermanencia;
	private final JComboBox comboBoxFormaCobranca;
	private final JLabel lblTipo;
	private final JPanel panel;
	private final JLabel lblValorEstacinamento;
	private final JLabel lblValorServoos;
	private final JLabel lblValorTotal;
	private final JLabel lblValorPago;
	private final JLabel lblTroco;
	private final JRadioButton rdbtnServios;
	private final TTextField fieldValorEstacionamento;
	private final TTextField textField_5;
	private final TTextField fieldValorTotal;
	private final TTextField fieldValorPago;
	private final TTextField fieldTroco;
	private final JPanel panel_1;
	private JPanel buttonPane;
	private final JPanel panel_2;
	private final JLabel lblDesconto;
	private final TTextField fieldDesconto;
	private final JLabel lblJuros;
	private final TTextField fieldJuros;
	private boolean aberto = false;
	private final JLabel lblDataEntrada;
	private final JTextField fieldDataEntrada;
	private final JLabel lblDataSaida;
	private final JTextField fieldDataSaida;
	DecimalFormat decimalFormat = new DecimalFormat("0.##");
	private final JPanel panelCliente;
	private final JLabel labelNome;
	private final TTextField fieldNomeCliente;
	private final JLabel lblCpf;
	private final TTextField fieldCpf;
	private final JLabel labelFone;
	private final TTextField fieldTelefone;
	private final JLabel lblEndereo;
	private final TTextField fieldEndereco;
	private final JLabel lblNumero;
	private final TTextField fieldNumero;
	private final JLabel lblCidade;
	private final TTextField fieldCidade;
	private final JLabel lblBairro;
	private final TTextField fieldBairro;
	private final JLabel lblTipoDeCliente;
	private final TTextField fieldTipoCliente;
	private final JLabel lblCelular;
	private final TTextField fieldCelular;
	private JButton buttonFechar;
	private JButton buttonCancel;
	private int typeClient;
	private long qtdDias = 0;
	private final JButton btnSubtotal;
	private final JButton btnTotal;
	private double desconto = 0;
	private double juros = 0;
	private double valorTotal = 0;
	private double valorPago = 0;
	private double troco = 0;
	private boolean finaliza = false;
	private final JLabel lblDadosDoCliente;
	private Estacionamento saidaAtual;

	public boolean isAberto() {
		return aberto;
	}

	public void setAberto(boolean aberto) {
		this.aberto = aberto;
	}

	public InternalFechamentoSaida() {
		getContentPane().setBackground(SystemColor.activeCaption);
		setTitle("Fechamento");
		setBounds(100, 100, 1100, 590);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBackground(UIManager.getColor("activeCaption"));
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);

		lblPlaca = new JLabel("PLACA:");
		lblPlaca.setBounds(15, 16, 64, 14);
		lblPlaca.setFont(new Font("Tahoma", Font.BOLD, 11));

		fieldPlaca = new TTextField();
		fieldPlaca.setBounds(15, 33, 144, 30);
		fieldPlaca.setBackground(new Color(255, 255, 255));
		fieldPlaca.setEditable(false);
		fieldPlaca.setFont(new Font("Tahoma", Font.PLAIN, 14));
		fieldPlaca.setForeground(new Color(255, 0, 0));
		fieldPlaca.setColumns(10);

		lblModelo = new JLabel("MODELO:");
		lblModelo.setBounds(169, 16, 103, 14);
		lblModelo.setFont(new Font("Tahoma", Font.BOLD, 11));

		fieldModelo = new TTextField();
		fieldModelo.setBounds(169, 33, 398, 30);
		fieldModelo.setBackground(new Color(255, 255, 255));
		fieldModelo.setEditable(false);
		fieldModelo.setFont(new Font("Tahoma", Font.PLAIN, 14));
		fieldModelo.setForeground(new Color(255, 0, 0));
		fieldModelo.setColumns(10);

		comboBoxFormaCobranca = new JComboBox();
		comboBoxFormaCobranca.setToolTipText("Selecione a forma de cobranca!");
		comboBoxFormaCobranca.setBounds(169, 197, 153, 20);
		comboBoxFormaCobranca.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				do_comboBoxFormaCobranca_actionPerformed(arg0);
			}
		});

		lblTipo = new JLabel("FORMA DE COBRAÇA:");
		lblTipo.setBounds(17, 200, 131, 14);

		panel = new JPanel();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		panel.setBounds(5, 235, 767, 263);
		panel.setBackground(UIManager.getColor("activeCaption"));

		rdbtnServios = new JRadioButton("SERVI\u00C7OS:");
		rdbtnServios.setEnabled(false);
		rdbtnServios.setBounds(454, 196, 113, 23);
		rdbtnServios.setBackground(UIManager.getColor("activeCaption"));

		panel_1 = new JPanel();
		panel_1.setForeground(Color.WHITE);
		panel_1.setBounds(5, 74, 765, 105);
		panel_1.setBackground(UIManager.getColor("activeCaption"));
		panel_1.setBorder(new LineBorder(Color.WHITE));

		lblCor = new JLabel("COR:");
		lblCor.setBounds(584, 16, 53, 14);
		lblCor.setFont(new Font("Tahoma", Font.BOLD, 11));

		fieldCor = new TTextField();
		fieldCor.setBounds(584, 33, 174, 30);
		fieldCor.setBackground(new Color(255, 255, 255));
		fieldCor.setFont(new Font("Tahoma", Font.PLAIN, 14));
		fieldCor.setForeground(new Color(255, 0, 0));
		fieldCor.setEditable(false);
		fieldCor.setColumns(10);
		{
			buttonPane = new JPanel();
			buttonPane.setBounds(5, 505, 1069, 48);
			buttonPane.setBackground(UIManager.getColor("activeCaption"));
			buttonPane.setBorder(new LineBorder(Color.WHITE));
			{
				buttonFechar = new JButton("Fechar");
				buttonFechar.setIcon(new ImageIcon(
						InternalFechamentoSaida.class
								.getResource("/img/Sair.png")));
				buttonFechar.setBounds(843, 7, 104, 36);
				buttonFechar.setFont(new Font("Tahoma", Font.PLAIN, 16));
				buttonFechar.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						do_fechar_actionPerformed(arg0);
					}
				});
				buttonFechar.setActionCommand("OK");
				getRootPane().setDefaultButton(buttonFechar);
			}
			{
				buttonCancel = new JButton("Cancel");
				buttonCancel.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent arg0) {
						do_buttonCancel_actionPerformed(arg0);
					}
				});
				buttonCancel.setIcon(new ImageIcon(
						InternalFechamentoSaida.class
								.getResource("/img/fechar.gif")));
				buttonCancel.setBounds(957, 7, 104, 36);
				buttonCancel.setFont(new Font("Tahoma", Font.PLAIN, 16));
				buttonCancel.setActionCommand("Cancel");
			}
		}

		lblEntrada = new JLabel("HORA ENTRADA:");
		lblEntrada.setBounds(11, 15, 143, 17);
		lblEntrada.setFont(new Font("Tahoma", Font.BOLD, 14));

		fieldEntrada = new JTextField();
		fieldEntrada.setBounds(164, 10, 156, 26);
		fieldEntrada.setBackground(new Color(255, 255, 255));
		fieldEntrada.setEditable(false);
		fieldEntrada.setFont(new Font("Tahoma", Font.BOLD, 16));
		fieldEntrada.setColumns(10);

		fieldSaida = new JTextField();
		fieldSaida.setBounds(579, 10, 177, 26);
		fieldSaida.setBackground(new Color(255, 255, 255));
		fieldSaida.setEditable(false);
		fieldSaida.setFont(new Font("Tahoma", Font.BOLD, 16));
		fieldSaida.setColumns(10);

		lblSada = new JLabel("HORA SAÍDA:");
		lblSada.setBounds(451, 15, 118, 17);
		lblSada.setFont(new Font("Tahoma", Font.BOLD, 14));

		fieldDataEntrada = new JTextField();
		fieldDataEntrada.setBounds(164, 65, 156, 26);
		fieldDataEntrada.setFont(new Font("Tahoma", Font.BOLD, 16));
		fieldDataEntrada.setEditable(false);
		fieldDataEntrada.setColumns(10);
		fieldDataEntrada.setBackground(new Color(255, 255, 255));

		lblDataEntrada = new JLabel("DATA ENTRADA:");
		lblDataEntrada.setBounds(11, 70, 143, 17);
		lblDataEntrada.setFont(new Font("Tahoma", Font.BOLD, 14));

		lblDataSaida = new JLabel("DATA SAIDA:");
		lblDataSaida.setBounds(451, 70, 118, 17);
		lblDataSaida.setFont(new Font("Tahoma", Font.BOLD, 14));

		fieldDataSaida = new JTextField();
		fieldDataSaida.setBounds(579, 65, 177, 26);
		fieldDataSaida.setFont(new Font("Tahoma", Font.BOLD, 16));
		fieldDataSaida.setEditable(false);
		fieldDataSaida.setColumns(10);
		fieldDataSaida.setBackground(new Color(255, 255, 255));

		lblValorEstacinamento = new JLabel("VALOR ESTACINAMENTO:");
		lblValorEstacinamento.setBounds(17, 21, 147, 14);

		lblValorServoos = new JLabel("VALOR SERVI\u00C7OS:");
		lblValorServoos.setBounds(17, 82, 147, 14);

		fieldValorEstacionamento = new TTextField();
		fieldValorEstacionamento.setHorizontalAlignment(SwingConstants.RIGHT);
		fieldValorEstacionamento.setForeground(Color.RED);
		fieldValorEstacionamento.setFont(new Font("Tahoma", Font.BOLD, 16));
		fieldValorEstacionamento.setSize(158, 20);
		fieldValorEstacionamento.setLocation(164, 18);
		fieldValorEstacionamento.setEditable(false);
		fieldValorEstacionamento.setColumns(10);

		textField_5 = new TTextField();
		textField_5.setHorizontalAlignment(SwingConstants.RIGHT);
		textField_5.setSize(158, 20);
		textField_5.setLocation(164, 77);
		textField_5.setEditable(false);
		textField_5.setColumns(10);

		panel_2 = new JPanel();
		panel_2.setBounds(10, 135, 738, 116);
		panel_2.setBackground(UIManager.getColor("activeCaption"));
		panel_2.setBorder(new LineBorder(Color.WHITE));

		lblDesconto = new JLabel("DESCONTO:");
		lblDesconto.setBounds(17, 52, 147, 14);

		fieldDesconto = new TTextField();
		fieldDesconto.setHorizontalAlignment(SwingConstants.RIGHT);
		fieldDesconto.setForeground(Color.RED);
		fieldDesconto.setSize(158, 20);
		fieldDesconto.setLocation(164, 49);
		fieldDesconto.setColumns(10);

		lblJuros = new JLabel("JUROS:");
		lblJuros.setBounds(17, 110, 147, 14);

		fieldJuros = new TTextField();
		fieldJuros.setHorizontalAlignment(SwingConstants.RIGHT);
		fieldJuros.setForeground(Color.RED);
		fieldJuros.setSize(158, 20);
		fieldJuros.setLocation(164, 107);
		fieldJuros.setColumns(10);

		lblValorTotal = new JLabel("VALOR TOTAL:");
		lblValorTotal.setBounds(11, 27, 137, 22);
		lblValorTotal.setFont(new Font("Tahoma", Font.PLAIN, 18));

		fieldValorTotal = new TTextField();
		fieldValorTotal.setHorizontalAlignment(SwingConstants.RIGHT);
		fieldValorTotal.setBounds(158, 24, 164, 28);
		fieldValorTotal.setBackground(new Color(255, 255, 255));
		fieldValorTotal.setEditable(false);
		fieldValorTotal.setFont(new Font("Tahoma", Font.PLAIN, 18));
		fieldValorTotal.setColumns(10);

		lblValorPago = new JLabel("VALOR PAGO:");
		lblValorPago.setBounds(11, 73, 137, 22);
		lblValorPago.setFont(new Font("Tahoma", Font.PLAIN, 18));

		fieldValorPago = new TTextField();
		fieldValorPago.setHorizontalAlignment(SwingConstants.RIGHT);
		fieldValorPago.setForeground(Color.BLUE);
		fieldValorPago.setBounds(158, 70, 164, 28);
		fieldValorPago.setFont(new Font("Tahoma", Font.BOLD, 28));
		fieldValorPago.setColumns(10);

		lblTroco = new JLabel("TROCO:");
		lblTroco.setBounds(438, 66, 96, 22);
		lblTroco.setFont(new Font("Tahoma", Font.PLAIN, 18));

		fieldTroco = new TTextField();
		fieldTroco.setForeground(Color.BLUE);
		fieldTroco.setHorizontalAlignment(SwingConstants.RIGHT);
		fieldTroco.setEditable(false);
		fieldTroco.setBounds(544, 63, 184, 28);
		fieldTroco.setFont(new Font("Tahoma", Font.PLAIN, 18));
		fieldTroco.setColumns(10);

		lblPenmancia = new JLabel("PERMAN\u00CANCIA:");
		lblPenmancia.setBounds(451, 20, 134, 17);
		lblPenmancia.setFont(new Font("Tahoma", Font.BOLD, 14));

		fieldPermanencia = new TTextField();
		fieldPermanencia.setHorizontalAlignment(SwingConstants.RIGHT);
		fieldPermanencia.setSize(177, 26);
		fieldPermanencia.setLocation(579, 15);
		fieldPermanencia.setBackground(new Color(255, 255, 255));
		fieldPermanencia.setEditable(false);
		fieldPermanencia.setFont(new Font("Tahoma", Font.BOLD, 16));
		fieldPermanencia.setColumns(10);
		panel_2.setLayout(null);
		panel_2.add(lblValorTotal);
		panel_2.add(lblValorPago);
		panel_2.add(fieldValorPago);
		panel_2.add(lblTroco);
		panel_2.add(fieldTroco);
		panel_2.add(fieldValorTotal);

		panelCliente = new JPanel();
		panelCliente.setForeground(Color.WHITE);
		panelCliente.setBounds(782, 16, 292, 481);
		panelCliente.setBackground(UIManager.getColor("activeCaption"));
		panelCliente.setBorder(new LineBorder(new Color(0, 0, 0)));

		labelNome = new JLabel("Nome:");

		fieldNomeCliente = new TTextField();
		fieldNomeCliente.setEditable(false);
		fieldNomeCliente.setBackground(new Color(255, 255, 255));
		fieldNomeCliente.setForeground(new Color(255, 0, 0));
		fieldNomeCliente.setColumns(10);

		lblCpf = new JLabel("CPF:");

		fieldCpf = new TTextField();
		fieldCpf.setEditable(false);
		fieldCpf.setBackground(new Color(255, 255, 255));
		fieldCpf.setForeground(new Color(255, 0, 0));
		fieldCpf.setColumns(10);

		labelFone = new JLabel("Telefone:");

		fieldTelefone = new TTextField();
		fieldTelefone.setEditable(false);
		fieldTelefone.setBackground(new Color(255, 255, 255));
		fieldTelefone.setForeground(new Color(255, 0, 0));
		fieldTelefone.setColumns(10);

		lblEndereo = new JLabel("Endereço:");

		fieldEndereco = new TTextField();
		fieldEndereco.setEditable(false);
		fieldEndereco.setBackground(new Color(255, 255, 255));
		fieldEndereco.setForeground(new Color(255, 0, 0));
		fieldEndereco.setColumns(10);

		lblNumero = new JLabel("Numero:");

		fieldNumero = new TTextField();
		fieldNumero.setEditable(false);
		fieldNumero.setBackground(new Color(255, 255, 255));
		fieldNumero.setForeground(new Color(255, 0, 0));
		fieldNumero.setColumns(10);

		lblCidade = new JLabel("Cidade:");

		fieldCidade = new TTextField();
		fieldCidade.setEditable(false);
		fieldCidade.setBackground(new Color(255, 255, 255));
		fieldCidade.setForeground(new Color(255, 0, 0));
		fieldCidade.setColumns(10);

		lblBairro = new JLabel("Bairro:");

		fieldBairro = new TTextField();
		fieldBairro.setEditable(false);
		fieldBairro.setBackground(new Color(255, 255, 255));
		fieldBairro.setForeground(new Color(255, 0, 0));
		fieldBairro.setColumns(10);

		lblTipoDeCliente = new JLabel("Tipo de Cliente");

		fieldTipoCliente = new TTextField();
		fieldTipoCliente.setEditable(false);
		fieldTipoCliente.setBackground(new Color(255, 255, 255));
		fieldTipoCliente.setForeground(new Color(255, 0, 0));
		fieldTipoCliente.setColumns(10);

		lblCelular = new JLabel("Celular:");

		fieldCelular = new TTextField();
		fieldCelular.setEditable(false);
		fieldCelular.setBackground(new Color(255, 255, 255));
		fieldCelular.setForeground(new Color(255, 0, 0));
		fieldCelular.setColumns(10);

		lblDadosDoCliente = new JLabel(" Dados do Cliente:");
		GroupLayout gl_panelCliente = new GroupLayout(panelCliente);
		gl_panelCliente
				.setHorizontalGroup(gl_panelCliente
						.createParallelGroup(Alignment.LEADING)
						.addGroup(
								gl_panelCliente
										.createSequentialGroup()
										.addGroup(
												gl_panelCliente
														.createParallelGroup(
																Alignment.LEADING)
														.addGroup(
																gl_panelCliente
																		.createSequentialGroup()
																		.addGap(5)
																		.addComponent(
																				labelNome,
																				GroupLayout.PREFERRED_SIZE,
																				46,
																				GroupLayout.PREFERRED_SIZE))
														.addGroup(
																gl_panelCliente
																		.createSequentialGroup()
																		.addGap(5)
																		.addComponent(
																				fieldNomeCliente,
																				GroupLayout.DEFAULT_SIZE,
																				254,
																				Short.MAX_VALUE))
														.addGroup(
																gl_panelCliente
																		.createSequentialGroup()
																		.addGap(5)
																		.addComponent(
																				lblCpf,
																				GroupLayout.PREFERRED_SIZE,
																				46,
																				GroupLayout.PREFERRED_SIZE))
														.addGroup(
																gl_panelCliente
																		.createSequentialGroup()
																		.addGap(5)
																		.addComponent(
																				fieldCpf,
																				GroupLayout.DEFAULT_SIZE,
																				254,
																				Short.MAX_VALUE))
														.addGroup(
																gl_panelCliente
																		.createSequentialGroup()
																		.addGap(5)
																		.addComponent(
																				labelFone,
																				GroupLayout.PREFERRED_SIZE,
																				74,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(59)
																		.addComponent(
																				lblCelular,
																				GroupLayout.PREFERRED_SIZE,
																				81,
																				GroupLayout.PREFERRED_SIZE))
														.addGroup(
																gl_panelCliente
																		.createSequentialGroup()
																		.addGap(5)
																		.addComponent(
																				fieldTelefone,
																				GroupLayout.DEFAULT_SIZE,
																				115,
																				Short.MAX_VALUE)
																		.addGap(23)
																		.addComponent(
																				fieldCelular,
																				GroupLayout.DEFAULT_SIZE,
																				116,
																				Short.MAX_VALUE))
														.addGroup(
																gl_panelCliente
																		.createSequentialGroup()
																		.addGap(5)
																		.addComponent(
																				lblEndereo,
																				GroupLayout.PREFERRED_SIZE,
																				74,
																				GroupLayout.PREFERRED_SIZE))
														.addGroup(
																gl_panelCliente
																		.createSequentialGroup()
																		.addGap(5)
																		.addComponent(
																				fieldEndereco,
																				GroupLayout.DEFAULT_SIZE,
																				254,
																				Short.MAX_VALUE))
														.addGroup(
																gl_panelCliente
																		.createSequentialGroup()
																		.addGap(5)
																		.addComponent(
																				lblNumero,
																				GroupLayout.PREFERRED_SIZE,
																				74,
																				GroupLayout.PREFERRED_SIZE)
																		.addGap(10)
																		.addComponent(
																				lblBairro,
																				GroupLayout.PREFERRED_SIZE,
																				74,
																				GroupLayout.PREFERRED_SIZE))
														.addGroup(
																gl_panelCliente
																		.createSequentialGroup()
																		.addGap(5)
																		.addComponent(
																				fieldCidade,
																				GroupLayout.DEFAULT_SIZE,
																				254,
																				Short.MAX_VALUE))
														.addGroup(
																gl_panelCliente
																		.createSequentialGroup()
																		.addGap(4)
																		.addComponent(
																				lblTipoDeCliente,
																				GroupLayout.PREFERRED_SIZE,
																				110,
																				GroupLayout.PREFERRED_SIZE))
														.addGroup(
																gl_panelCliente
																		.createSequentialGroup()
																		.addGap(4)
																		.addComponent(
																				fieldTipoCliente,
																				GroupLayout.DEFAULT_SIZE,
																				254,
																				Short.MAX_VALUE)
																		.addGap(1))
														.addGroup(
																gl_panelCliente
																		.createSequentialGroup()
																		.addGap(5)
																		.addGroup(
																				gl_panelCliente
																						.createParallelGroup(
																								Alignment.TRAILING)
																						.addComponent(
																								lblCidade,
																								Alignment.LEADING,
																								GroupLayout.DEFAULT_SIZE,
																								80,
																								Short.MAX_VALUE)
																						.addComponent(
																								fieldNumero,
																								Alignment.LEADING,
																								GroupLayout.DEFAULT_SIZE,
																								80,
																								Short.MAX_VALUE))
																		.addGap(10)
																		.addComponent(
																				fieldBairro,
																				GroupLayout.DEFAULT_SIZE,
																				164,
																				Short.MAX_VALUE)))
										.addGap(21))
						.addComponent(lblDadosDoCliente,
								GroupLayout.DEFAULT_SIZE, 290, Short.MAX_VALUE));
		gl_panelCliente.setVerticalGroup(gl_panelCliente.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_panelCliente
						.createSequentialGroup()
						.addComponent(lblDadosDoCliente,
								GroupLayout.PREFERRED_SIZE, 21,
								GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(labelNome, GroupLayout.PREFERRED_SIZE,
								14, GroupLayout.PREFERRED_SIZE)
						.addGap(11)
						.addComponent(fieldNomeCliente,
								GroupLayout.PREFERRED_SIZE, 20,
								GroupLayout.PREFERRED_SIZE)
						.addGap(10)
						.addComponent(lblCpf, GroupLayout.PREFERRED_SIZE, 14,
								GroupLayout.PREFERRED_SIZE)
						.addGap(11)
						.addComponent(fieldCpf, GroupLayout.PREFERRED_SIZE, 20,
								GroupLayout.PREFERRED_SIZE)
						.addGap(15)
						.addGroup(
								gl_panelCliente
										.createParallelGroup(Alignment.LEADING)
										.addComponent(labelFone,
												GroupLayout.PREFERRED_SIZE, 14,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(lblCelular,
												GroupLayout.PREFERRED_SIZE, 14,
												GroupLayout.PREFERRED_SIZE))
						.addGap(11)
						.addGroup(
								gl_panelCliente
										.createParallelGroup(Alignment.LEADING)
										.addComponent(fieldTelefone,
												GroupLayout.PREFERRED_SIZE, 20,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(fieldCelular,
												GroupLayout.PREFERRED_SIZE, 20,
												GroupLayout.PREFERRED_SIZE))
						.addGap(21)
						.addComponent(lblEndereo, GroupLayout.PREFERRED_SIZE,
								14, GroupLayout.PREFERRED_SIZE)
						.addGap(11)
						.addComponent(fieldEndereco,
								GroupLayout.PREFERRED_SIZE, 20,
								GroupLayout.PREFERRED_SIZE)
						.addGap(12)
						.addGroup(
								gl_panelCliente
										.createParallelGroup(Alignment.LEADING)
										.addComponent(lblNumero,
												GroupLayout.PREFERRED_SIZE, 14,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(lblBairro,
												GroupLayout.PREFERRED_SIZE, 14,
												GroupLayout.PREFERRED_SIZE))
						.addGap(11)
						.addGroup(
								gl_panelCliente
										.createParallelGroup(Alignment.LEADING)
										.addComponent(fieldNumero,
												GroupLayout.PREFERRED_SIZE, 20,
												GroupLayout.PREFERRED_SIZE)
										.addComponent(fieldBairro,
												GroupLayout.PREFERRED_SIZE, 20,
												GroupLayout.PREFERRED_SIZE))
						.addGap(15)
						.addComponent(lblCidade, GroupLayout.PREFERRED_SIZE,
								14, GroupLayout.PREFERRED_SIZE)
						.addGap(11)
						.addComponent(fieldCidade, GroupLayout.PREFERRED_SIZE,
								20, GroupLayout.PREFERRED_SIZE)
						.addGap(13)
						.addComponent(lblTipoDeCliente,
								GroupLayout.PREFERRED_SIZE, 14,
								GroupLayout.PREFERRED_SIZE)
						.addGap(11)
						.addComponent(fieldTipoCliente,
								GroupLayout.PREFERRED_SIZE, 20,
								GroupLayout.PREFERRED_SIZE)));
		panelCliente.setLayout(gl_panelCliente);
		panel_1.setLayout(null);
		panel_1.add(lblEntrada);
		panel_1.add(lblDataEntrada);
		panel_1.add(fieldDataEntrada);
		panel_1.add(fieldEntrada);
		panel_1.add(lblDataSaida);
		panel_1.add(lblSada);
		panel_1.add(fieldSaida);
		panel_1.add(fieldDataSaida);
		contentPanel.setLayout(null);
		contentPanel.add(panel);
		panel.setLayout(null);
		panel.add(lblValorEstacinamento);
		panel.add(fieldValorEstacionamento);
		panel.add(lblPenmancia);
		panel.add(fieldPermanencia);
		panel.add(lblDesconto);
		panel.add(fieldDesconto);
		panel.add(lblValorServoos);
		panel.add(textField_5);
		panel.add(lblJuros);
		panel.add(fieldJuros);
		panel.add(panel_2);

		btnTotal = new JButton("TOTAL");
		btnTotal.setToolTipText("Calcula o Troco");
		btnTotal.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				total(arg0);
			}
		});
		btnTotal.setBounds(332, 24, 89, 71);
		panel_2.add(btnTotal);

		btnSubtotal = new JButton("SUBTOTAL");
		btnSubtotal.setToolTipText("Calcula o Valor Total");
		btnSubtotal.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				do_btnSubtotal_actionPerformed(arg0);
			}
		});
		btnSubtotal.setBounds(645, 82, 111, 47);
		btnTotal.setEnabled(false);
		panel.add(btnSubtotal);
		contentPanel.add(panel_1);
		contentPanel.add(lblTipo);
		contentPanel.add(comboBoxFormaCobranca);
		contentPanel.add(rdbtnServios);
		contentPanel.add(lblPlaca);
		contentPanel.add(fieldPlaca);
		contentPanel.add(lblModelo);
		contentPanel.add(lblCor);
		contentPanel.add(fieldModelo);
		contentPanel.add(fieldCor);
		contentPanel.add(panelCliente);
		contentPanel.add(buttonPane);
		buttonPane.setLayout(null);
		buttonPane.add(buttonFechar);
		buttonPane.add(buttonCancel);
		listarTipoCliente();

	}

	public void atualizaTela(Estacionamento saida) {
		saidaAtual = saida;
		fieldTroco.setTipoDado(1);
		fieldValorPago.setTipoDado(1);
		fieldJuros.setTipoDado(1);
		fieldValorTotal.setTipoDado(1);
		fieldValorEstacionamento.setTipoDado(1);
		fieldDesconto.setTipoDado(1);
		fieldDesconto.setText("0");
		fieldJuros.setText("0");
		fieldPlaca.setText(saida.getPlaca());
		fieldCor.setText(saida.getCor());
		fieldModelo.setText(saida.getModelo());
		fieldEntrada.setText(saida.getHoraEntrada());
		fieldSaida.setText(saida.getHoraSaida());
		fieldDataEntrada.setText(saida.getDataEntrada());
		fieldDataSaida.setText(saida.getDataSaida());
		listarCliente();
		try {
			diferencaEntreDatas(saida.getDataEntrada(), saida.getDataSaida(),
					saida);

		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public void listarCliente() {
		boolean cliente = false;
		GenericDao<Object> veiculoDao = new GenericDao<Object>();
		List<Veiculo> listaVeiculo = veiculoDao.listar("Veiculo");
		for (int i = 0; i < listaVeiculo.size(); i++) {
			if ((listaVeiculo.get(i).getPlaca().equalsIgnoreCase(fieldPlaca
					.getText())) && (listaVeiculo.get(i).getCliente() != null)) {
				cliente = true;
				if (!listaVeiculo.get(i).getCliente().getTipoCliente()
						.getDescricao().equalsIgnoreCase("Horista")) {
					comboBoxFormaCobranca.setSelectedItem(listaVeiculo.get(i)
							.getCliente().getTipoCliente().getDescricao());
				}
				fieldNomeCliente.setText(listaVeiculo.get(i).getCliente()
						.getNome());
				fieldCpf.setText(listaVeiculo.get(i).getCliente().getCpf());
				fieldCelular.setText(listaVeiculo.get(i).getCliente()
						.getCelular());
				fieldTelefone.setText(listaVeiculo.get(i).getCliente()
						.getTelefone());
				fieldEndereco.setText(listaVeiculo.get(i).getCliente()
						.getEndereco());
				fieldNumero.setText(listaVeiculo.get(i).getCliente()
						.getEndNumero());
				fieldBairro.setText(listaVeiculo.get(i).getCliente()
						.getBairro());
				fieldCidade.setText(listaVeiculo.get(i).getCliente()
						.getCidade());
				fieldTipoCliente.setText(listaVeiculo.get(i).getCliente()
						.getTipoCliente().getDescricao());

			}
		}
		panelCliente.setVisible(cliente);

	}

	public List<TipoCliente> listarTipoCliente() {
		comboBoxFormaCobranca.removeAllItems();
		GenericDao<Object> tipoClienteDao = new GenericDao<Object>();
		List<TipoCliente> listaTC = tipoClienteDao.listar("TipoCliente");
		comboBoxFormaCobranca.addItem("Selecione");
		for (int i = 0; i < listaTC.size(); i++) {
			if (listaTC.get(i).isAtivo() == true) {
				comboBoxFormaCobranca.addItem(listaTC.get(i).getDescricao());
			}
		}
		return listaTC;

	}

	public void diferencaEntreDatas(String dataEntrada, String dataSaida,
			Estacionamento saida) throws ParseException {
		GregorianCalendar ini = new GregorianCalendar();
		GregorianCalendar fim = new GregorianCalendar();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		ini.setTime(sdf.parse(dataEntrada));
		fim.setTime(sdf.parse(dataSaida));
		long dt1 = ini.getTimeInMillis();
		long dt2 = fim.getTimeInMillis();
		qtdDias = ((dt2 - dt1) / 86400000);

		if (((dt2 - dt1) / 86400000) == 0) {
			// permanencia não atingiu uma diaria
			calculaTempo(saida);

		} else {
			// quantidade de dias
			fieldPermanencia.setText(String.valueOf((dt2 - dt1) / 86400000)
					+ " Dias");

		}

	}

	public void calculaTempo(Estacionamento saida) {
		String horaEntrada = fieldEntrada.getText();
		String horaSaida = fieldSaida.getText();
		String dataEntrada = fieldDataEntrada.getText();
		String dataSaida = fieldDataSaida.getText();

		String horas = "00";
		String minutos = "00";
		String segundos = "00";

		int sub = 0;
		int subHoras = 0;
		int subMinutos = 0;

		int segundos1 = (Integer.parseInt(horaEntrada.substring(0, 2)) * 3600)
				+ (Integer.parseInt(horaEntrada.substring(3, 5)) * 60)
				+ Integer.parseInt(horaEntrada.substring(6, 8));
		int segundos2 = (Integer.parseInt(horaSaida.substring(0, 2)) * 3600)
				+ (Integer.parseInt(horaSaida.substring(3, 5)) * 60)
				+ Integer.parseInt(horaSaida.substring(6, 8));

		if (segundos1 > segundos2) {
			sub = segundos1 - segundos2;
		} else if (segundos2 > segundos1) {
			sub = segundos2 - segundos1;
		} else {
			sub = 0;
		}

		if (sub >= 3600) {
			subHoras = (sub - (sub % 3600)) / 3600;
			sub = sub - (subHoras * 3600);
			if (subHoras < 10) {
				horas = "0" + Integer.toString(subHoras);
			} else {
				horas = Integer.toString(subHoras);
			}
		}

		if (sub >= 60) {
			subMinutos = (sub - (sub % 60)) / 60;
			sub = sub - (subMinutos * 60);
			if (subMinutos < 10) {
				minutos = "0" + Integer.toString(subMinutos);
			} else {
				minutos = Integer.toString(subMinutos);
			}
		}

		if (sub < 10) {
			segundos = "0" + Integer.toString(sub);
		} else {
			segundos = Integer.toString(sub);
		}
		fieldPermanencia.setText(horas + ":" + minutos + ":" + segundos);

	}

	protected void do_comboBoxFormaCobranca_actionPerformed(ActionEvent arg0) {
		if (comboBoxFormaCobranca.getSelectedItem().toString()
				.equalsIgnoreCase("Horista")) {
			if (qtdDias == 0) {
				CalculaPreco calcula = new CalculaPreco();
				String horaEntrada = fieldEntrada.getText();
				String horaSaida = fieldSaida.getText();
				String dataEntrada = fieldDataEntrada.getText();
				String dataSaida = fieldDataSaida.getText();
				fieldValorEstacionamento.setText(String.valueOf(calcula
						.calculaValorHora(dataEntrada + horaEntrada, dataSaida
								+ horaSaida)));
			} else {
				MessageFactory.showError(
						"Não é possível realizar o fechamento como Horista!",
						"Atenção");
				comboBoxFormaCobranca.setSelectedIndex(0);
			}
		} else if (comboBoxFormaCobranca.getSelectedItem().toString()
				.equalsIgnoreCase("Diarista")) {
			CalculaPreco calcula = new CalculaPreco();
			fieldValorEstacionamento.setText(String.valueOf(calcula
					.calculaValorDiaria(qtdDias)));

		} else if (comboBoxFormaCobranca.getSelectedItem().toString()
				.equalsIgnoreCase("Mensalista")) {
			CalculaPreco calcula = new CalculaPreco();
			fieldValorEstacionamento.setText(String.valueOf(calcula
					.calculaValorMes()));
			fecharCupom();
			buttonFechar.requestFocus();

		} else if (comboBoxFormaCobranca.getSelectedItem().toString()
				.equalsIgnoreCase("CLIDESP")) {
			CalculaPreco calcula = new CalculaPreco();
			fieldValorEstacionamento.setText(String.valueOf(calcula
					.calculaValorConvenio()));
			fecharCupom();
			buttonFechar.requestFocus();
		} else {
			comboBoxFormaCobranca.setSelectedIndex(0);

		}
	}

	protected boolean fecharCupom() {
		boolean fechar = true;
		if (comboBoxFormaCobranca.getSelectedIndex() == 0) {
			MessageFactory.showInformation("Selecione a forma de cobrança!",
					"Forma de Cobrança");
			fechar = false;
			comboBoxFormaCobranca.requestFocus();
		} else {
			try {
				if (!fieldValorEstacionamento.getText().isEmpty()) {
					valorTotal = Double.parseDouble(fieldValorEstacionamento
							.getText());
				} else {
					valorTotal = 0;
				}
				if (!fieldDesconto.getText().isEmpty()) {
					desconto = Double.parseDouble(fieldDesconto.getText());
				} else {
					desconto = 0;
				}
				if (!fieldJuros.getText().isEmpty()) {
					juros = Double.parseDouble(fieldJuros.getText());
				} else {
					juros = 0;
				}
				valorTotal -= desconto;
				valorTotal += juros;
				if (valorTotal >= 0) {
					fieldValorTotal.setText(String.valueOf(valorTotal));
					btnTotal.setEnabled(true);
					fieldValorPago.requestFocus();

				} else {
					fechar = false;
					MessageFactory.showError(
							"Valor não permitido!\n Favor verificar!",
							"Valor não permitido!");
					valorTotal = 0;
					fieldValorPago.setText(null);
					fieldValorTotal.setText(null);
					fieldDesconto.requestFocus();
				}
			} catch (java.lang.NumberFormatException ex) {
				MessageFactory.showError("Valor inválido!");
				fieldJuros.setText(null);
				fieldDesconto.setText(null);
				fieldDesconto.requestFocus();
				fechar = false;

			}
		}
		return fechar;
	}

	protected void do_btnSubtotal_actionPerformed(ActionEvent arg0) {
		fecharCupom();
	}

	protected boolean total(ActionEvent arg0) {

		if (fecharCupom()) {

			try {
				if (fieldValorPago.getText().isEmpty()) {
					valorPago = 0;
				} else {
					valorPago = Double.parseDouble(fieldValorPago.getText());

				}
				troco = valorPago - valorTotal;
				if ((troco == 0)
						&& comboBoxFormaCobranca.getSelectedIndex() != 0) {
					finaliza = true;
				}
				if (troco < 0) {
					troco = troco * (-1);
					MessageFactory.showInformation("FALTA: R$" + troco,
							"Atenção");
					fieldTroco.setText(null);
					fieldValorPago.requestFocus();
					finaliza = false;

				} else {
					fieldTroco.setText(String.valueOf(troco));
					finaliza = true;
				}
			} catch (java.lang.NumberFormatException e) {
				MessageFactory.showInformation("Erro ao calcular o troco!",
						"Total");
				finaliza = false;
			}
		}
		return finaliza;
	}

	protected void do_fechar_actionPerformed(ActionEvent arg0) {
		FramePrincipal principal = new FramePrincipal();
		principal.atualizaTabelaEntrada();
		if (total(arg0)) {
			saidaAtual.setValorTotal(valorTotal);
			saidaAtual.setJuros(juros);
			saidaAtual.setTroco(troco);
			saidaAtual.setDesconto(desconto);
			saidaAtual.setValorPago(valorPago);
			saidaAtual.setFormaDeCobranca(comboBoxFormaCobranca
					.getSelectedItem().toString());
			saidaAtual.setPermanencia(fieldPermanencia.getText());
			GenericDao<Object> saidaDao = new GenericDao<Object>();
			saidaDao.salvar(saidaAtual);

			MessageFactory.showInformation("Saída realizada com sucesso!",
					"Saída");
			principal.atualizaTabelaEntrada();

			aberto = false;
			dispose();
		} else {
			aberto = true;

		}

	}

	protected void do_buttonCancel_actionPerformed(ActionEvent arg0) {

		// FramePrincipal principal = new FramePrincipal();
		// GenericDao<Object> saidaDao = new GenericDao<Object>();
		// saidaAtual.setDataSaida(null);
		// saidaAtual.setHoraSaida(null);
		// saidaDao.salvar(saidaAtual);
		MessageFactory.showInformation("Saida cancelada!", "Atenção");
		// principal.atualizaTabelaEntrada();
		// OBS: so está atualizando a tabela de entrada com a saida cancelada
		// após efetuar uma entrada qualquer, mas esta cancelando a saida
		aberto = false;
		dispose();

	}

}
