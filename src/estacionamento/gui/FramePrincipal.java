package estacionamento.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.border.LineBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;

import com.toedter.calendar.JCalendar;

import estacionamento.componente.PanelComImagem;
import estacionamento.entidade.Estacionamento;
import estacionamento.entidade.Funcionarios;
import estacionamento.persistencia.GenericDao;
import framework.componente.TTable;
import framework.util.MessageFactory;

public class FramePrincipal extends JFrame implements ActionListener,
		MouseListener {
	private boolean logar;
	private final JMenuBar menuBar;
	private final JMenuItem mntmFuncionario;
	private final JMenuItem mntmEntrada;
	private final JMenu mnCadastro;
	private final JMenuItem mntmTabelaDePrecos;
	private final JMenuItem mntmClientes;
	private final JMenu mnRelatorios;
	private final JMenuItem mntmServios;
	private final JMenuItem mntmSada;
	private final JMenuItem mntmMovimentao;
	private final JMenuItem mntmContas;
	private final JMenu mnConfiguracao;
	private final JMenuItem mntmOpcoes;
	private final JMenuItem mntmNewMenuItem_1;
	private final JMenuItem mntmVeiculo;
	private final JLabel lblUser;
	private final JPanel panel;
	private final JButton btnEntrada;
	private final JButton btnSaida;
	private final JMenu mnMovimento;
	private final JMenuItem mntmEntrada_1;
	private final JMenuItem mntmSada_1;
	private final JDesktopPane desktopPrincipal;
	private final JScrollPane scrollPane;
	public TTable<Estacionamento> tableEntrada;
	private final JLabel LabelClock;
	private Timer timer;
	private final int contJanelas = 0;
	private Funcionarios user;

	// private InternalCliente cliente;
	// private FrameEntrada dialogEntrada;
	// private InternalVeiculo veiculo;
	// private InternalFuncionario funcionario;
	// private Opcao opcao;
	// private InternalPreco tabelaPreco;

	public Funcionarios getUser() {
		return user;
	}

	public void setUser(Funcionarios user) {
		this.user = user;
	}

	FrameEntrada dialogEntrada;
	InternalCliente cliente;
	InternalPreco tabelaPreco;
	InternalVeiculo veiculo;
	InternalFuncionario funcionario;
	Opcao opcao;
	private final JLabel lblCalendario;

	public FramePrincipal() {
		addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent e) {
				do_this_keyPressed(e);
			}
		});
		setTitle("Estacionamento");
		setMinimumSize(new Dimension(1024, 728));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(Frame.MAXIMIZED_BOTH);

		menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		mnCadastro = new JMenu("Cadastro");
		menuBar.add(mnCadastro);

		mntmClientes = new JMenuItem("Clientes");
		mntmClientes.addActionListener(this);

		mntmVeiculo = new JMenuItem("Veículo");
		mntmVeiculo.addActionListener(this);

		mnCadastro.add(mntmVeiculo);
		mnCadastro.add(mntmClientes);

		mntmFuncionario = new JMenuItem("Funcionário");
		mntmFuncionario.addActionListener(this);
		mnCadastro.add(mntmFuncionario);

		mntmServios = new JMenuItem("Serviços");
		mntmServios.setEnabled(false);
		mnCadastro.add(mntmServios);

		mntmTabelaDePrecos = new JMenuItem("Tabela de Preços");
		mntmTabelaDePrecos.addActionListener(this);
		mnCadastro.add(mntmTabelaDePrecos);

		mnMovimento = new JMenu("Movimento");
		menuBar.add(mnMovimento);

		mntmSada_1 = new JMenuItem("Saída");
		mnMovimento.add(mntmSada_1);

		mntmEntrada_1 = new JMenuItem("Entrada");
		mnMovimento.add(mntmEntrada_1);

		mnRelatorios = new JMenu("Relatórios");
		menuBar.add(mnRelatorios);

		mntmEntrada = new JMenuItem("Entrada");
		mnRelatorios.add(mntmEntrada);

		mntmSada = new JMenuItem("Saída");
		mnRelatorios.add(mntmSada);

		mntmMovimentao = new JMenuItem("Movimentação");
		mnRelatorios.add(mntmMovimentao);

		mntmContas = new JMenuItem("Contas");
		mnRelatorios.add(mntmContas);

		mnConfiguracao = new JMenu("Configuração");
		menuBar.add(mnConfiguracao);

		mntmNewMenuItem_1 = new JMenuItem("Forma de Cobrança");
		mnConfiguracao.add(mntmNewMenuItem_1);

		mntmOpcoes = new JMenuItem("Opções");
		mntmOpcoes.addActionListener(this);
		mnConfiguracao.add(mntmOpcoes);

		panel = new PanelComImagem();
		panel.setBorder(new LineBorder(new Color(0, 0, 0)));

		desktopPrincipal = new JDesktopPane();
		desktopPrincipal.setLayout(new BorderLayout(0, 0));

		scrollPane = new JScrollPane();
		desktopPrincipal.add(scrollPane, BorderLayout.CENTER);

		tableEntrada = new TTable<Estacionamento>((Estacionamento.class));

		tableEntrada.setColumnName("id", "Código");
		tableEntrada.getTable().setRowHeight(20);
		tableEntrada.removeColumn("permanencia");
		tableEntrada.removeColumn("formaDeCobranca");
		tableEntrada.removeColumn("idTipoVeiculo");
		tableEntrada.removeColumn("dataSaida");
		tableEntrada.removeColumn("horaSaida");
		tableEntrada.removeColumn("desconto");
		tableEntrada.removeColumn("troco");
		tableEntrada.removeColumn("juros");
		tableEntrada.removeColumn("valorTotal");
		tableEntrada.removeColumn("valorPago");

		scrollPane.setViewportView(tableEntrada);
		tableEntrada.getPanelFooter().setLayout(null);

		btnEntrada = new JButton("Entrada");
		btnEntrada.setToolTipText("F3 - Entrada de Veiculos");
		btnEntrada.setIcon(new ImageIcon(FramePrincipal.class
				.getResource("/img/car_add.png")));
		btnEntrada.addActionListener(this);

		btnSaida = new JButton("Saída");
		btnSaida.setIcon(new ImageIcon(FramePrincipal.class
				.getResource("/img/car_delete.png")));
		btnSaida.setToolTipText("F4 - Saída de Veículos");
		btnSaida.addActionListener(this);

		lblCalendario = new JLabel("");
		lblCalendario.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		lblCalendario.addMouseListener(this);
		lblCalendario.setIcon(new ImageIcon(FramePrincipal.class
				.getResource("/img/calendar-icon.png")));

		lblUser = new JLabel("user");
		lblUser.setForeground(Color.DARK_GRAY);
		lblUser.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 16));

		panel_1 = new JPanel();
		panel_1.setBackground(Color.BLACK);
		panel_1.setLayout(new BorderLayout(0, 0));

		// coloca o relogio na tela

		LabelClock = new JLabel("");
		panel_1.add(LabelClock, BorderLayout.CENTER);
		LabelClock.setHorizontalAlignment(SwingConstants.CENTER);
		LabelClock.setBackground(Color.BLACK);
		LabelClock.setForeground(Color.RED);
		LabelClock.setFont(new Font("Century Schoolbook L", Font.BOLD, 21));
		getContentPane().setLayout(new BorderLayout(0, 0));
		getContentPane().add(desktopPrincipal);
		getContentPane().add(panel, BorderLayout.NORTH);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(gl_panel.createParallelGroup(
				Alignment.LEADING).addGroup(
				gl_panel.createSequentialGroup()
						.addGap(11)
						.addComponent(btnEntrada, GroupLayout.PREFERRED_SIZE,
								129, GroupLayout.PREFERRED_SIZE)
						.addGap(9)
						.addComponent(btnSaida, GroupLayout.PREFERRED_SIZE,
								129, GroupLayout.PREFERRED_SIZE)
						.addGap(270)
						.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 252,
								GroupLayout.PREFERRED_SIZE)
						.addGap(247)
						.addComponent(lblUser, GroupLayout.PREFERRED_SIZE, 101,
								GroupLayout.PREFERRED_SIZE).addGap(104)
						.addComponent(lblCalendario)));
		gl_panel.setVerticalGroup(gl_panel
				.createParallelGroup(Alignment.LEADING)
				.addGroup(
						gl_panel.createSequentialGroup()
								.addGap(7)
								.addComponent(btnEntrada,
										GroupLayout.PREFERRED_SIZE, 41,
										GroupLayout.PREFERRED_SIZE))
				.addGroup(
						gl_panel.createSequentialGroup()
								.addGap(7)
								.addComponent(btnSaida,
										GroupLayout.PREFERRED_SIZE, 41,
										GroupLayout.PREFERRED_SIZE))
				.addComponent(lblCalendario)
				.addGroup(
						gl_panel.createSequentialGroup()
								.addContainerGap()
								.addGroup(
										gl_panel.createParallelGroup(
												Alignment.TRAILING)
												.addComponent(
														panel_1,
														GroupLayout.PREFERRED_SIZE,
														32,
														GroupLayout.PREFERRED_SIZE)
												.addComponent(
														lblUser,
														GroupLayout.PREFERRED_SIZE,
														26,
														GroupLayout.PREFERRED_SIZE))));
		panel.setLayout(gl_panel);

		panel_2 = new PanelComImagem();
		getContentPane().add(panel_2, BorderLayout.SOUTH);
		panel_2.setLayout(new BorderLayout(0, 0));

		lblLogo = new JLabel("");
		lblLogo.setIcon(new ImageIcon("../estacionamento/img/logo"));
		panel_2.add(lblLogo, BorderLayout.WEST);

		panel_3 = new PanelComImagem();
		panel_2.add(panel_3, BorderLayout.CENTER);

		// Sempre que iniciar mostra os veiculos em aberto
		atualizaTabelaEntrada();
		// Chama o metodo para disparar o relogio
		disparaRelogio();

	}

	public void inforUser(Funcionarios user) {
		this.user = user;
		lblUser.setText(user.getNome());
	}

	public void atualizaTabelaEntrada() {
		GenericDao<Object> entradaDao = new GenericDao<Object>();
		List<Estacionamento> entrada = entradaDao
				.listarEntrada("Estacionamento");
		tableEntrada.refreshData(entrada);

	}

	public void disparaRelogio() {
		if (timer == null) {
			timer = new Timer(1000, this);
			timer.setInitialDelay(0);
			timer.start();
		} else if (!timer.isRunning()) {
			timer.restart();
		}
	}

	Calendar data = new GregorianCalendar();
	String data_format = String.valueOf(data.get(Calendar.DATE)) + "/"
			+ String.valueOf(data.get(Calendar.MONTH) + 1) + "/"
			+ String.valueOf(data.get(Calendar.YEAR));
	private final JPanel panel_1;
	private JPanel panel_2;
	private JLabel lblLogo;
	private JPanel panel_3;

	@Override
	public void actionPerformed(ActionEvent arg0) {
		if (arg0.getSource() == mntmTabelaDePrecos) {
			do_mntmTabelaDePreos_actionPerformed(arg0);
		}
		if (arg0.getSource() == mntmVeiculo) {
			do_mntmVeiculo_actionPerformed(arg0);
		}
		if (arg0.getSource() == mntmClientes) {
			do_mntmClientes_actionPerformed(arg0);
		}
		if (arg0.getSource() == btnEntrada) {
			do_btnEntrada_actionPerformed(arg0);
		}
		if (arg0.getSource() == btnSaida) {
			do_btnSaida_actionPerformed(arg0);
		}
		if (arg0.getSource() == mntmFuncionario) {
			do_mntmFuncionario_actionListener(arg0);
		}
		if (arg0.getSource() == mntmOpcoes) {
			do_mntmOpcao_actionListener(arg0);
		}
		if (arg0.getSource() == mntmEntrada) {
			do_mntmEntrada_actionPerformed(arg0);
		}

		GregorianCalendar calendario = new GregorianCalendar();
		int h = calendario.get(GregorianCalendar.HOUR_OF_DAY);
		int m = calendario.get(GregorianCalendar.MINUTE);
		int s = calendario.get(GregorianCalendar.SECOND);

		String hora = ((h < 10) ? "0" : "") + h + ":" + ((m < 10) ? "0" : "")
				+ m + ":" + ((s < 10) ? "0" : "") + s;

		LabelClock.setText(data_format + "   " + hora);

	}

	public void do_btnEntrada_actionPerformed(ActionEvent arg0) {
		dialogEntrada = new FrameEntrada();
		if (dialogEntrada.isAberto() == false) {
			dialogEntrada.setPrincipal(this);
			dialogEntrada.setAberto(true);
			dialogEntrada.setResizable(true);
			desktopPrincipal.remove(dialogEntrada);
			desktopPrincipal.add(dialogEntrada);
			dialogEntrada.setVisible(true);

		} else {
			dialogEntrada.toFront();

		}

	}

	protected void do_btnSaida_actionPerformed(ActionEvent arg0) {
		DialogPlaca dialogplaca = new DialogPlaca();
		dialogplaca.setLocationRelativeTo(null);
		GenericDao<Object> saidaDao = new GenericDao<Object>();
		InternalFechamentoSaida fechamentoSaida = new InternalFechamentoSaida();
		Estacionamento saida = new Estacionamento();
		Date datahora = new Date();
		// formata a data e a hora
		SimpleDateFormat sdfData = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat sdfHora = new SimpleDateFormat("HH:mm:ss");

		if (tableEntrada.getSelecionado() != null) {

			dialogplaca
					.atualizarPlaca(tableEntrada.getSelecionado().getPlaca());
			dialogplaca.setVisible(true);
			String placa = dialogplaca.getPlaca();
			if (tableEntrada.getSelecionado().getPlaca()
					.equalsIgnoreCase(placa)) {
				tableEntrada.getSelecionado().setHoraSaida(
						sdfHora.format(datahora));
				tableEntrada.getSelecionado().setDataSaida(
						sdfData.format(datahora));

				saida = tableEntrada.getSelecionado();
				saida.setHoraSaida(sdfHora.format(datahora));
				saida.setDataSaida(sdfData.format(datahora));

				fechamentoSaida.atualizaTela(saida);
				desktopPrincipal.add(fechamentoSaida);
				fechamentoSaida.setResizable(true);
				fechamentoSaida.setVisible(true);

			} else {
				try {
					List<Estacionamento> fechamento = saidaDao.listarSaida(
							"Estacionamento", placa);
					saida = fechamento.get(0);
					saida.setDataSaida(sdfData.format(datahora));
					saida.setHoraSaida(sdfHora.format(datahora));
					fechamentoSaida.atualizaTela(saida);
					desktopPrincipal.add(fechamentoSaida);
					fechamentoSaida.setResizable(true);
					fechamentoSaida.setVisible(true);

				} catch (java.lang.IndexOutOfBoundsException e) {
					MessageFactory.showError("Placa invorreta!", "Atenção");
				}

			}
		} else {
			try {
				dialogplaca.setVisible(true);
				String placa = dialogplaca.getPlaca();
				List<Estacionamento> fechamento = saidaDao.listarSaida(
						"Estacionamento", placa);
				saida = fechamento.get(0);
				saida.setDataSaida(sdfData.format(datahora));
				saida.setHoraSaida(sdfHora.format(datahora));
				fechamentoSaida.atualizaTela(saida);
				desktopPrincipal.add(fechamentoSaida);
				fechamentoSaida.setResizable(true);
				fechamentoSaida.setVisible(true);

			} catch (java.lang.IndexOutOfBoundsException e) {
				MessageFactory.showError("Placa incorreta!", "Atenção");
			}
		}
		InternalFrameListener l = new InternalFrameListener() {

			@Override
			public void internalFrameOpened(InternalFrameEvent e) {

			}

			@Override
			public void internalFrameIconified(InternalFrameEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void internalFrameDeiconified(InternalFrameEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void internalFrameDeactivated(InternalFrameEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void internalFrameClosing(InternalFrameEvent e) {
				// TODO Auto-generated method stub

			}

			@Override
			public void internalFrameClosed(InternalFrameEvent e) {
				atualizaTabelaEntrada();
			}

			@Override
			public void internalFrameActivated(InternalFrameEvent e) {
				// TODO Auto-generated method stub

			}
		};
		fechamentoSaida.addInternalFrameListener(l);

	}

	protected void do_mntmVeiculo_actionPerformed(ActionEvent arg0) {
		veiculo = new InternalVeiculo();
		if (InternalVeiculo.aberto == false) {
			veiculo = new InternalVeiculo();
			InternalVeiculo.aberto = true;
			desktopPrincipal.remove(veiculo);
			desktopPrincipal.add(veiculo);
			veiculo.setResizable(true);
			veiculo.setVisible(true);
		} else {
			desktopPrincipal.moveToFront(veiculo);
		}
	}

	protected void do_mntmClientes_actionPerformed(ActionEvent arg0) {
		cliente = new InternalCliente();
		if (InternalCliente.aberto == false) {
			cliente = new InternalCliente();
			InternalCliente.aberto = true;
			desktopPrincipal.remove(cliente);
			desktopPrincipal.add(cliente);
			cliente.setResizable(true);
			cliente.setVisible(true);

		} else {
			desktopPrincipal.moveToFront(cliente);
		}
	}

	// private static BufferedImage loadImage(String file) throws IOException {
	// return ImageIO.read(new File(file));
	// }

	protected void do_mntmFuncionario_actionListener(ActionEvent arg0) {
		funcionario = new InternalFuncionario();
		if (user.getTipo().equalsIgnoreCase("S")) {
			if (InternalFuncionario.aberto == false) {
				funcionario = new InternalFuncionario();
				InternalFuncionario.aberto = true;
				desktopPrincipal.remove(funcionario);
				desktopPrincipal.add(funcionario);
				// funcionario.setResizable(true);
				funcionario.setVisible(true);

			} else {
				desktopPrincipal.moveToFront(funcionario);
			}
		} else {
			JOptionPane
					.showMessageDialog(null,
							"Você não tem previlegio para acessar a tela de funcionarios.");
		}
		desktopPrincipal.moveToFront(funcionario);
	}

	protected void do_mntmOpcao_actionListener(ActionEvent arg0) {
		opcao = new Opcao();
		if (Opcao.aberto == false) {
			opcao = new Opcao();
			Opcao.aberto = true;
			desktopPrincipal.remove(opcao);
			desktopPrincipal.add(opcao);
			// opcao.setResizable(true);
			// opcao.verificaStatus();
			opcao.setVisible(true);

		} else {
			desktopPrincipal.moveToFront(opcao);
		}
	}

	protected void do_mntmTabelaDePreos_actionPerformed(ActionEvent arg0) {
		tabelaPreco = new InternalPreco();
		if (tabelaPreco.isAberto() == false) {
			tabelaPreco = new InternalPreco();
			tabelaPreco.setAberto(true);
			tabelaPreco.setResizable(true);
			desktopPrincipal.remove(tabelaPreco);
			desktopPrincipal.add(tabelaPreco);
			tabelaPreco.setVisible(true);
		} else {
			desktopPrincipal.moveToFront(tabelaPreco);
		}

	}

	protected void do_mntmEntrada_actionPerformed(ActionEvent arg0) {
		// System.out.println("Gerando relatório...");
		// UsuarioDAO usuarioDAO = new UsuarioDAO();
		// List listaUs = usuarioDAO.listaTodos();
		//
		// JasperReport pathjrxml =
		// JasperCompileManager.compileReport("relatorio/reportex.jrxml");
		// JasperPrint printReport = JasperFillManager.fillReport(pathjrxml,
		// null, new JRBeanCollectionDataSource(listaUs));
		// JasperExportManager.exportReportToPdfFile(printReport,
		// "relatorio/reportex.pdf");
		// System.out.println("Relatorio gerado");
		// }
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getSource() == lblCalendario) {
			do_lblCalendario_mouseClicked(e);
		}
	}

	protected void do_lblCalendario_mouseClicked(MouseEvent e) {
		CalendarioViewer cv = new CalendarioViewer();
		cv.setLocation(lblCalendario.getX() - 190, lblCalendario.getY() + 90);
		cv.setVisible(true);
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	protected void do_this_keyPressed(KeyEvent e) {
		int tecla = e.getKeyCode();
		if (tecla == KeyEvent.VK_F3) {
			do_btnEntrada_actionPerformed(null);
			System.out.println("Entrada");
		}
		if (tecla == KeyEvent.VK_F4) {
			do_btnSaida_actionPerformed(null);
			System.out.println("Saida");

		}

	}
}
