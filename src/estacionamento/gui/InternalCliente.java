package estacionamento.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.text.MaskFormatter;

import org.dom4j.Branch;

import estacionamento.componente.TButton;
import estacionamento.componente.TComboBox;
import estacionamento.config.Parametros;
import estacionamento.entidade.Cliente;
import estacionamento.entidade.Endereco;
import estacionamento.entidade.Funcionarios;
import estacionamento.entidade.TipoCliente;
import estacionamento.enums.Uf;
import estacionamento.persistencia.GenericDao;
import estacionamento.sistema.Calendario;
import estacionamento.sistema.ValidacaoCpf;
import estacionamento.sistema.WebServiceCep;
import framework.component.field.TTextField;
import framework.component.table.TTableCompleta;

public class InternalCliente extends javax.swing.JInternalFrame implements
		ActionListener, FocusListener {

	private long id;

	public InternalCliente() {
		setTitle("Cadastro de Clientes");
		maskDataNasc.setPlaceholderCharacter('_');
		try {
			maskDataNasc.setMask("##/##/####");
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		setVerifyInputWhenFocusTarget(false);
		try {
			GenericDao d = new GenericDao();
			id = d.ultimoID("Cliente") + 1;
		} catch (java.lang.NullPointerException e) {
			id = 1;
		}

		try {
			maskCep.setPlaceholderCharacter('_');
			maskCep.setMask("#####-###");
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			maskTelefone.setPlaceholderCharacter('_');
			maskTelefone.setMask("(##)####-####");
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			maskCpf.setPlaceholderCharacter('_');
			maskCpf.setMask("###.###.###-##");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		initComponents();
		atualizaTabelaCliente();
	}

	private void initComponents() {

		jPanel1 = new JPanel();
		jPanel1.setPreferredSize(new Dimension(10, 200));
		jLabel1 = new JLabel();
		jLabel1.setFont(new Font("Dialog", Font.PLAIN, 13));
		jLabel1.setBounds(16, 49, 63, 14);
		textFieldcod = new TTextField();
		textFieldcod.setBounds(97, 46, 63, 20);
		textFieldcod.setText(String.valueOf(id));
		textFieldcod.setEditable(false);
		jLabel2 = new JLabel();
		jLabel2.setFont(new Font("Dialog", Font.PLAIN, 13));
		jLabel2.setBounds(166, 49, 49, 14);
		textFieldNome = new TTextField();
		textFieldNome.setBounds(214, 46, 592, 20);
		jLabel3 = new JLabel();
		jLabel3.setFont(new Font("Dialog", Font.PLAIN, 13));
		jLabel3.setBounds(16, 76, 45, 14);
		jLabel4 = new javax.swing.JLabel();
		jLabel4.setFont(new Font("Dialog", Font.PLAIN, 13));
		jLabel4.setBounds(607, 110, 81, 14);
		jLabel5 = new javax.swing.JLabel();
		jLabel5.setFont(new Font("Dialog", Font.PLAIN, 13));
		jLabel5.setBounds(231, 142, 49, 14);
		jLabel6 = new javax.swing.JLabel();
		jLabel6.setFont(new Font("Dialog", Font.PLAIN, 13));
		jLabel6.setBounds(685, 142, 49, 14);
		textFieldEndereco = new TTextField();
		textFieldEndereco.setEditable(false);
		textFieldEndereco.setBounds(685, 108, 333, 20);
		textFieldBairro = new TTextField();
		textFieldBairro.setEditable(false);
		textFieldBairro.setBounds(298, 140, 248, 20);
		jLabel7 = new JLabel();
		jLabel7.setFont(new Font("Dialog", Font.PLAIN, 13));
		jLabel7.setBounds(811, 49, 63, 14);
		jLabel8 = new javax.swing.JLabel();
		jLabel8.setFont(new Font("Dialog", Font.PLAIN, 13));
		jLabel8.setBounds(16, 110, 49, 14);
		textFieldCelular = new TTextField();
		textFieldCelular.setBounds(95, 108, 131, 20);
		jLabel9 = new javax.swing.JLabel();
		jLabel9.setFont(new Font("Dialog", Font.PLAIN, 13));
		jLabel9.setBounds(811, 76, 37, 14);
		textFieldRG = new TTextField();
		textFieldRG.setBounds(880, 74, 138, 20);
		jLabel10 = new javax.swing.JLabel();
		jLabel10.setFont(new Font("Dialog", Font.PLAIN, 13));
		jLabel10.setBounds(607, 76, 37, 14);
		jLabel11 = new javax.swing.JLabel();
		jLabel11.setFont(new Font("Dialog", Font.PLAIN, 13));
		jLabel11.setBounds(231, 110, 37, 14);
		jLabel12 = new javax.swing.JLabel();
		jLabel12.setFont(new Font("Dialog", Font.PLAIN, 13));
		jLabel12.setBounds(564, 142, 37, 14);
		jLabel13 = new javax.swing.JLabel();
		jLabel13.setFont(new Font("Dialog", Font.PLAIN, 13));
		jLabel13.setBounds(16, 176, 63, 14);
		textFieldEmail = new JTextField();
		textFieldEmail.setBounds(97, 171, 921, 20);
		comboBoxSexo = new TComboBox();
		comboBoxSexo.setBounds(97, 73, 63, 20);

		jPanel1.setBorder(javax.swing.BorderFactory
				.createTitledBorder("Informações:"));

		jLabel1.setText("Código:");

		jLabel2.setText("Nome:");

		jLabel3.setText("Sexo:");

		jLabel4.setText("Endereço:");

		jLabel5.setText("Bairro:");

		jLabel6.setText("Cidade:");

		jLabel7.setText("Telefone:");

		jLabel8.setText("Celular:");

		jLabel9.setText("RG:");

		jLabel10.setText("CPF:");

		jLabel11.setText("CEP:");

		jLabel12.setText("UF:");

		jLabel13.setText("Email:");

		comboBoxSexo.setModel(new javax.swing.DefaultComboBoxModel(
				new String[] { "M", "F" }));

		JLabel lblN = new JLabel("N.:");
		lblN.setFont(new Font("Dialog", Font.PLAIN, 13));
		lblN.setBounds(16, 142, 31, 14);

		textFieldNumero = new TTextField();
		textFieldNumero.setBounds(97, 140, 104, 20);
		textFieldNumero.setColumns(10);
		jLabel26 = new javax.swing.JLabel();
		jLabel26.setFont(new Font("Dialog", Font.PLAIN, 13));
		jLabel26.setBounds(16, 19, 114, 14);

		jLabel26.setText("Tipo de Cliente:");
		comboBoxTipoCliente = new TComboBox();
		comboBoxTipoCliente.setBounds(140, 17, 114, 20);

		panel_1 = new JPanel();
		jPanel1.setLayout(null);
		jPanel1.add(jLabel1);
		jPanel1.add(jLabel4);
		jPanel1.add(jLabel5);
		jPanel1.add(jLabel13);
		jPanel1.add(jLabel7);
		jPanel1.add(jLabel9);
		jPanel1.add(textFieldRG);
		jPanel1.add(textFieldBairro);
		jPanel1.add(jLabel8);
		jPanel1.add(jLabel10);
		jPanel1.add(jLabel6);
		jPanel1.add(jLabel12);
		jPanel1.add(textFieldCelular);
		jPanel1.add(comboBoxSexo);
		jPanel1.add(jLabel26);
		jPanel1.add(comboBoxTipoCliente);
		jPanel1.add(textFieldcod);
		jPanel1.add(jLabel2);
		jPanel1.add(textFieldNome);
		jPanel1.add(jLabel3);
		jPanel1.add(textFieldEndereco);
		jPanel1.add(lblN);
		jPanel1.add(textFieldNumero);
		jPanel1.add(jLabel11);
		jPanel1.add(textFieldEmail);

		textFieldCpf = new JFormattedTextField(maskCpf);
		textFieldCpf.setBounds(652, 74, 154, 19);
		jPanel1.add(textFieldCpf);

		textFieldTelefone = new JFormattedTextField(maskTelefone);
		textFieldTelefone.setBounds(880, 47, 138, 19);
		jPanel1.add(textFieldTelefone);

		textFieldCep = new JFormattedTextField(maskCep);
		textFieldCep.addFocusListener(this);
		textFieldCep.setBounds(298, 108, 133, 19);
		jPanel1.add(textFieldCep);
		getContentPane().setLayout(new BorderLayout(0, 0));
		getContentPane().add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(0, 0));
		jPanel3 = new JPanel();
		panel_1.add(jPanel3, BorderLayout.SOUTH);
		btnSalvar = new TButton();
		btnSalvar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Cliente c = new Cliente();
				String msg = " cadastrado com sucesso.";
				// Para saber quando estou fazendo um update ou insert
				if (alterar == true) {
					c.setId(Integer.parseInt(textFieldcod.getText()));
					msg = " alterado com sucesso.";
					id = id - 1;
					alterar = false;
				}

				int dia = Integer.parseInt(textFieldDatanasc.getText()
						.substring(0, 2));
				int mes = Integer.parseInt(textFieldDatanasc.getText()
						.substring(3, 5));
				int ano = Integer.parseInt(textFieldDatanasc.getText()
						.substring(6, 10));

				c.setNome(textFieldNome.getText());
				c.setSexo(comboBoxSexo.getSelectedItem().toString());
				c.setDatanasc(new GregorianCalendar(ano, mes - 1, dia));
				c.setEndereco(textFieldEndereco.getText());
				c.setEndNumero(textFieldNumero.getText());
				c.setBairro(textFieldBairro.getText());
				c.setCidade(textFieldCidade.getText());
				c.setUf(textFieldUF.getText());
				c.setCep(textFieldCep.getText());
				c.setTipoEndereco(textFieldTipo.getText());
				c.setEmail(textFieldEmail.getText());
				c.setTelefone(textFieldTelefone.getText());
				c.setCelular(textFieldCelular.getText());
				c.setRg(textFieldRG.getText());
				c.setCpf(textFieldCpf.getText());
				TipoCliente tc = new TipoCliente();
				tc.setId(comboBoxTipoCliente.getSelectedIndex() + 1);
				c.setTipoCliente(tc);

				// 1 = Ativo.
				// 0 = Inativo.
				c.setAtivo(chckbxAtivo.isSelected());

				if (validarCampos() == false) {
					GenericDao<Object> cdao = new GenericDao<Object>();
					if (cdao.salvar(c) == true) {
						atualizaTabelaCliente();
						JOptionPane.showMessageDialog(null,
								"Cliente " + c.getNome() + msg);
						limpar();
						id = id + 1;
						textFieldcod.setText(String.valueOf(id));
						chckbxAtivo.setSelected(true);
					}
				}

			}
		});

		btnSalvar.setIcon(new ImageIcon(InternalCliente.class
				.getResource("/img/save.png")));
		btnExcluir = new javax.swing.JButton();
		btnExcluir.addActionListener(this);
		btnExcluir.setIcon(new ImageIcon(InternalCliente.class
				.getResource("/img/excluir.gif")));
		btnAlterar = new javax.swing.JButton();
		btnAlterar.setIcon(new ImageIcon(InternalCliente.class
				.getResource("/img/alterar1.png")));
		btnAlterar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				// TODO Auto-generated method stub
				if (tableCompleta.getSelecionado() != null) {
					habilitarCampos(true);
					Cliente cliente = (Cliente) tableCompleta.getSelecionado();

					Calendar data = cliente.getDatanasc();

					SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

					textFieldcod.setEditable(true);
					textFieldcod.setText(String.valueOf(cliente.getId()));
					textFieldcod.setEditable(false);
					textFieldNome.setText(cliente.getNome());
					comboBoxSexo.setSelectedItem(cliente.getSexo());
					textFieldEmail.setText(cliente.getEmail());
					textFieldRG.setText(cliente.getRg());
					textFieldTelefone.setText(cliente.getTelefone());
					textFieldCelular.setText(cliente.getCelular());
					textFieldCidade.setText(cliente.getCidade());
					textFieldUF.setText(cliente.getUf());
					textFieldBairro.setText(cliente.getBairro());
					textFieldEndereco.setText(cliente.getEndereco());
					textFieldNumero.setText(cliente.getEndNumero());
					textFieldTipo.setText(cliente.getTipoEndereco());
					textFieldCep.setText(cliente.getCep());
					textFieldCpf.setText(cliente.getCpf());
					textFieldDatanasc.setText(sdf.format(data.getTime()));
					textFieldIdade.setText(String.valueOf(cliente.getIdade()));
					System.out.println(cliente.isAtivo());
					chckbxAtivo.setSelected(cliente.isAtivo());

					alterar = true;
				} else {
					JOptionPane.showMessageDialog(null,
							"Favor selecionar algum registro !");
				}

			}

		});
		btnSair = new javax.swing.JButton();
		btnSair.setIcon(new ImageIcon(InternalCliente.class
				.getResource("/img/Sair.png")));

		jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(0));

		btnSalvar.setText("Salvar");
		jPanel3.add(btnSalvar);

		btnExcluir.setText("Excluir");
		jPanel3.add(btnExcluir);

		btnAlterar.setText("Alterar");
		jPanel3.add(btnAlterar);

		btnSair.setText("Sair");
		btnSair.addActionListener(new java.awt.event.ActionListener() {
			@Override
			public void actionPerformed(java.awt.event.ActionEvent evt) {
				jButton5ActionPerformed(evt);
			}
		});
		jPanel3.add(btnSair);

		tableCompleta = new TTableCompleta(Cliente.class);
		tableCompleta.removeColumn("ativo");
		tableCompleta.removeColumn("tipoEndereco");
		panel_1.add(tableCompleta, BorderLayout.CENTER);
		getContentPane().add(jPanel1, BorderLayout.NORTH);
		atualizaTabelaCliente();

		lblDataNascimento = new JLabel("Data Nascimento");
		lblDataNascimento.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblDataNascimento.setBounds(166, 76, 133, 15);
		jPanel1.add(lblDataNascimento);

		textFieldDatanasc = new JFormattedTextField(maskDataNasc);
		textFieldDatanasc.addFocusListener(this);
		textFieldDatanasc.setBounds(298, 74, 133, 19);
		jPanel1.add(textFieldDatanasc);

		lblIdade = new JLabel("Idade");
		lblIdade.setFont(new Font("Dialog", Font.PLAIN, 13));
		lblIdade.setBounds(449, 76, 37, 15);
		jPanel1.add(lblIdade);

		textFieldIdade = new TTextField();
		textFieldIdade.setEditable(false);
		textFieldIdade.setBounds(492, 74, 97, 19);
		jPanel1.add(textFieldIdade);
		textFieldIdade.setColumns(10);

		textFieldUF = new TTextField();
		textFieldUF.setEditable(false);
		textFieldUF.setBounds(607, 140, 45, 19);
		jPanel1.add(textFieldUF);
		textFieldUF.setColumns(10);

		textFieldCidade = new TTextField();
		textFieldCidade.setEditable(false);
		textFieldCidade.setBounds(752, 140, 266, 19);
		jPanel1.add(textFieldCidade);
		textFieldCidade.setColumns(10);

		chckbxAtivo = new JCheckBox("Ativo");
		chckbxAtivo.setSelected(true);
		chckbxAtivo.setBounds(960, 15, 60, 23);
		jPanel1.add(chckbxAtivo);

		lblTipo = new JLabel("Tipo");
		lblTipo.setFont(new Font("Dialog", Font.PLAIN, 12));
		lblTipo.setBounds(449, 108, 37, 15);
		jPanel1.add(lblTipo);

		textFieldTipo = new JTextField();
		textFieldTipo.setEditable(false);
		textFieldTipo.setFont(new Font("Dialog", Font.PLAIN, 12));
		textFieldTipo.setBounds(492, 108, 97, 19);
		jPanel1.add(textFieldTipo);
		textFieldTipo.setColumns(10);
		listarTipoCliente();
	}// </editor-fold>//GEN-END:initComponents

	private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {// GEN-FIRST:event_jButton5ActionPerformed
		aberto = false;
		dispose();
		// TODO add your handling code here:
	}// GEN-LAST:event_jButton5ActionPerformed

	// Variables declaration - do not modify//GEN-BEGIN:variables
	private javax.swing.JButton btnSalvar;
	private javax.swing.JButton btnExcluir;
	private javax.swing.JButton btnAlterar;
	private javax.swing.JButton btnSair;
	private TComboBox comboBoxTipoCliente;
	private TComboBox comboBoxSexo;
	private javax.swing.JLabel jLabel1;
	private javax.swing.JLabel jLabel10;
	private javax.swing.JLabel jLabel11;
	private javax.swing.JLabel jLabel12;
	private javax.swing.JLabel jLabel13;
	private javax.swing.JLabel jLabel2;
	private javax.swing.JLabel jLabel26;
	private javax.swing.JLabel jLabel3;
	private javax.swing.JLabel jLabel4;
	private javax.swing.JLabel jLabel5;
	private javax.swing.JLabel jLabel6;
	private javax.swing.JLabel jLabel7;
	private javax.swing.JLabel jLabel8;
	private javax.swing.JLabel jLabel9;
	private javax.swing.JPanel jPanel3;
	private TTextField textFieldcod;
	private JTextField textFieldEmail;
	private TTextField textFieldNome;
	private TTextField textFieldEndereco;
	private TTextField textFieldBairro;
	private TTextField textFieldCelular;
	private TTextField textFieldRG;
	public static boolean aberto = false;
	private TTextField textFieldNumero;
	private boolean alterar = false;
	private JFormattedTextField textFieldCpf;
	/**
	 * @wbp.nonvisual location=-39,557
	 */
	private final MaskFormatter maskCpf = new MaskFormatter();
	private JFormattedTextField textFieldTelefone;
	/**
	 * @wbp.nonvisual location=-69,427
	 */
	private final MaskFormatter maskTelefone = new MaskFormatter();
	private JFormattedTextField textFieldCep;
	/**
	 * @wbp.nonvisual location=-69,297
	 */
	private final MaskFormatter maskCep = new MaskFormatter();
	private JPanel jPanel1;
	private JPanel panel_1;
	private JLabel lblDataNascimento;
	private JFormattedTextField textFieldDatanasc;
	private JLabel lblIdade;
	/**
	 * @wbp.nonvisual location=231,-23
	 */
	private final MaskFormatter maskDataNasc = new MaskFormatter();
	private TTextField textFieldIdade;
	private TTextField textFieldUF;
	private TTextField textFieldCidade;
	private JCheckBox chckbxAtivo;
	private TTableCompleta tableCompleta;
	private JLabel lblTipo;
	private JTextField textFieldTipo;

	public void limpar() {
		comboBoxTipoCliente.setSelectedIndex(0);
		textFieldNome.setText(null);
		comboBoxSexo.setSelectedItem("M");
		textFieldEmail.setText(null);
		textFieldRG.setText(null);
		textFieldTelefone.setText(null);
		textFieldCelular.setText(null);
		textFieldUF.setText(null);
		textFieldCidade.setText(null);
		textFieldBairro.setText(null);
		textFieldEndereco.setText(null);
		textFieldNumero.setText(null);
		textFieldCep.setText(null);
		textFieldCpf.setText(null);
		textFieldDatanasc.setText(null);
		textFieldIdade.setText(null);

	}

	public boolean validarCampos() {
		boolean validado = false;
		if (textFieldNome.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Favor informar o Nome !");
			textFieldNome.requestFocus();
			validado = true;
		} else if (textFieldEndereco.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Favor informar o Endereço !");
			textFieldEndereco.requestFocus();
			validado = true;
		} else if (textFieldCpf.getText().equalsIgnoreCase("   .   .   -  ")) {
			JOptionPane.showMessageDialog(null, "Favor informar o CPF !");
			textFieldCpf.requestFocus();
			validado = true;
		} else if (textFieldRG.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Favor informar o RG !");
			textFieldRG.requestFocus();
			validado = true;
		} else if (textFieldBairro.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null, "Favor informar um Bairro !");
			textFieldBairro.requestFocus();
			validado = true;
		}
		String CPF = textFieldCpf.getText().substring(0, 3)
				+ textFieldCpf.getText().substring(4, 7)
				+ textFieldCpf.getText().substring(8, 11)
				+ textFieldCpf.getText().substring(12, 14);

		if (ValidacaoCpf.isCPF(CPF) != true) {
			JOptionPane.showMessageDialog(null, "CPF Invalido", "INFORMATIVO",
					JOptionPane.INFORMATION_MESSAGE);
			textFieldCpf.setText(null);
			textFieldCpf.requestFocus();
			validado = true;
		}
		return validado;

	}

	public List<TipoCliente> listarTipoCliente() {
		comboBoxTipoCliente.removeAllItems();
		GenericDao<Object> tipoClienteDao = new GenericDao<Object>();
		List<TipoCliente> listaTC = tipoClienteDao
				.listar("TipoCliente where ativo=1");
		for (int i = 0; i < listaTC.size(); i++) {
			if (listaTC.get(i).isAtivo() == true)
				comboBoxTipoCliente.addItem(listaTC.get(i));

		}
		return listaTC;
	}

	public void atualizaTabelaCliente() {
		GenericDao<Object> cliDao = new GenericDao<Object>();
		tableCompleta.setDadosIniciais(cliDao.listar("Cliente where ativo=1"));
		textFieldcod.setEditable(true);
		textFieldcod.setText(String.valueOf(id));
		textFieldcod.setEditable(false);
	}

	public void habilitarCampos(boolean ligar) {
		if (ligar) {
			textFieldcod.setEditable(false);
			textFieldEmail.setEditable(true);
			textFieldRG.setEditable(true);
			textFieldTelefone.setEditable(true);
			textFieldCelular.setEditable(true);
			textFieldNumero.setEditable(true);
			textFieldCep.setEditable(true);
			textFieldDatanasc.setEditable(true);
		} else {
			textFieldcod.setEditable(true);
			textFieldcod.setText(null);
			comboBoxTipoCliente.setEditable(false);
			comboBoxSexo.setEditable(false);
			textFieldEmail.setEditable(false);
			textFieldRG.setEditable(false);
			textFieldTelefone.setEditable(false);
			textFieldCelular.setEditable(false);
			textFieldNumero.setEditable(false);
			textFieldCep.setEditable(false);
			textFieldDatanasc.setEditable(false);
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		if (e.getSource() == btnExcluir) {
			do_btnExcluir_actionPerformed(e);
		}
	}

	protected void do_btnExcluir_actionPerformed(ActionEvent e) {
		try {
			GenericDao<Object> cdao = new GenericDao<Object>();
			Cliente cliente = (Cliente) tableCompleta.getSelecionado();
			if (JOptionPane.showConfirmDialog(null,
					"Deseja realmente excluir o cliente " + cliente.getNome(),
					"Excluir", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				cdao.excluir(cliente);
				atualizaTabelaCliente();
			}

		} catch (java.lang.NullPointerException w) {
			JOptionPane.showMessageDialog(null,
					"Selecione o Cliente que deseja excluir!");

		}

	}

	@Override
	public void focusGained(FocusEvent e) {
	}

	@Override
	public void focusLost(FocusEvent e) {
		if (e.getSource() == textFieldCep) {
			do_textFieldCep_focusLost(e);
		}
		if (e.getSource() == textFieldDatanasc) {
			do_frmtdtxtfldDatanasc_focusLost(e);
		}
	}

	protected void do_frmtdtxtfldDatanasc_focusLost(FocusEvent e) {
		int dia = Integer.parseInt(textFieldDatanasc.getText().substring(0, 2));
		int mes = Integer.parseInt(textFieldDatanasc.getText().substring(3, 5));
		int ano = Integer
				.parseInt(textFieldDatanasc.getText().substring(6, 10));
		Calendar nasc = new GregorianCalendar(ano, mes - 1, dia);

		textFieldIdade.setText(String.valueOf(new Calendario()
				.calcularIdade(nasc)));
		textFieldCpf.requestFocus();

	}

	protected void do_textFieldCep_focusLost(FocusEvent e) {

		GenericDao<Endereco> listarEndereco = new GenericDao<Endereco>();
		List<Endereco> endereco = new ArrayList<Endereco>();
		try {
			endereco.add((Endereco) listarEndereco.listarEndereco("Endereco",
					textFieldCep.getText()).get(0));
			System.out.println(endereco.size());
			textFieldCidade.setText(endereco.get(0).getCidade());
			textFieldBairro.setText(endereco.get(0).getBairro());
			textFieldEndereco.setText(endereco.get(0).getLogradouro());
			textFieldUF.setText(endereco.get(0).getUf());
			textFieldTipo.setText(endereco.get(0).getTp_logradouro());
			textFieldNumero.requestFocus();

		} catch (java.lang.IndexOutOfBoundsException e2) {
			Parametros p = new Parametros();
			if (Boolean.parseBoolean(p.lerArqConf("cep_internet"))) {
				buscaCepInternet();
			} else {
				JOptionPane.showMessageDialog(null,
						"CEP inexistente ou não cadastrado no banco de dados");
				textFieldCidade.setText(null);
				textFieldBairro.setText(null);
				textFieldEndereco.setText(null);
				textFieldUF.setText(null);
				textFieldTipo.setText(null);
			}
		}

	}

	public void buscaCepInternet() {

		// Faz a busca para o cep 58043-280
		WebServiceCep endereco =  WebServiceCep
				.searchCep(textFieldCep.getText());
		// A ferramenta de busca ignora qualquer caracter que n?o seja n?mero.

		// caso a busca ocorra bem, imprime os resultados.
		if (endereco.wasSuccessful()) {
			textFieldCidade.setText(endereco.getCidade());
			textFieldBairro.setText(endereco.getBairro());
			textFieldUF.setText(endereco.getUf());
			textFieldTipo.setText(endereco.getLogradouroType());
			textFieldEndereco.setText(endereco.getLogradouro());

			// Gravar endereço localizado no banco de dados
			Endereco end = new Endereco();
			GenericDao<Endereco> endDao = new GenericDao<Endereco>();

			String uf_nome = null;
			String ufInternet = endereco.getUf();
			for (Uf uf : Uf.values()) {

				if (ufInternet.equalsIgnoreCase(String.valueOf(uf))) {
					uf_nome = uf.getUf_estenco();
					break;
				}

			}
			end.setCep(textFieldCep.getText());
			end.setCidade(endereco.getCidade());
			end.setBairro(endereco.getBairro());

			end.setUf_nome(uf_nome);
			end.setUf(endereco.getUf());
			end.setTp_logradouro(endereco.getLogradouroType());
			end.setLogradouro(endereco.getLogradouro());

			endDao.salvar(end);

			// caso haja problemas imprime as exceções.
		} else {
			JOptionPane.showMessageDialog(null, "Descrição do erro: "
					+ endereco.getResultText());
		}
	}
}
