package estacionamento.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JFormattedTextField;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.text.MaskFormatter;

import estacionamento.componente.TButton;
import estacionamento.entidade.Cliente;
import estacionamento.entidade.TipoVeiculo;
import estacionamento.entidade.Veiculo;
import estacionamento.persistencia.GenericDao;
import framework.component.field.TTextField;
import framework.component.table.TTableCompleta;

public class InternalVeiculo extends JInternalFrame implements ActionListener {
	private final TTextField textFieldNome;
	private long id = 0;
	private Integer idTipoVeiculo = 0;
	public static boolean aberto = false;
	private final JPanel panel_2;
	private boolean alterar = false;
	private final JTextField textFieldCod;
	private final JFormattedTextField textFieldPlaca;
	private final JFormattedTextField textFieldCPF;
	private final TTextField textFieldFabricante;
	private final TTextField textFieldModelo;
	private final TTextField textFieldCor;
	private final TTextField textFieldAno;
	private final MaskFormatter maskPlaca = new MaskFormatter();
	private final MaskFormatter maskCpf = new MaskFormatter();
	private final JPanel panel_3;
	private final TButton btnSalvar;
	private final TButton btnExcluir;
	private final TButton btnAlterar;
	private final TButton btnSair;
	private TTableCompleta tableCompleta;

	public InternalVeiculo() {

		try {
			maskPlaca.setMask("***-####");
			maskPlaca.setPlaceholderCharacter('_');
			maskCpf.setPlaceholderCharacter('_');
			maskCpf.setMask("###.###.###-##");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			GenericDao d = new GenericDao();
			id = d.ultimoID("Veiculo") + 1;
		} catch (java.lang.NullPointerException e) {
			id = 1;
		}

		setTitle("Cadastro de Veículos");
		setBounds(100, 100, 884, 571);

		JPanel panel = new JPanel();
		panel.setLayout(null);
		panel.setPreferredSize(new Dimension(10, 200));

		JLabel label = new JLabel();
		label.setText("Código:");
		label.setFont(new Font("Dialog", Font.PLAIN, 13));
		label.setBounds(16, 23, 76, 14);
		panel.add(label);

		JLabel lblAnoDeFabricao = new JLabel();
		lblAnoDeFabricao.setText("Ano de Fabricação:");
		lblAnoDeFabricao.setFont(new Font("Dialog", Font.PLAIN, 13));
		lblAnoDeFabricao.setBounds(288, 87, 135, 14);
		panel.add(lblAnoDeFabricao);

		JLabel lblCor = new JLabel();
		lblCor.setText("Cor:");
		lblCor.setFont(new Font("Dialog", Font.PLAIN, 13));
		lblCor.setBounds(16, 83, 76, 14);
		panel.add(lblCor);

		JLabel lblModelo_1 = new JLabel();
		lblModelo_1.setText("Modelo:");
		lblModelo_1.setFont(new Font("Dialog", Font.PLAIN, 13));
		lblModelo_1.setBounds(288, 55, 63, 14);
		panel.add(lblModelo_1);

		JLabel lblModelo = new JLabel();
		lblModelo.setText("Fabricante:");
		lblModelo.setFont(new Font("Dialog", Font.PLAIN, 13));
		lblModelo.setBounds(16, 53, 76, 14);
		panel.add(lblModelo);

		JLabel lblPlaca = new JLabel();
		lblPlaca.setText("Placa:");
		lblPlaca.setFont(new Font("Dialog", Font.PLAIN, 13));
		lblPlaca.setBounds(286, 25, 49, 14);
		panel.add(lblPlaca);
		panel_2 = new JPanel();
		getContentPane().add(panel_2, BorderLayout.CENTER);
		panel_2.setLayout(new BorderLayout(0, 0));

		panel_3 = new JPanel();

		btnSalvar = new TButton();
		btnSalvar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnSalvar.setIcon(new ImageIcon(InternalVeiculo.class
				.getResource("/img/save.png")));
		btnSalvar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				do_btnSalvar_actionPerformed(arg0);
			}
		});
		btnSalvar.setText("Salvar");
		panel_3.add(btnSalvar);

		btnExcluir = new TButton();
		btnExcluir.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnExcluir.setIcon(new ImageIcon(InternalVeiculo.class
				.getResource("/img/excluir.gif")));
		btnExcluir.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				do_btnExcluir_actionPerformed(arg0);
			}
		});
		btnExcluir.setText("Excluir");
		panel_3.add(btnExcluir);

		btnAlterar = new TButton();
		btnAlterar.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnAlterar.setIcon(new ImageIcon(InternalVeiculo.class
				.getResource("/img/alterar.gif")));
		btnAlterar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				do_btnAlterar_actionPerformed(arg0);
			}
		});
		btnAlterar.setText("Alterar");
		panel_3.add(btnAlterar);

		btnSair = new TButton();

		btnSair.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnSair.setIcon(new ImageIcon(InternalVeiculo.class
				.getResource("/img/Sair.png")));
		btnSair.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				do_btnSair_actionPerformed(arg0);
			}
		});
		btnSair.setText("Sair");
		panel_3.add(btnSair);

		GroupLayout groupLayout = new GroupLayout(getContentPane());
		groupLayout.setHorizontalGroup(groupLayout
				.createParallelGroup(Alignment.TRAILING)
				.addComponent(panel, GroupLayout.PREFERRED_SIZE, 878,
						Short.MAX_VALUE)
				.addGroup(
						Alignment.LEADING,
						groupLayout
								.createSequentialGroup()
								.addComponent(panel_2,
										GroupLayout.DEFAULT_SIZE, 868,
										Short.MAX_VALUE).addContainerGap())
				.addGroup(
						Alignment.LEADING,
						groupLayout
								.createSequentialGroup()
								.addComponent(panel_3,
										GroupLayout.DEFAULT_SIZE, 868,
										Short.MAX_VALUE).addContainerGap()));
		groupLayout.setVerticalGroup(groupLayout.createParallelGroup(
				Alignment.LEADING).addGroup(
				groupLayout
						.createSequentialGroup()
						.addComponent(panel, GroupLayout.PREFERRED_SIZE, 163,
								GroupLayout.PREFERRED_SIZE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 320,
								Short.MAX_VALUE)
						.addPreferredGap(ComponentPlacement.RELATED)
						.addComponent(panel_3, GroupLayout.PREFERRED_SIZE, 40,
								GroupLayout.PREFERRED_SIZE).addGap(7)));
		
		tableCompleta = new TTableCompleta(Veiculo.class);
		panel_2.add(tableCompleta, BorderLayout.CENTER);

		JLabel lblCliente = new JLabel();
		lblCliente.setText("CPF Cliente:");
		lblCliente.setFont(new Font("Dialog", Font.PLAIN, 13));
		lblCliente.setBounds(16, 121, 76, 14);
		panel.add(lblCliente);

		JLabel lblNome = new JLabel("Nome:");
		lblNome.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblNome.setBounds(289, 123, 46, 14);
		panel.add(lblNome);

		textFieldNome = new TTextField();
		textFieldNome.setEditable(false);
		textFieldNome.setBounds(353, 121, 401, 20);
		panel.add(textFieldNome);
		textFieldNome.setColumns(10);

		textFieldCPF = new JFormattedTextField(maskCpf);
		textFieldCPF.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				do_textFieldCPF_focusLost(arg0);
			}
		});
		textFieldCPF.setBounds(104, 119, 151, 20);
		panel.add(textFieldCPF);
		textFieldCPF.setColumns(10);

		textFieldCod = new JTextField();
		textFieldCod.setFont(new Font("Tahoma", Font.PLAIN, 11));
		textFieldCod.setBounds(98, 21, 58, 20);
		textFieldCod.setBounds(104, 21, 153, 20);
		panel.add(textFieldCod);
		textFieldCod.setColumns(10);

		textFieldPlaca = new JFormattedTextField(maskPlaca);
		textFieldPlaca.setBounds(353, 23, 155, 20);
		panel.add(textFieldPlaca);
		textFieldPlaca.setColumns(10);

		textFieldFabricante = new TTextField();
		textFieldFabricante.setColumns(10);
		textFieldFabricante.setBounds(103, 51, 154, 20);
		panel.add(textFieldFabricante);

		textFieldModelo = new TTextField();
		textFieldModelo.setColumns(10);
		textFieldModelo.setBounds(353, 53, 155, 20);
		panel.add(textFieldModelo);

		textFieldCor = new TTextField();
		textFieldCor.setColumns(10);
		textFieldCor.setBounds(103, 81, 154, 20);
		panel.add(textFieldCor);

		textFieldAno = new TTextField();
		textFieldAno.setBounds(422, 85, 86, 20);
		panel.add(textFieldAno);
		textFieldAno.setColumns(10);

		JLabel lblTipoVeculo = new JLabel("Tipo Veículo");
		lblTipoVeculo.setBounds(548, 26, 91, 15);
		panel.add(lblTipoVeculo);

		// Popular combobox com os dados TipoVeículo do banco de dados.
		List<TipoVeiculo> tipoveiculo = new ArrayList<TipoVeiculo>();
		GenericDao<Object> dbTipoVeiculo = new GenericDao<Object>();
		tipoveiculo.addAll(dbTipoVeiculo.listar("TipoVeiculo where ativo = 1"));
		final JComboBox cbxTipoVeiculo = new JComboBox();
		cbxTipoVeiculo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				idTipoVeiculo = 0;
				idTipoVeiculo = 1 + cbxTipoVeiculo.getSelectedIndex();
			}
		});
		cbxTipoVeiculo
				.setModel(new DefaultComboBoxModel(tipoveiculo.toArray()));

		cbxTipoVeiculo.setBounds(643, 21, 223, 21);
		panel.add(cbxTipoVeiculo);
		getContentPane().setLayout(groupLayout);

		// habilitarCampos(true);
		atualizaTabelaVeiculo();

	}

	public void limpar() {
		textFieldFabricante.setText(null);
		textFieldAno.setText(null);
		textFieldCor.setText(null);
		textFieldModelo.setText(null);
		textFieldPlaca.setText(null);
		textFieldCPF.setText(null);
		textFieldNome.setText(null);

	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
	}

	private void atualizaTabelaVeiculo() {
		
		GenericDao<Object> veiculoDao = new GenericDao<Object>();
		tableCompleta.setDadosIniciais(veiculoDao.listar("Veiculo"));
		textFieldCod.setEditable(true);
		textFieldCod.setText(String.valueOf(id));
		textFieldCod.setEditable(false);
	}

	protected void do_textFieldCPF_focusLost(FocusEvent arg0) {
		if (textFieldCPF.getText().equalsIgnoreCase("___.___.___-__")) {
			textFieldCPF.setText(null);
			textFieldNome.setText(null);

		} else {
			GenericDao<Cliente> listarCliente = new GenericDao<Cliente>();
			List<Cliente> cliente = new ArrayList<Cliente>();
			try {

				cliente.add((Cliente) listarCliente.listarCliente(
						textFieldCPF.getText()).get(0));
				textFieldNome.setText(cliente.get(0).getNome().toUpperCase());

			} catch (java.lang.IndexOutOfBoundsException e2) {
				textFieldCPF.setText(null);
				textFieldNome.setText(null);
				JOptionPane.showMessageDialog(null,
						"CPF inexistente ou não cadastrado no banco de dados");
				textFieldCPF.setText(null);

			}

		}
	}

	protected void do_btnSalvar_actionPerformed(ActionEvent arg0) {
		Veiculo veiculo = new Veiculo();
		Cliente c = new Cliente();
		TipoVeiculo tipoVeiculo = new TipoVeiculo();
		GenericDao<Cliente> clienteDao = new GenericDao<Cliente>();
		List<Cliente> listaCliente = new ArrayList<Cliente>();

		String msg = " cadastrado com sucesso.";
		// Para saber quando estou fazendo um update ou insert
		if (alterar == true) {
			veiculo.setId(Integer.parseInt(textFieldCod.getText()));
			msg = " alterado com sucesso.";
			id = id - 1;
			alterar = false;
		}
		if (validarCampos()) {
			try {
				tipoVeiculo.setId(idTipoVeiculo);
				veiculo.setFrabricante(textFieldFabricante.getText());
				veiculo.setAnoFabricacao(Integer.parseInt(textFieldAno
						.getText()));
				veiculo.setCor(textFieldCor.getText());
				veiculo.setModelo(textFieldModelo.getText());
				veiculo.setPlaca(textFieldPlaca.getText());
				veiculo.setTipoveiculo(tipoVeiculo);
			} catch (java.lang.NumberFormatException e) {
				JOptionPane.showMessageDialog(null,
						"Favor informar o ano de fabricação do veiculo !");
				textFieldAno.requestFocus();
			}

			if (!textFieldNome.getText().isEmpty()) {
				listaCliente.add((Cliente) clienteDao.listarCliente(
						textFieldCPF.getText()).get(0));
				textFieldNome.setText(listaCliente.get(0).getNome()
						.toUpperCase());
				c = listaCliente.get(0);
				veiculo.setCliente(c);
			} else {
				c = null;
				veiculo.setCliente(null);
			}

			GenericDao<Object> veiculoDao = new GenericDao<Object>();
			if (veiculoDao.salvar(veiculo) == true) {
				atualizaTabelaVeiculo();
				JOptionPane.showMessageDialog(null,
						"Veiculo " + veiculo.getPlaca() + msg);
				limpar();
				id = id + 1;
				textFieldCod.setText(String.valueOf(id));
			}
		}
	}

	protected void do_btnSair_actionPerformed(ActionEvent arg0) {
		aberto = false;
		dispose();
	}

	protected void do_btnExcluir_actionPerformed(ActionEvent arg0) {
		try {
			GenericDao<Object> veiculoDao = new GenericDao<Object>();
			Veiculo veiculo = new Veiculo();
			veiculo = (Veiculo) tableCompleta.getSelecionado();
			if (JOptionPane.showConfirmDialog(null,
					"Deseja realmente excluir o veiculo " + veiculo.getModelo()
							+ "Placa: " + veiculo.getPlaca()// .getPlaca(),
									// ATENCAO PARA
									// MUDANCA
									.toString(), "Excluir",
					JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
				veiculoDao.excluir(tableCompleta.getSelecionado());
				atualizaTabelaVeiculo();
			}
		} catch (java.lang.NullPointerException w) {
			JOptionPane.showMessageDialog(null,
					"Selecione o veiculo que deseja excluir!");
		}
	}

	protected void do_btnAlterar_actionPerformed(ActionEvent arg0) {

		if (tableCompleta.getSelecionado() != null) {
			Veiculo veiculo = new Veiculo();
			veiculo = (Veiculo) tableCompleta.getSelecionado();
			textFieldAno.setText(String.valueOf(veiculo.getAnoFabricacao()));
			textFieldCod.setText(String.valueOf(veiculo.getId()));
			textFieldCor.setText(veiculo.getCor());
			textFieldFabricante.setText(veiculo.getFrabricante());
			textFieldModelo.setText(veiculo.getModelo());
			textFieldPlaca.setText(veiculo.getPlaca());

			if (veiculo.getCliente() != null) {
				textFieldCPF.setText(veiculo.getCliente().getCpf());
				textFieldNome.setText(veiculo.getCliente().getNome());
			} else {
				textFieldCPF.setText(null);
				textFieldNome.setText(null);
			}
			alterar = true;
		}
	}

	public boolean validarCampos() {
		boolean validado = true;
		if (textFieldPlaca.getText().equalsIgnoreCase("___-____")) {
			JOptionPane.showMessageDialog(null, "Favor a placa do veiculo!");
			textFieldPlaca.requestFocus();
			validado = false;
		} else if (textFieldFabricante.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null,
					"Favor informar o fabricante ou marca do veiculo !");
			textFieldFabricante.requestFocus();
			validado = false;
		} else if (textFieldModelo.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null,
					"Favor informar o modelo do veiculo !");
			textFieldModelo.requestFocus();
			validado = false;
		} else if (textFieldCor.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null,
					"Favor informar a cor do veiculo !");
			textFieldCor.requestFocus();
			validado = false;
		} else if (textFieldAno.getText().isEmpty()) {
			JOptionPane.showMessageDialog(null,
					"Favor informar o ano de fabricação do veiculo !");
			textFieldAno.requestFocus();
			validado = false;
		}
		return validado;
	}
}
