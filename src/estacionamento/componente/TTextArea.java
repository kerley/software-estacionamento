package estacionamento.componente;

import javax.swing.JTextArea;
import javax.swing.text.Document;

public class TTextArea extends JTextArea {

	private static final long serialVersionUID = -893918208587652239L;
	private AlterarFocoEnter alterarFocoEnter = new AlterarFocoEnter();

	public TTextArea() {
		super();
		alterarFocoEnter.alterar(this);
	}

	public TTextArea(Document doc, String text, int rows, int columns) {
		super(doc, text, rows, columns);
		alterarFocoEnter.alterar(this);
	}

	public TTextArea(Document doc) {
		super(doc);
		alterarFocoEnter.alterar(this);
	}

	public TTextArea(int rows, int columns) {
		super(rows, columns);
		alterarFocoEnter.alterar(this);
	}

	public TTextArea(String text, int rows, int columns) {
		super(text, rows, columns);
		alterarFocoEnter.alterar(this);
	}

	public TTextArea(String text) {
		super(text);
		alterarFocoEnter.alterar(this);
	}

}
