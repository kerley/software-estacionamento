package estacionamento.componente;


import javax.swing.ImageIcon;
import javax.swing.JPanel;
import java.awt.Graphics;
import java.awt.Image;

public class PanelComImagem extends JPanel {
	@Override
	protected void paintComponent(final Graphics g) {

		Image background = new ImageIcon(getClass().getResource(
				"/img/fundo.png")).getImage();
		g.drawImage(background, 0, 0, this);

	}
}
