package estacionamento.componente;

import java.awt.AWTKeyStroke;
import java.awt.Component;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.util.HashSet;
import java.util.Set;

import javax.swing.JComponent;
import javax.swing.KeyStroke;

public class AlterarFocoEnter {
	public void alterar(Component component) {
		Set<AWTKeyStroke> keystrokes = component
				.getFocusTraversalKeys(KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS);
		Set<AWTKeyStroke> newKeystrokes = new HashSet<AWTKeyStroke>(keystrokes);
		newKeystrokes.add(AWTKeyStroke.getAWTKeyStroke(KeyEvent.VK_ENTER, 0));
		component.setFocusTraversalKeys(
				KeyboardFocusManager.FORWARD_TRAVERSAL_KEYS, newKeystrokes);
	}
	

	public void clicaBotao(Component component) {
	        ((JComponent) component).registerKeyboardAction(  
	                ((JComponent) component).getActionForKeyStroke(  
	                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),  
	                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),  
	                JComponent.WHEN_FOCUSED);  
	          
	        ((JComponent) component).registerKeyboardAction(  
	                ((JComponent) component).getActionForKeyStroke(  
	                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),  
	                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),  
	                JComponent.WHEN_FOCUSED);  
	    }  
	}
