package estacionamento.componente;

import javax.swing.JTextField;
import javax.swing.text.Document;

public class TTextField extends JTextField  {

	private static final long serialVersionUID = -1652614652802936516L;
	private final AlterarFocoEnter alterarFocoEnter = new AlterarFocoEnter();
	
	public TTextField() {
		super();
		alterarFocoEnter.alterar(this);
	}

	public TTextField(Document doc, String text, int columns) {
		super(doc, text, columns);
		alterarFocoEnter.alterar(this);
	}

	public TTextField(int columns) {
		super(columns);
		alterarFocoEnter.alterar(this);
	}

	public TTextField(String text, int columns) {
		super(text, columns);
		alterarFocoEnter.alterar(this);
	}

	public TTextField(String text) {
		super(text);
		alterarFocoEnter.alterar(this);
	}
	
	
}
