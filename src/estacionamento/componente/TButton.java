package estacionamento.componente;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.JButton;

public class TButton extends JButton {

	private static final long serialVersionUID = -5300900088899891857L;
	private AlterarFocoEnter alterarFocoEnter = new AlterarFocoEnter();

	public TButton() {
		super();
		alterarFocoEnter.clicaBotao(this);
	}

	public TButton(Action a) {
		super(a);
		alterarFocoEnter.clicaBotao(this);
	}

	public TButton(Icon icon) {
		super(icon);
		alterarFocoEnter.clicaBotao(this);
	}

	public TButton(String text, Icon icon) {
		super(text, icon);
		alterarFocoEnter.clicaBotao(this);
	}

	public TButton(String text) {
		super(text);
		alterarFocoEnter.clicaBotao(this);
	}

}
