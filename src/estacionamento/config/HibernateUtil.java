package estacionamento.config;

//Arquivo de configuracao do hibernete
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import estacionamento.entidade.Cliente;
import estacionamento.entidade.Endereco;
import estacionamento.entidade.Estacionamento;
import estacionamento.entidade.FormaCobranca;
import estacionamento.entidade.Funcionarios;
import estacionamento.entidade.Preco;
import estacionamento.entidade.TipoCliente;
import estacionamento.entidade.TipoVeiculo;
import estacionamento.entidade.Veiculo;

public class HibernateUtil {
	private static final SessionFactory sessionFactory;
	static {
		AnnotationConfiguration cfg = new AnnotationConfiguration();

		cfg.setProperty("hibernate.dialect", Parametros.DIALECT);
		cfg.setProperty("hibernate.connection.driver_class", Parametros.DRIVER);
		cfg.setProperty("hibernate.connection.url", Parametros.URL);
		cfg.setProperty("hibernate.connection.username", Parametros.USUARIO);
		cfg.setProperty("hibernate.connection.password", Parametros.SENHA);
		// configuracao de log no console
		cfg.setProperty("hibernate.show_sql", "true");

		// hibernate realiza a tranza��o automaticamente
		// realiza as instru�oes sql.
		// Padrao eh n�o fazer nada
		cfg.setProperty("hibernate.hbm2ddl.auto", "update");

		cfg.addAnnotatedClass(TipoCliente.class);
		cfg.addAnnotatedClass(Cliente.class);
		cfg.addAnnotatedClass(Endereco.class);
		cfg.addAnnotatedClass(Veiculo.class);
		cfg.addAnnotatedClass(Preco.class);
		cfg.addAnnotatedClass(FormaCobranca.class);
		cfg.addAnnotatedClass(Funcionarios.class);
		cfg.addAnnotatedClass(Estacionamento.class);
		cfg.addAnnotatedClass(TipoVeiculo.class);
		sessionFactory = cfg.buildSessionFactory();

	}

	public static Session getSession() {
		return sessionFactory.openSession();
	}
}