package estacionamento.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JOptionPane;

//arquivo de configura��o de paramentros para conexao com banco
public class Parametros {

	public static String lerArqConf(String chave) {
		String result = null;
		try {

			File file = new File("hd.conf");// Estanciando um objeto do tipo
											// File e passando o caminho do
											// arquivo que desejo manipular

			FileInputStream fis = new FileInputStream(file); // Estanciando um
																// novo objeto
																// do tipo
																// FileInputStream.
																// FileInputStream
																// permite a
																// leitura de
																// arquivo em
																// disco

			Properties props = new Properties(); // Arqui estou estanciando um
													// objeto do tipo
													// Properties. Properties
													// tem um metado getProperty
													// que me permite trabalhar
													// com chave em arquivo,
													// porem o arquivo deve esta
													// no seguinte formato
													// chave=valor.

			props.load(fis);
			result = props.getProperty(chave); // Arqui estou passando a chave
												// que quero o resultado e
												// adicionando em uma variavel
												// result
			fis.close();
		} catch (java.io.FileNotFoundException e) {
			JOptionPane.showMessageDialog(null,
					"hd.conf (Arquivo ou diretório não encontrado)");
			System.exit(0);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;

	}

	public static final String URL = "jdbc:mysql://" + lerArqConf("host")
			+ ":3306/" + lerArqConf("banco");
	public static final String USUARIO = lerArqConf("user");
	public static final String SENHA = lerArqConf("senha");
	public static final String DRIVER = "com.mysql.jdbc.Driver";
	public static final String DIALECT = "org.hibernate.dialect.MySQLDialect";

}