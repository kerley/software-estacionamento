SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE SCHEMA IF NOT EXISTS `db_estacionamento` DEFAULT CHARACTER SET latin1 ;
USE `db_estacionamento` ;

-- -----------------------------------------------------
-- Table `db_estacionamento`.`Usuario`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `db_estacionamento`.`Usuario` (
  `idUsuario` BIGINT(20) NOT NULL ,
  `Nome` VARCHAR(60) NOT NULL ,
  `Cpf` INT(11) NOT NULL ,
  `Tipo` CHAR(1) NOT NULL ,
  PRIMARY KEY (`idUsuario`) ,
  UNIQUE INDEX `Cpf_UNIQUE` (`Cpf` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `db_estacionamento`.`entrada`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `db_estacionamento`.`entrada` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `placa` VARCHAR(10) NULL DEFAULT NULL ,
  `veiculo` VARCHAR(30) NULL DEFAULT NULL ,
  `cor` VARCHAR(20) NULL DEFAULT NULL ,
  `horaentrada` VARCHAR(8) NULL DEFAULT NULL ,
  `dataentrada` VARCHAR(10) NULL DEFAULT NULL ,
  `saida_id` BIGINT(20) NULL DEFAULT '0' ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
AUTO_INCREMENT = 30
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `db_estacionamento`.`funcionarios`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `db_estacionamento`.`funcionarios` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `nome` TEXT NULL DEFAULT NULL ,
  `idade` INT(3) NULL DEFAULT NULL ,
  `cpf` VARCHAR(14) NULL DEFAULT NULL ,
  `telefone` VARCHAR(13) NULL DEFAULT NULL ,
  `login` TEXT NULL DEFAULT NULL ,
  `senha` VARCHAR(45) NULL DEFAULT NULL ,
  `tipo` CHAR(1) NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `cpf_UNIQUE` (`cpf` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 12
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `db_estacionamento`.`saida`
-- -----------------------------------------------------
CREATE  TABLE IF NOT EXISTS `db_estacionamento`.`saida` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `horasaida` VARCHAR(8) NOT NULL ,
  `datasaida` VARCHAR(10) NOT NULL ,
  `entrada_id` BIGINT(20) NOT NULL ,
  PRIMARY KEY (`id`, `entrada_id`) ,
  INDEX `fk_saida_entrada` (`entrada_id` ASC) ,
  CONSTRAINT `fk_saida_entrada`
    FOREIGN KEY (`entrada_id` )
    REFERENCES `db_estacionamento`.`entrada` (`id` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 24
DEFAULT CHARACTER SET = latin1;



SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
